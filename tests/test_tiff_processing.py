import ctrappy.scan
import ctrappy.file
import ctrappy.gui_tabs.tools.tiff_functions as tiff_functions
import ctrappy.tracking
import pandas as pd
import os
import numpy as np


def test_tiff_to_scan():

    # Paths to data resources.
    test_dir = os.path.dirname(__file__)
    path1_tiff = os.path.join(test_dir, 'resources', 'dna1_scan5.tiff')
    path1_meta_yml = os.path.join(test_dir, 'resources', 'dna1_scan5.yaml')
    path2_tiff = os.path.join(test_dir, 'resources', 'dna2_scan11.tiff')
    path2_meta_yml = os.path.join(test_dir, 'resources', 'dna2_scan11.yaml')
    paths_tiff = [path1_tiff, path2_tiff]
    paths_meta_yml = [path1_meta_yml, path2_meta_yml]
    path_params_yml = os.path.join(test_dir, 'resources', 'params_offset_tracking.yaml')
    path_config_yml = os.path.join(test_dir, 'resources', 'config.yaml')
    path_scans_csv = os.path.join(test_dir, 'resources', 'scans.csv')

    # Read tiff files and metadata.
    params = ctrappy.file.read_yaml(path_params_yml, schema='tracking_params')
    config = ctrappy.file.read_yaml(path_config_yml, schema='ctrappy_configuration')
    df_full = pd.DataFrame()
    for i, path_tiff, path_meta in zip([0, 1], paths_tiff, paths_meta_yml):

        # Read metadata, apply offset.
        metadata = ctrappy.file.read_yaml(path_meta, schema='tiff_metadata')
        image = ctrappy.file.read_biotiff(path_tiff)
        shift = params['offset']['brightfield confocal shift (micron)']
        offset_values = tiff_functions.get_offset_from_metadata(metadata, config, shift)
        image.apply_offset_mask(offset_values)

        # Detect & track spots.
        spots = image.get_spots(params['detection']['thresholds'],
                                params['detection']['radius (pixels)'],
                                offset_values,
                                colors=tiff_functions.get_color_inds(params['detection']['laser colors']),
                                remove_edge_spots=False)
        tracks = image.track_spots(spots, params['tracking']['max frame skip'],
                                   params['tracking']['max dist between spots (pixels)'])

        tracks['scan_id'] = i
        tracks['dna_start_pixel'] = offset_values[0]
        tracks['dna_end_pixel'] = offset_values[1]
        tracks['n_scans'] = len(paths_tiff)
        tracks['n_frames'] = image.n_frames

        # Remove single spots.
        single_inds = np.array([])
        for j, track in tracks.groupby('track_id'):
            if len(track) == 1:
                single_inds = np.concatenate((single_inds, track.index.values))
        tracks = tracks[~tracks.index.isin(single_inds)]

        # Run colocalization.
        colocalizer = ctrappy.tracking.Colocalizer(config['colocalization_distance'],
                                                   config['bleed_through'],
                                                   cf_res=config['confocal_pixel_size'])
        df_tracks_converted = tiff_functions.tracks_to_scan_df(tracks, path_tiff)
        tracks_col = colocalizer.colocalize(df_tracks_converted, params['detection']['laser colors'])

        # Append to scans.
        if not tracks_col.empty:
            df_full = pd.concat((df_full, tracks_col), ignore_index=True)

    # Check result.
    df_scans_ref = pd.read_csv(path_scans_csv)
    assert (df_full['corrected_intensity'] - df_scans_ref['corrected_intensity']).abs().max() < 1e-7
    assert (df_full['x_pixel'] - df_scans_ref['x_pixel']).abs().max() < 1e-3
    assert (df_full['dna_start_pixel'] - df_scans_ref['dna_start_pixel']).abs().max() < 1e-7