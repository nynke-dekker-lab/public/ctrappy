import ctrappy.tracking
import numpy as np


def test_single_generated_spot_detection():
    data = np.zeros((1, 100, 100, 1))
    for x in range(100):
        for y in range(100):
            data[0, y, x, 0] = int(300 * ctrappy.tracking.gaussian2d(x, y, 42.3, 88.7, 3))

    detector = ctrappy.tracking.Detector(radius=4, thresholds=[0.1], overlap=0.5)
    df = detector.detect(data, colors=[0])
    assert len(df) == 1
    assert 42 < df['x'].iloc[0] < 43  # Subpixel localization is not perfect.
    assert 88 < df['y'].iloc[0] < 89
