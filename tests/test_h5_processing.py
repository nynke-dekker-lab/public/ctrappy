import os
import ctrappy.file
import numpy as np


def test_h5file():
    """Read h5 file, check if resulting image and metadata are as expected."""

    # Read input file.
    resources_dir = os.path.join(os.path.dirname(__file__), 'resources')
    file_path = os.path.join(resources_dir, 'dna1.h5')
    h5file, images = ctrappy.file.read_h5file(file_path)

    # Make output directory.
    test_results_path = os.path.join(os.path.dirname(__file__), 'test_output')
    if not os.path.exists(test_results_path):
        os.makedirs(test_results_path)

    # Export output data.
    images[5].export(os.path.join(test_results_path, "scan.tiff"),
                     os.path.join(test_results_path, "metadata.yaml"),
                     os.path.join(test_results_path, "Fd.csv"))

    # Compare image to expected output.
    compare_tiff_path = os.path.join(resources_dir, 'dna1_scan5.tiff')
    compare_tiff = ctrappy.file.read_biotiff(compare_tiff_path)
    test_tiff_path = os.path.join(test_results_path, 'scan.tiff')
    test_tiff = ctrappy.file.read_biotiff(test_tiff_path)
    tiff_diff = np.amax(np.abs((compare_tiff.data - test_tiff.data).flatten()))

    # Compare metadata to expected output.
    compare_yaml_path = os.path.join(resources_dir, 'dna1_scan5.yaml')
    compare_yaml = ctrappy.file.read_yaml(compare_yaml_path, schema='tiff_metadata')
    test_yaml_path = os.path.join(test_results_path, 'metadata.yaml')
    test_yaml = ctrappy.file.read_yaml(test_yaml_path, schema='tiff_metadata')
    dna_length_um_diff = np.abs(compare_yaml['DNA length (micron)'] - test_yaml['DNA length (micron)'])
    bead0_center_um = np.abs(compare_yaml['bead0 center (micron)'] - test_yaml['bead0 center (micron)'])

    # Remove output.
    if os.path.exists(test_results_path):
        os.remove(os.path.join(test_results_path, "scan.tiff"))
        os.remove(os.path.join(test_results_path, "metadata.yaml"))
        os.remove(os.path.join(test_results_path, "Fd.csv"))
        os.rmdir(test_results_path)

    # Check up to machine precision.
    assert tiff_diff < 1e-14
    assert dna_length_um_diff < 1e-14
    assert bead0_center_um < 1e-14
