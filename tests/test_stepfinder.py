import ctrappy.stepfinder
import numpy as np


def test_stepfinder():
    x = np.arange(10)
    y = 1 - np.array([0.0, 0.1, 0.0, 0.0, 0.3, 0.35, 0.29, 1.0, 1.0, 0.9])

    step_locs, step_sizes, sig_det = ctrappy.stepfinder.find_steps(x, y,
                                                                   min_step_size=0.2,
                                                                   min_plateau_length=1)
    assert len(step_locs) == 2
    assert step_locs[0] == 4
    assert step_locs[1] == 7

    step_locs, step_sizes, sig_det = ctrappy.stepfinder.find_steps(x, y,
                                                                   min_step_size=0.5,
                                                                   min_plateau_length=1)
    assert len(step_locs) == 1
    assert step_locs[0] == 7
