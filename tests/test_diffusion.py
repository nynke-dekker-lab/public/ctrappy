import ctrappy.diffusion
import ctrappy.diffusion_fit
import ctrappy.gui_cpa_figs
import ctrappy.scan
import numpy as np
import pandas as pd


def get_example_scans(n_frames):
    scans_dict = {'file_name': ['test' for _ in range(n_frames)],
                  'scan_id': [0 for _ in range(n_frames)],
                  'trace_id': [0 for _ in range(n_frames)],
                  'track_id': [0 for _ in range(n_frames)],
                  'color_id': [0 for _ in range(n_frames)],
                  'color_str': ['r' for _ in range(n_frames)],
                  'frame': [i for i in range(n_frames)],
                  'time_s': [i for i in range(n_frames)],
                  'intensity': [100 for _ in range(n_frames)],
                  'corrected_intensity': [100 for _ in range(n_frames)],
                  'x_pixel': [10 for _ in range(n_frames)],
                  'y_pixel': [0 for _ in range(n_frames)],
                  'dna_start_pixel': [0 for _ in range(n_frames)],
                  'dna_end_pixel': [100 for _ in range(n_frames)],
                  'n_scans': [1 for _ in range(n_frames)],
                  'n_frames': [n_frames for _ in range(n_frames)]}
    return pd.DataFrame(scans_dict)


def get_example_config():
    config_dict = {'min_step_sizes_rgb': [50, 50, 50],
                   'avg_step_sizes_rgb': [100, 100, 100],
                   'confocal_pixel_size': 0.05,
                   'step_counting_divide': True,
                   'colocalization_distance': 0.2,
                   'bleed_through': np.zeros((3, 3, 3))}
    return config_dict


def test_msd():
    x = np.arange(1, 11)
    msd = ctrappy.diffusion.get_msd(x)
    for xval, msdval in zip(x, msd):
        assert msdval == xval**2


def test_diffusion():

    np.random.seed(0)
    x_vals = np.cumsum(np.random.normal(0, 1, 1000))
    diff_estimate = 1
    t_exposure = 0.05
    avg_photon_count = 100
    sigma_psf = 0.5
    frame_time = 0.5
    d = ctrappy.diffusion.get_diffusion_coefficient(x_vals, diff_estimate, t_exposure,
                                                    avg_photon_count, sigma_psf, frame_time)
    assert 0.5 < d < 5  # D approx 1; should be correct order of magnitude.

    np.random.seed(0)
    x_vals = np.cumsum(np.random.normal(0, 5, 1000))
    diff_estimate = 25
    t_exposure = 0.05
    avg_photon_count = 100
    sigma_psf = 0.5
    frame_time = 0.5
    d = ctrappy.diffusion.get_diffusion_coefficient(x_vals, diff_estimate, t_exposure,
                                                    avg_photon_count, sigma_psf, frame_time)
    assert 12 < d < 120  # D approx 25; should be correct order of magnitude.


def test_anomalous_diffusion():

    # Uniform motion.
    df_scans = get_example_scans(n_frames=500)
    df_scans['x_pixel'] = np.array([x for x in range(500)])
    df_scans['x_pixel'] += np.random.normal(0, 0.1, 500)
    config = get_example_config()
    experiment = ctrappy.scan.Experiment(df_scans, config,
                                         laser_colors='rgb', dna_length_kbp=100,
                                         redo_step_fitting=True)
    result = ctrappy.gui_cpa_figs.get_anom_diff_fit_log(experiment, sigma_error=0.1)
    assert 0.5 < result['anomalous_diffusion_coefficient'][0] <= 2.0  # D approx 1
    assert 1.5 < result['anomalous_diffusion_exponent'][0] <= 2.0  # alpha approx 2

    # Random walk.
    df_scans = get_example_scans(n_frames=500)
    np.random.seed(0)
    df_scans['x_pixel'] = 50 + np.cumsum(np.random.normal(0, 5, 500))
    df_scans['x_pixel'] += np.random.normal(0, 0.1, 500)
    config = get_example_config()
    experiment = ctrappy.scan.Experiment(df_scans, config,
                                         laser_colors='rgb', dna_length_kbp=100,
                                         redo_step_fitting=True)
    result = ctrappy.gui_cpa_figs.get_anom_diff_fit_log(experiment, sigma_error=0.01)
    assert 12 < result['anomalous_diffusion_coefficient'][0] <= 120  # D approx 25
    assert 0.5 < result['anomalous_diffusion_exponent'][0] <= 1.5  # alpha approx 1
