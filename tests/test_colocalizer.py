import ctrappy.tracking
import numpy as np
import pandas as pd


def test_colocalizer():

    # Crosstalk matrix
    cs_mat = np.zeros((3, 3, 3))
    cs_mat[1, 0, 0] = 5  # Red leaks 5% of its intensity into green; RG colocalizations cause green intensity correction.
    cs_mat[1, 2, 2] = 1  # Blue leaks 1% of its intensity into green; GB colocalizations cause green intensity correction.

    # Colocalizer objects; test for two coloc distances
    col0 = ctrappy.tracking.Colocalizer(colocalization_distance=0.3,
                                        crosstalk_matrices=cs_mat,
                                        colocalization_frames=3,
                                        cf_res=0.05)
    col1 = ctrappy.tracking.Colocalizer(colocalization_distance=0.6,
                                        crosstalk_matrices=cs_mat,
                                        colocalization_frames=3,
                                        cf_res=0.05)

    # Input data.
    data = {
        'frame':        [0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3],
        'color_str':    ['r', 'r', 'r', 'r', 'g', 'g', 'g', 'g', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b'],
        'color_id':     [0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2],
        'intensity':    [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100],
        'x_pixel':      [0, 0, 0, 0, 8, 8, 8, 8, 10, 10, 10, 10, 9, 9, 9, 9],
        'y_pixel':      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        'track_id':     [0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 8, 8, 8, 8]
    }
    df_in = pd.DataFrame(data)

    # Check if results are correct.
    df_out = col0.colocalize(df_in, 'rgb')
    df_r = df_out[df_out['color_str'] == 'r']
    df_g = df_out[df_out['color_str'] == 'g']
    df_b0 = df_out[df_out['track_id'] == 2]
    df_b1 = df_out[df_out['track_id'] == 8]
    assert (~df_r['colocalized']).all()
    assert df_g['colocalized'].all()
    assert (~df_b0['colocalized']).all()
    assert df_b1['colocalized'].all()
    assert (df_g['corrected_intensity'] == 99).all()

    df_out = col1.colocalize(df_in, 'rgb')
    df_g = df_out[df_out['color_str'] == 'g']
    df_rgb1 = df_out[df_out['track_id'] != 2]
    df_b0 = df_out[df_out['track_id'] == 2]
    assert df_rgb1['colocalized'].all()
    assert (~df_b0['colocalized']).all()
    assert (df_g['corrected_intensity'] == 94).all()
test_colocalizer()
