import ctrappy.scan
import pandas as pd
import numpy as np


def get_example_scans():
    scans_dict = {'file_name': ['test1', 'test1', 'test1', 'test1', 'test2', 'test2'],
                  'scan_id': [0, 0, 0, 0, 1, 1],
                  'trace_id': [0, 0, 1, 1, 0, 0],
                  'track_id': [0, 0, 1, 1, 0, 0],
                  'color_id': [0, 0, 1, 1, 2, 2],
                  'color_str': ['r', 'r', 'g', 'g', 'b', 'b'],
                  'frame': [0, 1, 0, 1, 0, 1],
                  'intensity': [200, 200, 100, 100, 25, 25],
                  'corrected_intensity': [200, 200, 100, 100, 25, 25],
                  'x_pixel': [9, 9, 10, 10, 15, 15],
                  'y_pixel': [5, 5, 5, 5, 5, 5],
                  'dna_start_pixel': [0, 0, 0, 0, 0, 0],
                  'dna_end_pixel': [20, 20, 20, 20, 20, 20],
                  'n_scans': [2, 2, 2, 2, 2, 2],
                  'n_frames': [2, 2, 2, 2, 2, 2]}
    return pd.DataFrame(scans_dict)


def get_example_config():
    config_dict = {'min_step_sizes_rgb': [50, 50, 50],
                   'avg_step_sizes_rgb': [100, 100, 100],
                   'confocal_pixel_size': 0.05,
                   'step_counting_divide': True,
                   'colocalization_distance': 0.2,
                   'bleed_through': np.zeros((3, 3, 3))}
    return config_dict


def test_experiment_step_fitting_divide():
    """Test experiment initialization with step_counting_divide=True and redo_step_fitting=True;
    step count should be equal to 2 for a spot of 200ADU intensity, 1 for a spot of 100ADU intensity,
    and 0 for a spot with 25ADU intensity."""
    df_scans = get_example_scans()
    config = get_example_config()
    experiment = ctrappy.scan.Experiment(df_scans, config, 'rgb',
                                         dna_length_kbp=10, redo_step_fitting=True)
    df_experiment = experiment.to_dataframe()
    assert df_experiment[df_experiment['color_str'] == 'r']['step_count'].iloc[0] == 2.0
    assert df_experiment[df_experiment['color_str'] == 'g']['step_count'].iloc[0] == 1.0
    assert df_experiment[df_experiment['color_str'] == 'b']['step_count'].iloc[0] == 0.0


def test_experiment_step_fitting_no_divide():
    """Test experiment initialization with step_counting_divide=False and redo_step_fitting=True;
    step count should be equal to 1 for a spot of 200ADU intensity, 1 for a spot of 100ADU intensity,
    and 0 for a spot with 25ADU intensity."""
    df_scans = get_example_scans()
    config = get_example_config()
    config['step_counting_divide'] = False
    experiment = ctrappy.scan.Experiment(df_scans, config, 'rgb',
                                         dna_length_kbp=10, redo_step_fitting=True)
    df_experiment = experiment.to_dataframe()
    assert df_experiment[df_experiment['color_str'] == 'r']['step_count'].iloc[0] == 1.0
    assert df_experiment[df_experiment['color_str'] == 'g']['step_count'].iloc[0] == 1.0
    assert df_experiment[df_experiment['color_str'] == 'b']['step_count'].iloc[0] == 0.0


def test_experiment_rerun_colocalization():
    """Test rerun_colocalization() - the experiment trace_ids should all be 0 after running that function."""
    df_scans = get_example_scans()
    config = get_example_config()
    experiment = ctrappy.scan.Experiment(df_scans, config, 'rgb',
                                         dna_length_kbp=10, redo_step_fitting=True)
    experiment = ctrappy.scan.rerun_colocalization(experiment)
    df_after = experiment.to_dataframe()
    assert df_after['trace_id'].unique() == np.array([0])
    assert df_after[df_after['scan_id'] == 0]['colocalized'].unique() == np.array([True])
