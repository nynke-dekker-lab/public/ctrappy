"""
Code for opening and saving files.
"""
import pandas as pd
from lumicks import pylake
from ctrappy.image import Image
from skimage import io
import numpy as np
import yaml
from datetime import datetime
import base64
import os
from jsonschema import validate

SCHEMA_DIR = os.path.join(os.path.dirname(__file__), 'schemas')


def read_yaml(path, schema=None):
    """Read yaml file from path. Allowed schemas are 'gui_defaults',
    'ctrappy_configuration', 'tiff_metadata', 'tracking_params'."""
    with open(path) as f:
        params = yaml.load(f, Loader=yaml.FullLoader)
    if schema is None:
        return params
    elif schema == 'gui_defaults':
        schema_path = os.path.join(SCHEMA_DIR, 'gui_schema.yml')
        validate(instance=params, schema=read_yaml(schema_path))
        return params
    elif schema == 'ctrappy_configuration':
        schema_path = os.path.join(SCHEMA_DIR, 'config_schema.yml')
        validate(instance=params, schema=read_yaml(schema_path))
        return params
    elif schema == 'tiff_metadata':
        schema_path = os.path.join(SCHEMA_DIR, 'metadata_schema.yml')
        validate(instance=params, schema=read_yaml(schema_path))
        return params
    elif schema == 'tracking_params':
        schema_path = os.path.join(SCHEMA_DIR, 'tracking_params_schema.yml')
        validate(instance=params, schema=read_yaml(schema_path))
        return params
    else:
        raise(ValueError(f"Schema '{schema}' not found."))


def read_config(path, schema='ctrappy_configuration'):
    """Read config yaml file from path."""
    return read_yaml(path, schema)


def get_avg_quantity_from_h5file(quantity, start, stop):
    """Read a quantity from an h5 file between timestamps start/stop; take the mean."""
    i0 = np.argmin(np.absolute(quantity.timestamps - start))
    i1 = np.argmin(np.absolute(quantity.timestamps - stop))
    x = quantity.data[i0:i1]
    x = x[x != 0]
    if len(x) != 0:
        return x.mean()
    else:
        return np.nan


def get_full_quantity_from_h5file(quantity, start, stop):
    """Read a quantity from an h5 file between timestamps start/stop."""
    i0 = np.argmin(np.absolute(quantity.timestamps - start))
    i1 = np.argmin(np.absolute(quantity.timestamps - stop))
    return quantity.data[i0:i1]


def get_scan_force_distance_data(h5file, scan, raise_errors=True):
    """Get the force and distance data from an h5 file during a scan; output as DataFrame."""
    try:
        scan_bead1 = get_full_quantity_from_h5file(h5file['Bead position']['Bead 1 X'], scan.start, scan.stop)
        scan_bead2 = get_full_quantity_from_h5file(h5file['Bead position']['Bead 2 X'], scan.start, scan.stop)
        scan_force1 = get_full_quantity_from_h5file(h5file['Force LF']['Force 1x'], scan.start, scan.stop)
        scan_force2 = get_full_quantity_from_h5file(h5file['Force LF']['Force 2x'], scan.start, scan.stop)
        dist = get_full_quantity_from_h5file(h5file.distance1, scan.start, scan.stop)

        timestamp = h5file['Force LF']['Force 1x'].timestamps
        i0 = np.argmin(np.absolute(timestamp - scan.start))
        i1 = np.argmin(np.absolute(timestamp - scan.stop))
        timestamp = timestamp[i0:i1]
        t = (timestamp - np.amin(timestamp)) / 1e9
    except Exception as e:
        if raise_errors:
            raise Exception("Error reading force/distance data from H5 file: " + str(e))
        else:
            timestamp = np.array([])
            t = np.array([])
            scan_bead1 = np.array([])
            scan_bead2 = np.array([])
            scan_force1 = np.array([])
            scan_force2 = np.array([])
            dist = np.array([])
    data = {'timestamp': timestamp,
            'time_s': t,
            'x_bead1': scan_bead1,
            'x_bead2': scan_bead2,
            'F_bead1': scan_force1,
            'F_bead2': scan_force2,
            'dist': dist}
    # Deal with aborted scan, where data elements do not have the same length.
    n_datapoints = np.inf
    for _, y in data.items():
        n_datapoints = min(n_datapoints, len(y))
    for x, y in data.items():
        data[x] = data[x][:n_datapoints]
    return pd.DataFrame(data)


def get_scan_metadata(h5file, scan, raise_errors=True):
    """Get scan metadata from an h5file; output as dictionary."""
    try:
        dist = float(get_avg_quantity_from_h5file(h5file.distance1, scan.start, scan.stop))
    except Exception as e:
        if raise_errors:
            raise Exception("Error reading distance data from H5 file, data does not exist: " + str(e))
        else:
            dist = None
    try:
        half_image = scan.pixels_per_line * 0.05 / 2
        bead1 = get_avg_quantity_from_h5file(h5file['Bead position']['Bead 1 X'], scan.start, scan.stop)
        bead2 = get_avg_quantity_from_h5file(h5file['Bead position']['Bead 2 X'], scan.start, scan.stop)
        center = scan.center_point_um['x']
        x0_bead = float(half_image + min(bead1, bead2) - center)
        x1_bead = float(half_image + max(bead1, bead2) - center)
        bead1y = get_avg_quantity_from_h5file(h5file['Bead position']['Bead 1 Y'], scan.start, scan.stop)
        bead2y = get_avg_quantity_from_h5file(h5file['Bead position']['Bead 2 Y'], scan.start, scan.stop)
    except Exception as e:
        if raise_errors:
            raise Exception("Error reading bead position & scan volume center data from H5 file: " + str(e))
        else:
            x0_bead = 0
            x1_bead = 0
            bead1 = 0
            bead2 = 0
            bead1y = 0
            bead2y = 0
            center = 0
    try:
        power_r = float(h5file["Confocal diagnostics"]["Excitation Laser Red"].data.mean())
        power_g = float(h5file["Confocal diagnostics"]["Excitation Laser Green"].data.mean())
        power_b = float(h5file["Confocal diagnostics"]["Excitation Laser Blue"].data.mean())
    except Exception:
        try:
            power_r = float(h5file["Confocal diagnostics"]["Excitation Laser 638 nm"].data.mean())
            power_g = float(h5file["Confocal diagnostics"]["Excitation Laser 561 nm"].data.mean())
            power_b = float(h5file["Confocal diagnostics"]["Excitation Laser 488 nm"].data.mean())
        except Exception as e:
            if raise_errors:
                raise Exception("Error reading laser powers from H5 file: " + str(e))
            else:
                power_r = None
                power_g = None
                power_b = None
    try:
        start_t = datetime.utcfromtimestamp(scan.start / 1e9).strftime('%Y-%m-%d %H:%M:%S')
    except Exception:
        if raise_errors:
            raise Exception("Error reading scan start time from H5 file: data not found.")
        else:
            start_t = None
    output = {'DNA length (micron)': dist,
              'bead0 center (micron)': x0_bead,
              'bead1 center (micron)': x1_bead,
              'red laser power (%)': power_r,
              'green laser power (%)': power_g,
              'blue laser power (%)': power_b,
              'start time': start_t,
              'raw bead1 position x (micron)': float(bead1),
              'raw bead1 position y (micron)': float(bead1y),
              'raw bead2 position x (micron)': float(bead2),
              'raw bead2 position y (micron)': float(bead2y),
              'raw image width (pixels)': int(scan.pixels_per_line),
              'raw DNA center position (micron)': float((bead1 + bead2) / 2),
              'raw scan volume center x (micron)': float(center),
              'raw scan volume center y (micron)': float(scan.center_point_um['y']),
              'estimated brightfield confocal shift (micron)': float(center - (bead1 + bead2) / 2)}
    return output


def read_h5file(path):
    """Read scan images from h5 file.

    Parameters
    ----------
    path : str
        Path to file.

    Returns
    -------
    h5file : pylake.File
        Pylake file object.
    output : dict
        Dict with scan names (keys) and Scan objects (values).
    """
    try:
        h5file = pylake.File(path)
    except OSError:
        return None
    output = dict()
    for name, scan in h5file.scans.items():
        ctrap_scan = Image(name, scan, get_scan_metadata(h5file, scan), get_scan_force_distance_data(h5file, scan))
        if ctrap_scan.n_frames > 0 and ctrap_scan.name is not None:
            try:
                name_nr = int(name)
            except Exception:
                raise Exception("Error: scan name needs to be an integer number (sorry!)")
            output[name_nr] = ctrap_scan
    return h5file, output


def read_biotiff(path):
    """Read scan from tiff file.

    Parameters
    ----------
    path : str
        Path to file.

    Returns
    -------
    output : scan
        Scan object.
    """
    im = io.imread(path)
    scan = type('scan_placeholder', (object,), {})()
    if len(im.shape) == 3:
        n_frames = 1
    else:
        n_frames = im.shape[0]
    scan.num_frames = n_frames
    scan.rgb_image = im
    scan.start = 0
    scan = Image(0, scan)
    return scan


def download_link(object_to_download, download_filename, download_link_text):
    """
    Generates a link to download the given object_to_download for streamlit.

    object_to_download (str, pd.DataFrame, dict):  The object to be downloaded.
    download_filename (str): filename and extension of file. e.g. mydata.csv, some_txt_output.txt, output.yaml
    download_link_text (str): Text to display for download link.

    Examples:
    download_link(YOUR_DF, 'YOUR_DF.csv', 'Click here to download data!')
    download_link(YOUR_STRING, 'YOUR_STRING.txt', 'Click here to download your text!')
    download_link(YOUR_DICT, 'YOUR_STRING.yaml', 'Click here to download your dict!')

    """
    if isinstance(object_to_download, pd.DataFrame):
        object_to_download = object_to_download.to_csv(index=False)
    if isinstance(object_to_download, dict):
        object_to_download = yaml.dump(object_to_download, default_flow_style=False, sort_keys=False)

    # some strings <-> bytes conversions necessary here
    b64 = base64.b64encode(object_to_download.encode()).decode()

    return f'<a href="data:file/txt;base64,{b64}" download="{download_filename}">{download_link_text}</a>'
