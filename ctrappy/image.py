"""
This module contains the Image class for opening a C-trap scan image.
"""

import matplotlib.pyplot as plt
from datetime import datetime
import numpy as np
import tifffile
import yaml
import ctrappy.tracking
import pandas as pd
import cv2


class Image:
    """C-trap scan image object. Initialization converts scan data to shape (frames, y, x, channels),
    also for single frame scans.

    Parameters
    ----------
    name : int or str
        Blob name; should be int or str(int).
    lumicks_scan : lumicks.pylake scan object
        Scan data.
    metadata : dict
        Scan metadata. Default: None
    df_force_distance : pandas DataFrame
        Force/distance data during the scan. Default: None
    """

    def __init__(self, name, lumicks_scan, metadata=None, df_force_distance=None):

        if metadata is None:
            self.metadata = {}
        else:
            self.metadata = metadata
        if df_force_distance is None:
            self.df_force_distance = pd.DataFrame()
        else:
            self.df_force_distance = df_force_distance
        try:
            name_nr = int(name)
        except Exception:
            raise Exception("Error: scan name needs to be an integer number (sorry!)")
        self.name = name_nr
        self.n_frames = lumicks_scan.num_frames
        try:
            self.data_raw = lumicks_scan.rgb_image
        except:
            self.data_raw = lumicks_scan.get_image()
        if self.n_frames == 1:
            self.data_raw = self.data_raw.reshape((1, self.data_raw.shape[0],
                                                   self.data_raw.shape[1], self.data_raw.shape[2]))
        self.data = self.data_raw[:, :, :, :]
        self.start_time = datetime.utcfromtimestamp(lumicks_scan.start / 1e9).strftime('%Y-%m-%d %H:%M:%S')
        self.lumicks_scan = lumicks_scan

    def __hash__(self):
        return hash(self.data)

    def to_tiff(self, filename, resolution=(1e7 / 50, 1e7 / 50, "CENTIMETER")):
        """Save to tiff."""
        if self.n_frames > 1:
            tifffile.imsave(filename, self.data, resolution=resolution)
        else:
            tifffile.imsave(filename, self.data[0, :, :, :], resolution=resolution)

    def frame(self, frame, colors=None):
        """Return a single frame.

        Parameters
        ----------
        frame : int
            Frame number.
        colors : list of 3 bools or None
            For each color (r, g, b), apply a mask. If None, use all colors.

        Returns
        -------
        frame_data : numpy array with shape (y_size_fig, x_size_fig, 3)
            Frame data.
        """
        if colors is None:
            colors = [True, True, True]
        color_mask = np.array(colors, dtype=int).reshape((1, 1, 3))
        frame_data = self.data[frame, :, :, :]
        frame_data = np.multiply(color_mask, frame_data)
        return frame_data

    def plot(self, frame, display_max):
        """Plot frame."""

        # Prepare.
        color_codes = ['r', 'g', 'b']
        height_ratio = ((self.data.shape[1]/20) / (self.data.shape[2]/140))**2
        fig, axes = plt.subplots(3, 1, figsize=(12, 8),
                                 gridspec_kw={'height_ratios': [height_ratio, 1, 1]})
        axes[0].get_shared_x_axes().join(axes[0], axes[1])
        frame_data = self.frame(frame)

        # Show frame.
        imshow_data = np.clip(frame_data, 0, display_max) / display_max
        axes[0].imshow(imshow_data, vmin=0, vmax=display_max)
        axes[0].set_xlabel('x (pixels)')
        axes[0].set_ylabel('y (pixels)')
        # axes[0].set_axis_off()

        # Show photon counts projected on x axis.
        for color in range(3):
            axes[1].plot(np.sum(frame_data[:, :, color], axis=0), c=color_codes[color])
            axes[1].set_xlabel('x (pixels)')
            axes[1].set_ylabel('proj. I (ADU)')
            axes[1].set_xlim((0, self.data.shape[2] - 1))

        # Show intensity over time.
        for color in range(3):
            axes[2].plot(np.sum(np.sum(self.data[:, :, :, color], axis=1), axis=1), c=color_codes[color])
            axes[2].set_xlabel('t (frame)')
            axes[2].set_ylabel('total I (ADU)')
            axes[2].set_xlim((0, max((self.data.shape[0] - 1), 1)))

        plt.tight_layout()
        return fig

    def apply_offset_mask(self, offset_values):
        """Apply offset mask to self.data, getting rid of any bead data to prevent spot detection on the beads."""
        offset_l = max(0, int(np.floor(offset_values[0])))
        offset_r = min(int(np.ceil(offset_values[1])), self.data.shape[2]-1)
        mask = np.zeros(self.data.shape)
        mask[:, :, offset_l:offset_r+1, :] = 1
        self.data = self.data_raw * mask

    def export(self, file_tiff, file_metadata, file_fddata):
        """Export image data to tiff, metadata to yaml and force-distance data to csv."""
        self.lumicks_scan.export_tiff(file_tiff)
        with open(file_metadata, 'w') as outfile:
            yaml.dump(self.metadata, outfile, default_flow_style=False, sort_keys=False)
        self.df_force_distance.to_csv(file_fddata)

    def export_video(self, output_path, display_max):
        """Export scan as .avi video."""
        output_img = np.clip(self.data[:, :, :, :], 0, display_max) / display_max
        size = (output_img.shape[2], output_img.shape[1])
        n_frames = output_img.shape[0]
        fps = 10
        out = cv2.VideoWriter(output_path, cv2.VideoWriter_fourcc(*'FMP4'),
                              fps, (10 * size[0], 10 * size[1]), True)
        for i in range(n_frames):
            # data = np.random.randint(0, 256, size, dtype='uint8')
            frame = np.dstack([(255 * output_img[i, :, :, 2]).astype(np.uint8),
                               (255 * output_img[i, :, :, 1]).astype(np.uint8),
                               (255 * output_img[i, :, :, 0]).astype(np.uint8)])
            frame = cv2.resize(frame, (10 * size[0], 10 * size[1]), fx=0, fy=0, interpolation=cv2.INTER_NEAREST_EXACT)
            out.write(frame)
        out.release()

    def get_spots(self, thresholds, radius, offset_values=None, frame=None, colors=None, remove_edge_spots=False):
        return get_spots(self.data, thresholds, radius, offset_values, frame, colors, remove_edge_spots)

    @staticmethod
    def track_spots(df_spots, frame_skip, spot_dist):
        return track_spots(df_spots, frame_skip, spot_dist)


def get_spots(data, thresholds, radius, offset_values=None, frame=None, colors=None, remove_edge_spots=False):
    """Detect spots in the image, given user input values for detection thresholds, radius and image offsets."""
    detector = ctrappy.tracking.Detector(radius=radius, thresholds=thresholds, overlap=0.5)
    if frame is None:
        df_spots = detector.detect(data, colors=colors)
    else:
        df_spots = detector.detect(data[frame:frame+1, :, :, :], colors=colors)
        df_spots['frame'] = frame
    if offset_values is not None:
        # Get rid of spots on the edge.
        if remove_edge_spots:
            keep_r = (df_spots['x'] >= offset_values[0] + detector.radii[0]/2) & \
                     (df_spots['x'] <= offset_values[1] - detector.radii[0]/2) & \
                     (df_spots['y'] >= detector.radii[0]/2) & \
                     (df_spots['y'] <= data.shape[1] - detector.radii[0]/2) & \
                     (df_spots['color'] == 0)
            keep_g = (df_spots['x'] >= offset_values[0] + detector.radii[1]/2) & \
                     (df_spots['x'] <= offset_values[1] - detector.radii[1]/2) & \
                     (df_spots['y'] >= detector.radii[1]/2) & \
                     (df_spots['y'] <= data.shape[1] - detector.radii[1]/2) & \
                     (df_spots['color'] == 1)
            keep_b = (df_spots['x'] >= offset_values[0] + detector.radii[2]/2) & \
                     (df_spots['x'] <= offset_values[1] - detector.radii[2]/2) & \
                     (df_spots['y'] >= detector.radii[2]/2) & \
                     (df_spots['y'] <= data.shape[1] - detector.radii[2]/2) & \
                     (df_spots['color'] == 2)
            df_spots = df_spots[keep_r | keep_g | keep_b]
        else:
            df_spots = df_spots[(df_spots['x'] >= offset_values[0]) & (df_spots['x'] <= offset_values[1])]
        # Reset frame_subindex
        for i, df_frame in df_spots.groupby(['frame', 'color']):
            subindices = np.arange(len(df_frame))
            df_spots.loc[df_frame.index.values, 'frame_subindex'] = subindices

    return df_spots


def track_spots(df_spots, frame_skip, spot_dist):
    """Track spots in df_spots dataframe, given user input values for max frame skip and max spot distance."""
    tracker = ctrappy.tracking.Tracker(skip_frames=frame_skip, max_dist=spot_dist, max_segment_dist=spot_dist)
    df_tracks = pd.DataFrame()

    for color in range(3):

        # If frame_skip and spot_dist are lists, use element [color]
        if isinstance(frame_skip, list) and isinstance(spot_dist, list):
            tracker = ctrappy.tracking.Tracker(skip_frames=frame_skip[color], max_dist=spot_dist[color],
                                               max_segment_dist=spot_dist[color])

        df_color = df_spots[df_spots['color'] == color]
        if not df_color.empty:
            df_tracks_color = tracker.track(df_color)
            df_tracks_color['color'] = color
            if not df_tracks.empty:
                df_tracks_color['track_id'] = df_tracks_color['track_id'] + \
                                                 df_tracks['track_id'].max() + 1
            df_tracks = pd.concat((df_tracks, df_tracks_color), ignore_index=True)

    if not df_tracks.empty:
        df_tracks = df_tracks.sort_values(['color', 'frame', 'track_id']).reset_index(drop=True)
    return df_tracks
