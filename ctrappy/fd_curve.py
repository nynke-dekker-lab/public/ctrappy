"""FD analysis functions"""
import streamlit as st
import numpy as np
import ruptures
import matplotlib.pyplot as plt
import pandas as pd
import lmfit
from tqdm import tqdm

# Get force offset from parameters.
def get_force_offset(params_fd_in):
    kBT = params_fd_in['kBT']
    Lp = params_fd_in['Lp']
    L_dna = params_fd_in['dna_length_kbp']
    d0 = params_fd_in['d_start']
    offset = (kBT / Lp) * \
             (0.25 * (1 - d0 / (L_dna * 0.34))**(-2) - \
              0.25 + d0 / (L_dna * 0.34))
    return offset

 # Drop nan from numpy array
def dropna(arr):
    return arr[~np.isnan(arr)]

# Read force file.
def read_force_file(filename, params_fd):
    
    #read the file
    df_fd = pd.read_csv(filename)
    Fsearch = list(df_fd.iloc[:, 5])
    IndCut = Fsearch.index(max(Fsearch))
    
    #trim dataset according to index (remove reverse data) 
    df_fd = df_fd.drop(df_fd[df_fd.iloc[:, 0] > IndCut].index)
    
    #record data without force or distance filtering
    d_raw = np.array(df_fd.iloc[:, 3])
    F_raw = np.array(df_fd.iloc[:, 5])
    
    #trim the dataset according to force and distance
    df_fd = df_fd[(df_fd.iloc[:, 3] >= params_fd['d_min'])]
    df_fd = df_fd[(df_fd.iloc[:, 3] <= params_fd['d_max'])]
    df_fd = df_fd[(df_fd.iloc[:, 5] >= params_fd['force_min'])]
    df_fd = df_fd[(df_fd.iloc[:, 5] <= params_fd['force_max'])]
    
    #remove NaN from the data
    d = dropna(np.array(df_fd.iloc[:, 3]))
    F = dropna(np.array(df_fd.iloc[:, 5]))
        
    return d, F + get_force_offset(params_fd), d_raw, F_raw

# Worm-like chain models
def WLC(d : np.ndarray, kBT : float, Lc : float, Lp : float) -> np.ndarray:
    r"""Worm-like chain model.

    .. math::
        F = \frac{k_BT}{L_p} \left[\frac{1}{4\left(1-\frac{d}{L_c}\right)^2}-\frac{1}{4}+\frac{d}{L_c}\right]

    Parameters
    ----------
    d : array-like
        Distance between end-points. Units: [um]
    kBT : float
        Boltzman contant times Temperature. Units: [pN*nm]
    Lc : float 
        Contour length. Units: [nm]
    Lp : float
        Persistance length. Units: [nm]

    Outputs
    -------
    F : array-like
        Required force to extend a worm-like chain. Units: [pN]

    Bustamante C, Marko JF, Siggia ED, Smith S. 
    Entropic elasticity of lambda-phage DNA. 
    Science. 1994 Sep 9;265(5178):1599-600. 
    doi: 10.1126/science.8079175. PMID: 8079175.
    """
    # Transform units: [um] to [nm]
    d = d*1000
    
    return (kBT/Lp)*(0.25/(1-d/Lc)**2 - 0.25 + d/Lc)

def bouchiat(d : np.ndarray, kBT : float, Lc : float, Lp : float) -> np.ndarray:
    r"""Bouchiat et al. worm-like chain model with seventh order correction.

    .. math::
        F = \frac{k_BT}{L_p} \left[\frac{1}{4\left(1-\frac{d}{L_c}\right)^2}-\frac{1}{4}+\frac{d}{L_c} +\sum_{n=1}^7 \alpha_n \left(\frac{d}{L_c}\right)^n\right]

    Parameters
    ----------
    d : array-like
        Distance between end-points. Units: [um]
    kBT : float
        Boltzman contant times Temperature. Units: [pN*nm]
    Lc : float 
        Contour length. Units: [nm]
    Lp : float
        Persistance length. Units: [nm]

    Outputs
    -------
    F : array-like
        Required force to extend a worm-like chain. Units: [pN]

    C. Bouchiat, M.D. Wang, J.-F. Allemand, T. Strick, S.M. Block, V. Croquette
    Estimating the Persistence Length of a Worm-Like Chain Molecule from Force-Extension Measurements
    Biophysical Journal
    """
    # Transform units: [um] to [nm]
    d = d*1000
    
    # Correction coefficients
    alpha = np.array([-0.5164228, -2.737418, 16.07497, -38.87607, 39.49944, -14.17718])

    # Compute correction
    corr = 0
    for n in range(len(alpha)): corr += alpha[n]*(d/Lc)**(n+2) 
    
    return (kBT/Lp)*(0.25/(1-d/Lc)**2 - 0.25 + d/Lc + corr)

def odijk(F : np.ndarray, kBT : float, Lc : float, Lp : float, S : float) -> np.ndarray:
    r"""Odidjk worm-like chain model.

    .. math::
        d = L_c \left( 1 - \frac{1}{2}\sqrt{\frac{k_BT}{FL_p}} + \frac{F}{S}

    Parameters
    ----------
    F : array-like
        Required force to extend a worm-like chain. Units: [pN]
    kBT : float
        Boltzman contant times Temperature. Units: [pN*nm]
    Lc : float 
        Contour length. Units: [nm]
    Lp : float
        Persistance length. Units: [nm]
    S : float
        Stretch modulus. Units: [pN]

    Outputs
    -------
    d : array-like
        Distance between end-points. Units: [um]

    Odijk, T. 
    Stiff Chains and Filaments under Tension 
    Macromolecules 1995 28 (20) 
    7016-7018 doi: 10.1021/ma00124a044
    """

    return Lc*(1-0.5*(kBT/(F*Lp))**0.5+F/S)/1000

def extbouchiat(fparams : lmfit.Parameters, F : np.ndarray, d : np.ndarray) -> np.ndarray:
    r"""Modified Bouchiat et al. worm-like chain model with seventh order correction.

    .. math::
        F = \frac{k_BT}{L_p} \left[\frac{1}{4\left(1-\frac{d}{L_c}+\frac{F}{S}\right)^2}-\frac{1}{4}+\frac{d}{L_c}-\frac{F}{S} +\sum_{n=1}^7 \alpha_n \left(\frac{d}{L_c} - \frac{F}{S} \right)^n\right]

    Parameters
    ----------
    fparams : Parameters
        Fitting parameters such as:
            kBT : float
                Boltzman contant times Temperature. Units: [pN*nm]
            Lc : float 
                Contour length. Units: [nm]
            Lp : float
                Persistance length. Units: [nm]
            S : float
                Stretch modulus. Units: [pN]
    F : array-like
        Required force to extend a worm-like chain. Units: [pN]
    d : array-like
        Distance between end-points. Units: [um]


    Outputs
    -------
    residual : array-like
        Residual to minimize for fitting. Units: [pN]

    C. Bouchiat, M.D. Wang, J.-F. Allemand, T. Strick, S.M. Block, V. Croquette
    Estimating the Persistence Length of a Worm-Like Chain Molecule from Force-Extension Measurements
    Biophysical Journal
    """
    # Transform units: [um] to [nm]
    d = d*1000
    # Unpack parameters: extract .value attribute for each parameter
    fparamsvals = fparams.valuesdict()
    kBT = fparamsvals['kBT']
    Lc = fparamsvals['Lc']
    Lp = fparamsvals['Lp']
    S = fparamsvals['S']

    # Compute normalized extension
    l = d/Lc - F/S
    
    # Correction coefficients
    alpha = np.array([-0.5164228, -2.737418, 16.07497, -38.87607, 39.49944, -14.17718])

    # Compute correction
    corr = 0
    for n in range(len(alpha)): corr += alpha[n]*(l)**(n+2) 
    
    return (kBT/Lp)*(0.25/(1-l)**2 - 0.25 + l + corr)

def res_extbouchiat(fparams : lmfit.Parameters, F : np.ndarray, d : np.ndarray) -> np.ndarray:
    r"""Modified Bouchiat et al. worm-like chain model with seventh order correction.

    .. math::
        F = \frac{k_BT}{L_p} \left[\frac{1}{4\left(1-\frac{d}{L_c}\right)^2}-\frac{1}{4}+\frac{d}{L_c} +\sum_{n=1}^7 \alpha_n \left(\frac{d}{L_c}\right)^n\right]

    Parameters
    ----------
    F : array-like
        Required force to extend a worm-like chain. Units: [pN]
    d : array-like
        Distance between end-points. Units: [um]
    fparams : Parameters
        Fitting parameters such as:
            kBT : float
                Boltzman contant times Temperature. Units: [pN*nm]
            Lc : float 
                Contour length. Units: [nm]
            Lp : float
                Persistance length. Units: [nm]
            S : float
                Stretch modulus. Units: [pN]

    Outputs
    -------
    residual : array-like
        Residual to minimize for fitting. Units: [pN]

    C. Bouchiat, M.D. Wang, J.-F. Allemand, T. Strick, S.M. Block, V. Croquette
    Estimating the Persistence Length of a Worm-Like Chain Molecule from Force-Extension Measurements
    Biophysical Journal
    """
    # Transform units: [um] to [nm]
    d = d*1000
    # Unpack parameters: extract .value attribute for each parameter
    fparamsvals = fparams.valuesdict()
    kBT = fparamsvals['kBT']
    Lc = fparamsvals['Lc']
    Lp = fparamsvals['Lp']
    S = fparamsvals['S']

    # Compute normalized extension
    l = d/Lc - F/S
    
    # Correction coefficients
    alpha = np.array([-0.5164228, -2.737418, 16.07497, -38.87607, 39.49944, -14.17718])

    # Compute correction
    corr = 0
    for n in range(len(alpha)): corr += alpha[n]*(l)**(n+2) 
    
    return F - (kBT/Lp)*(0.25/(1-l)**2 - 0.25 + l + corr)

def extWLC(fparams : lmfit.Parameters, F : np.ndarray, d : np.ndarray) -> np.ndarray:
    r"""Modified worm-like chain model.

    .. math::
        F = \frac{k_BT}{L_p} \left[\frac{1}{4\left(1-\frac{d}{L_c} + \frac{F}{S}\right)^2}-\frac{1}{4}+\frac{d}{L_c} - \frac{F}{S}\right]

    Parameters
    ----------
    fdata : tuple
        Observations of distance [um] and force [pN], respectively.
    kBT : float
        Boltzman contant times Temperature. Units: [pN*nm]
    Lc : float 
        Contour length. Units: [nm]
    Lp : float
        Persistance length. Units: [nm]
    S : float
        Stretch modulus. Units: [pN]

    Outputs
    -------
    F : array-like
        Required force to extend a worm-like chain. Units: [pN]

    Peijing Jeremy Wang, Andrei Chabes, Rocco Casagrande, X. Cindy Tian, Lars Thelander & Tim C. Huffaker (1997) 
    Rnr4p, a Novel Ribonucleotide Reductase Small-Subunit Protein, Molecular and Cellular Biology, 17:10, 6114-6121, 
    DOI: 10.1128/MCB.17.10.6114
    """
    # Transform units: [um] to [nm]
    d = d*1000
    # Unpack parameters: extract .value attribute for each parameter
    fparamsvals = fparams.valuesdict()
    kBT = fparamsvals['kBT']
    Lc = fparamsvals['Lc']
    Lp = fparamsvals['Lp']
    S = fparamsvals['S']

    # Compute normalized extension
    l = d/Lc - F/S
    
    return (kBT/Lp)*(0.25/(1-l)**2 - 0.25 + l)

def res_extWLC(fparams : lmfit.Parameters, F : np.ndarray, d : np.ndarray) -> np.ndarray:
    r"""Modified worm-like chain model.

    .. math::
        res = F - \frac{k_BT}{L_p} \left[\frac{1}{4\left(1-\frac{d}{L_c} + \frac{F}{S}\right)^2}-\frac{1}{4}+\frac{d}{L_c} - \frac{F}{S}\right]

    Parameters
    ----------
    fdata : tuple
        Observations of distance [um] and force [pN], respectively.
    kBT : float
        Boltzman contant times Temperature. Units: [pN*nm]
    Lc : float 
        Contour length. Units: [nm]
    Lp : float
        Persistance length. Units: [nm]
    S : float
        Stretch modulus. Units: [pN]

    Outputs
    -------
    F : array-like
        Required force to extend a worm-like chain. Units: [pN]

    Peijing Jeremy Wang, Andrei Chabes, Rocco Casagrande, X. Cindy Tian, Lars Thelander & Tim C. Huffaker (1997) 
    Rnr4p, a Novel Ribonucleotide Reductase Small-Subunit Protein, Molecular and Cellular Biology, 17:10, 6114-6121, 
    DOI: 10.1128/MCB.17.10.6114
    """
    # Transform units: [um] to [nm]
    d = d*1000
    # Unpack parameters: extract .value attribute for each parameter
    fparamsvals = fparams.valuesdict()
    kBT = fparamsvals['kBT']
    Lc = fparamsvals['Lc']
    Lp = fparamsvals['Lp']
    S = fparamsvals['S']

    # Compute normalized extension
    l = d/Lc - F/S
    
    return F - (kBT/Lp)*(0.25/(1-l)**2 - 0.25 + l)

# Class for fitting
class WormLikeChain:
    def __init__(self, model : str = "odijk") -> None:
        """Estimating the main parameters of a Worm-Like Chain Molecule from Force-Extension Measurements.
        
        Parameters
        ----------
        method : str
            Worm-like chain model for fitting. Options are: 'WLC', 'bouchiat', and 'odijk'.
        """
        # Store model functions
        if model == "WLC":
            self.func = WLC
        elif model == "bouchiat":
            self.func = bouchiat
        elif model == "odijk":
            self.func = odijk
        elif model == "extWLC":
            self.func = extWLC
            self.resfunc = res_extWLC
        elif model == "extbouchiat":
            self.func = extbouchiat
            self.resfunc = res_extbouchiat
        else:
            raise ValueError("Unknown fitting model. Available models are: 'WLC', 'extWLC', 'bouchiat', 'extbouchiat' and 'odijk'.")
        
        # Create fitting model
        self.fmodel = lmfit.Model(self.func)
        self.model = model

    def __repr__(self):
        """Set model representation."""
        # Recover the employed method
        return self.func.__doc__
    
    def compile(self, params : dict) -> None:
        """Compile fitting model based on parameters dictionary.
        
        Parameters
        ----------
        params : dict
            Dictionary with initial values and bounds for model parameters."""
        
        # Store general parameters
        KB = 0.013806 # Boltzmann constant in pN*nm*K-1
        if ("kBT" in params) & ("T" not in params): 
            self.fmodel.set_param_hint('kBT', value=params["kBT"])
        elif ("kBT" not in params) & ("T" in params):
            self.fmodel.set_param_hint('kBT', value=KB*(273.15 + params["T"]))
        else:
            if params["kBT"] == KB*(273.15 + params["T"]):
                self.fmodel.set_param_hint('kBT', value=params["kBT"])
            else:
                raise ValueError("Please provide either T or kBT. They are contradictory")
        
        # Store DNA parameters
        self.fmodel.set_param_hint('Lc', value=params['Lc'], min=params['Lc_lower'], max=params['Lc_upper'])
        self.fmodel.set_param_hint('Lp', value=params['Lp'], min=params['Lp_lower'], max=params['Lp_upper'])
        if (self.model == "odijk") or (self.model == "extWLC") or (self.model == "extbouchiat"):
            self.fmodel.set_param_hint('S', value=params['S'], min=params['S_lower'], max=params['S_upper'])

        # Create parameters
        self.fparams = self.fmodel.make_params()

        # keep kBT constant
        self.fparams['kBT'].vary = False
    
    def fit(self, data : tuple, min_delta : float, max_iters : int, filename : str = None, method : str = "leastsq" , verbose : bool = False) -> pd.DataFrame:
        """Worm-Like Chain Molecule fitting from Force-Extension Measurements.
        
        Parameters
        ----------
        data : tuple
            Observations of distance [um] and force [pN], respectively.
        min_delta : float 
            Stop fitting when increment in Lp is lower than min_delta. Units: [nm]
        max_iters : int
            Stop fitting when reach max_iters
        method : str
            Name of minimization method to use (default is "leastsq").
        filename : str
            Name of the file conatining the data. It corresponds to plot title.
        verbose : bool
            If True, a progress bar shows the progress

        Outputs
        -------
        df_file : pd.DataFrame
            Optimal results after fitting.
        """
        # Save data
        d, F = data

        # Empty dataframe to store results.
        df_full = pd.DataFrame(columns =['Lc[nm]', 'Lp[nm]','S[pN]', 'Chisqr', 'filename', 'iter', 'dLp[nm]'])

        # Stopping parameters
        dLp = np.infty
        i = 0

        # Start fitting
        with tqdm(total=max_iters, disable = not verbose) as pbar:
            while (dLp > min_delta) and (i < max_iters):
                # Fit the model
                if self.model == "odijk":
                    self.result = self.fmodel.fit(d, self.fparams, F=F, method=method)
                    Lc, Lp, S, Chisqr = self.result.params['Lc'].value, self.result.params['Lp'].value, self.result.params['S'].value, self.result.chisqr
                elif (self.model == "extWLC") or (self.model == "extbouchiat"):
                    self.result = lmfit.minimize(self.resfunc, self.fparams, args=(F, d), method=method)
                    Lc, Lp, S, Chisqr = self.result.params['Lc'].value, self.result.params['Lp'].value, self.result.params['S'].value, self.result.chisqr                   
                else:
                    self.result = self.fmodel.fit(F, self.fparams, d=d, method=method)
                    Lc, Lp, S, Chisqr = self.result.params['Lc'].value, self.result.params['Lp'].value, None, self.result.chisqr

                # Check convercence
                if i > 0:
                    dLp = np.abs(Lp-df_full.loc[i-1, 'Lp[nm]'])

                df_res = pd.DataFrame({'Lc[nm]' : [Lc],
                                        'Lp[nm]' : [Lp],
                                        'S[pN]' : [S],
                                        'Chisqr' : [Chisqr],
                                        'filename' : [filename],
                                        'iter' : [i + 1],
                                        'dLp[nm]' : [dLp]})
                
                if (Lc <= self.fparams["Lc"].min) | (Lc >= self.fparams["Lc"].max):
                    print(f'Fitted Lc is out of bounds in iter {i}, data filtered out')
                elif (Lp <= self.fparams["Lp"].min) | (Lp >= self.fparams["Lp"].max):
                    print('Fitted Lp is out of bounds, data filtered out')
                else:
                    # print('Final Lc, Lp, S values within filtering bounds')
                    df_full = pd.concat((df_full, df_res), ignore_index=True)

                # Update Lp, Lc, S (conditional) and iter
                self.fparams["Lp"].set(Lp)
                self.fparams["Lc"].set(Lc)
                if (self.model == "odijk") or (self.model == "extWLC") or (self.model == "extbouchiat"):
                    self.fparams["S"].set(S)
                i += 1

                # Update the progress bar
                pbar.update(1)

            # Complete the progress bar
            pbar.update(max_iters-i)

            # Save fitted values per file
            df_file = pd.DataFrame([df_full[['Lc[nm]', 'Lp[nm]', 'S[pN]', "Chisqr", 'iter', 'filename']].values[-1]], columns=['opt_Lc[nm]', 'opt_Lp[nm]', 'opt_S[pN]', "Chisqr", 'nFittings', 'filename'])

            # Save fitting progress as attribute
            self.df_full = df_full
                
            return df_file
        
    def plot(self, data : tuple, filename : str = None):
        """Plot fitting results compare to observations.
        
        Parameters
        ----------
        data : tuple
            Observations of distance [um] and force [pN], respectively.
        """
        # Save data
        d, F = data

        # Update font size
        plt.rcParams.update({'font.size': 25})

        # Create figure
        fig, ax = plt.subplots(figsize=(20, 10))

        # if filename is not None:
        #     fig.suptitle('FD curve fitting (%s)' %filename)

        # Set labels
        ax.set_xlabel(r"distance [$\mu m$]")
        ax.set_ylabel(r"force [pN]")

        # Plot observations
        ax.scatter(d, F, s=15, c='black', label="data")

        # Plot best fit
        if self.model == "odijk":
            ax.plot(self.result.best_fit, F, c = 'red', lw = 2.5, label=self.model)
        elif (self.model == "extbouchiat") or (self.model == "extWLC"):
            ax.plot(d, self.func(self.fparams, F, d), c = 'red', lw = 2.5, label=self.model)
        else:
            ax.plot(d, self.result.best_fit, c = 'red', lw = 2.5, label=self.model)
        plt.legend()
        plt.show()

        return fig

    def stats(self):
        """Show fitting results."""
        return self.result.fit_report()
    
    def plot_residuals(self, data : tuple):
        """Show residuals as a function of force or distance.
        
        Parameters
        ----------
        data : tuple
            Observations of distance [um] and force [pN], respectively.
        """
        
        # Save data
        d, F = data

        # Determine nice limits by hand
        def bins(x, y):
            binwidth = 0.25
            xymax = max(np.max(np.abs(x)), np.max(np.abs(y)))
            lim = (int(xymax/binwidth) + 1) * binwidth
            return np.arange(-lim, lim + binwidth, binwidth)

        # Create figure
        fig, axs = plt.subplots(1, 2, width_ratios=(4, 1), sharey = True)

        # Set labels
        if self.method == "odijk":
            axs[0].set_ylabel(r"residuals [$\mu m$]")
            axs[0].set_xlabel(r"force [pN]")
            axs[0].scatter(F, self.result.residual, s=2.5)
        else:
            axs[0].set_xlabel(r"distance [$\mu m$]")
            axs[0].set_ylabel(r"residuals [pN]")
            axs[0].scatter(d, self.result.residual, s=2.5)
        
        axs[1].hist(self.result.residual, orientation="horizontal")
        axs[1].set_xlabel("counts")

#Define change point finder.
def changepoint_analysis(data_x, data_y, min_segment_size, penalty):
    #TODO: min_segment_size not used.
    
    # Changepoint detection.
    est = ruptures.Window(width=10, model='l2', jump=1)
    signal = np.array(data_y).reshape((len(data_y), 1))
    pred = [0] + est.fit_predict(signal, pen=np.log(len(data_y)) * penalty)
    
    # Linear fits between changepoints
    x_fit = []
    y_fit = []
    for i in range(len(pred) - 1):
        x_seg = np.array(data_x)[pred[i]:pred[i + 1]]
        y_seg = np.array(data_y)[pred[i]:pred[i + 1]]
        x_fit.append(x_seg)
        y_fit.append([np.mean(y_seg) for _ in x_seg])
        
    # Calculate residuals, step increments and breaking forces.
    residuals = np.array(data_y) - np.array([y for sublist in y_fit for y in sublist])
    delta_Lc = np.diff(np.array([y[0] for y in y_fit]))
    if len(pred) > 2:
        breaking_forces = data_x[np.array(pred[1:-1]) - 1]
    else:
        breaking_forces = np.array([])
    
    return x_fit, y_fit, breaking_forces, delta_Lc, residuals

# Plot FD fitting
def plot_fd_fit(filename, dori, Fori, Lc, Lp, S, params_fd):
    fig, ax = plt.subplots(figsize=(25, 10))
    fig.suptitle('FD curve fitting (%s)' %filename)
    
    # Plot the original data
    ax.scatter(dori, Fori,s=10,c='black')

    # Plot the fitted curve
    x = np.linspace(params_fd['force_min'], params_fd['force_max'], 100)
    y = Lc*(1-0.5*(params_fd['kBT']/(x*Lp))**0.5+x/S)/1000
    ax.plot(y, x, c='red', lw = 10)
    # Plot settings
    ax.set_xlim(params_fd['dis_lim'])
    ax.set_xlabel('Distance [um]')
    ax.set_ylabel('Force [pN]')
    plt.show()
    plt.close()

    return fig

# Plot fd analysis.
def plot_Lc_analysis(filename, dori, Fori, F, Lc, x_fit, y_fit):
    fig,(left, right) = plt.subplots(1, 2)
    fig.suptitle('Contour length analysis (%s)' %filename)
    fig.set_figheight(10)
    fig.set_figwidth(25)
    left.plot(dori,Fori, c = 'black')
    left.set_title('Force-distance curve')
    left.set_xlabel('Distance [um]')
    left.set_ylabel('Force [pN]')
    right.plot(F,Lc)
    right.set_title('Lc-force step finding')
    for x, y in zip(x_fit, y_fit):
        right.plot(x, y, c='r')
    right.set_xlabel('Force [pN]')
    right.set_ylabel('Contour length [nm]')

    return fig
        