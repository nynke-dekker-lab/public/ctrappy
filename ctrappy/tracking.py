"""Code for detecting, tracking and colocalizing spots."""
import numpy as np
from scipy.optimize import linear_sum_assignment, curve_fit
import pandas as pd
from skimage.feature import blob_log


def gaussian(x, mu, sigma):
    """Gaussian function."""
    return np.exp(-(x - mu) ** 2 / (2 * sigma ** 2)) / (sigma * np.sqrt(2 * np.pi))


def gaussian2d(x, y, mu_x, mu_y, sigma):
    """2D Gaussian function."""
    return gaussian(x, mu_x, sigma) * gaussian(y, mu_y, sigma)


def fit_gauss2d(imgdata, mu_x, mu_y, sigma, amplitude, bg):
    """Wrapper function for fitting a 2D Gaussian."""
    x, y = imgdata
    return amplitude * gaussian2d(x, y, mu_x, mu_y, sigma) + bg


def fermi_dirac(x, mu, beta):
    """Fermi-Dirac distribution."""
    return 1 / (np.exp(beta * (x - mu)) + 1)


def double_step_func(x, amplitude, x0, x1, x2, x3, background):
    """Fitting function for bead detection; trying to find steps in the projected intensity at the bead edges."""
    # Discrete case does not work for curve_fit.
    # return amplitude * np.array(((x >= x0) & (x < x1)) | ((x >= x2) & (x < x3)), dtype=float)

    # Minimum bead width is 10 pixels.
    x0 = min(x0, x1 - 10)
    x3 = max(x3, x2 + 10)

    result = np.zeros(x.shape)
    cutoff_points = [int((x0 + x1) / 2), int((x1 + x2) / 2), int((x2 + x3) / 2)]
    result[:cutoff_points[0]] = 1 - fermi_dirac(x[:cutoff_points[0]], x0, 2)
    result[cutoff_points[0]:cutoff_points[1]] = fermi_dirac(x[cutoff_points[0]:cutoff_points[1]], x1, 2)
    result[cutoff_points[1]:cutoff_points[2]] = 1 - fermi_dirac(x[cutoff_points[1]:cutoff_points[2]], x2, 2)
    result[cutoff_points[2]:] = fermi_dirac(x[cutoff_points[2]:], x3, 2)
    return amplitude * result + background


class BeadDetector:
    """Class (experimental) for bead detection."""

    def __init__(self, initial_guess):
        self.initial_guess = initial_guess

    def detect(self, intensity_profile):
        """Run bead detection on a projected intensity profile."""
        y = intensity_profile
        x = np.arange(len(y))
        popt, pcov = curve_fit(double_step_func, x, y, p0=self.initial_guess,
                               bounds=[(0, 0, 10, len(y)/2 + 10, len(y)/2 + 20, 0),
                                       (np.inf, len(y)/2 - 20, len(y)/2 - 10, len(y), len(y) + 1, np.inf)])
        return popt, double_step_func(x, *popt)


class Detector:
    """Spot detector using Laplacian of Gaussian.

    Parameters
    ----------
    radius : int or list of 3 ints
        Detection radius (or radii per color).
    thresholds : list of 3 floats
        Detection threshold for each (r,g,b) color. Default: [0.5, 0.5, 0.5].
    overlap : float
        Allowed overlap between detections.
    """

    def __init__(self, radius=4, thresholds=None, overlap=0.5):
        if thresholds is None:
            thresholds = [0.5, 0.5, 0.5]
        if isinstance(radius, list):
            self.radii = radius
        else:
            self.radii = [radius, radius, radius]
        self.thresholds = thresholds
        self.overlap = overlap

    @staticmethod
    def _create_circular_mask(h, w, center, radius):
        """Create a circular mask. The mask has size (w, h) and the circle has a center coordinate and radius."""
        y, x = np.ogrid[:h, :w]
        dist_from_center = np.sqrt((x - center[0])**2 + (y-center[1])**2)
        mask = dist_from_center <= radius
        return mask

    def _detect_log(self, image, color):
        """Detect spots in image using the scipy Laplacian of Gaussian implementation.

        Parameters
        ----------
        image : numpy array of floats
            2D image.
        color : int
            Color index.

        Returns
        -------
        spots : numpy array
            Numpy array listing spots by [x coord, y coord, radius, intensity].
        """

        threshold = self.thresholds[color]
        radius = self.radii[color]

        # Run LoG detection.
        blobs = blob_log(image, min_sigma=radius / np.sqrt(2), max_sigma=radius / np.sqrt(2),
                         num_sigma=1, threshold=threshold, overlap=self.overlap)

        # Get total intensities in the 3rd column & subpixel resolution location in axes 1 and 2.
        spots = np.zeros((blobs.shape[0], 3))
        for i, blob in enumerate(blobs):

            # Make a circular mask to get total intensity.
            mask = self._create_circular_mask(image.shape[0], image.shape[1],
                                              np.array([blob[1], blob[0]]), radius)
            masked_image = image * mask
            intensity = np.sum(masked_image.flatten())
            spots[i, 2] = intensity

            # Reshape data to array with axes (y coordinate, x coordinate, masked image value)
            coords = np.indices(image.shape)
            data = np.zeros((3, image.shape[0], image.shape[1]))
            data[0:2, :, :] = coords
            data[2, :, :] = masked_image
            data = data.reshape(3, image.shape[0] * image.shape[1])

            # Compute best fit of projection to 2D Gaussian on masked image.
            try:
                popt, pcov = curve_fit(fit_gauss2d, data[0:2, :], data[2, :],
                                       p0=[blob[0], blob[1], radius, intensity, 0],
                                       bounds=([blob[0]-1, blob[1]-1, 0, 1, 0],
                                               [blob[0]+1, blob[1]+1, radius*2, intensity*2, intensity]))
                # Get spots coordinates
                spots[i, 0] = popt[0]
                spots[i, 1] = popt[1]
                # Correct background
                bg = popt[-1]
                intensity = np.sum(masked_image.flatten() - bg)

            except RuntimeError:  # curve_fit does not converge
                spots[i, 0] = blob[0]
                spots[i, 1] = blob[1]

            # TODO: we could use popt[2] (fitted sigma) as some kind of selection criterion

        return spots

    def detect(self, images, colors=None):
        """Detect spots for all frames in a 4D numpy array with axes (frames, y, x, colors).

        Parameters
        ----------
        images : numpy array of floats
            4D array containing (frames, y, x, colors).
        colors : list of integers
            Color indices to include (default: [0, 1, 2])

        Returns
        -------
        df_out : pandas DataFrame
            DataFrame containing spots.
        """

        if colors is None:
            colors = [0, 1, 2]
        spots = []
        for frame in range(images.shape[0]):
            # print(frame)
            for color in colors:
                detections = self._detect_log(images[frame, :, :, color], color)
                if len(detections) > 0:
                    x_list = detections[:, 1]
                    y_list = detections[:, 0]
                    intensity_list = detections[:, 2]
                    f_list = np.array([frame for _ in x_list])
                    c_list = np.array([color for _ in x_list])
                    index_list = np.arange(len(x_list))
                    for f, c, x, y, intensity, index in zip(f_list, c_list, x_list, y_list, intensity_list, index_list):
                        spots.append([f, c, x, y, intensity, index])

        if len(spots) > 0:
            df_out = pd.DataFrame(columns=['frame', 'color', 'x', 'y', 'intensity', 'frame_subindex'],
                                  data=np.array(spots))
            df_out['frame'] = df_out['frame'].astype(int)
            df_out['color'] = df_out['color'].astype(int)
            df_out['frame_subindex'] = df_out['frame_subindex'].astype(int)
            return df_out
        else:
            return pd.DataFrame(columns=['frame', 'color', 'x', 'y', 'intensity', 'frame_subindex'])


class Tracker:
    """Class for tracking spots between frames.

    Parameters
    ----------
    skip_frames : int
        Max number of frames to skip for segment linking.
    max_dist : float
        Maximum distance between points for frame-to-frame linking (pixels).
    max_segment_dist : float
        Maximum distance between end and start points for segment linking (pixels).
    """

    def __init__(self, skip_frames=1, max_dist=10, max_segment_dist=10):
        self.skip_frames = skip_frames
        self.max_dist = max_dist
        self.max_segment_dist = max_segment_dist

    def lap_matrix(self, blobs0, blobs1):
        """Populate LAP matrix for frame-to-frame linking - see https://www.nature.com/articles/nmeth.1237.pdf"""

        # Linking costs.
        lap = np.full((len(blobs0) + len(blobs1), len(blobs0) + len(blobs1)), np.infty)
        x0 = np.array(blobs0['x'])
        x1 = np.array(blobs1['x'])
        y0 = np.array(blobs0['y'])
        y1 = np.array(blobs1['y'])
        dx = np.power(x0[:, np.newaxis] - x1, 2)
        dy = np.power(y0[:, np.newaxis] - y1, 2)
        distsq = dx + dy
        indices = np.where(distsq <= self.max_dist**2)
        for loc0, loc1 in zip(indices[0], indices[1]):
            i0 = int(blobs0['frame_subindex'].iloc[loc0])
            i1 = int(blobs1['frame_subindex'].iloc[loc1])
            lap[i0, i1] = distsq[loc0, loc1]

        # Non-linking costs.
        no_link_blob0 = np.full((len(blobs0), len(blobs0)), np.infty)
        np.fill_diagonal(no_link_blob0, self.max_dist ** 2)
        lap[:len(blobs0), len(blobs1):] = no_link_blob0
        no_link_blob1 = np.full((len(blobs1), len(blobs1)), np.infty)
        np.fill_diagonal(no_link_blob1, self.max_dist ** 2)
        lap[len(blobs0):, :len(blobs1)] = no_link_blob1
        lap[len(blobs0):, len(blobs1):] = lap[:len(blobs0), :len(blobs1)].T

        return lap

    def lap_matrix_segments(self, df_tracks):
        """Populate LAP matrix for segment linking - see https://www.nature.com/articles/nmeth.1237.pdf"""

        n_segments = df_tracks['track_id'].max() + 1

        df_segments = pd.DataFrame(columns=['segment_id', 'frame_start', 'frame_end',
                                            'x_start', 'x_end', 'y_start', 'y_end'])
        for i in range(n_segments):
            seg_i = df_tracks[df_tracks['track_id'] == i]
            seg_i_start = seg_i.iloc[0]
            seg_i_end = seg_i.iloc[-1]
            seg = {'frame_start': [seg_i_start['frame']], 'frame_end': [seg_i_end['frame']],
                   'x_start': [seg_i_start['x']], 'x_end': [seg_i_end['x']],
                   'y_start': [seg_i_start['y']], 'y_end': [seg_i_end['y']]}
            df_segments = pd.concat((df_segments, pd.DataFrame(seg)), ignore_index=True)

        segment_combos = []
        for i in range(n_segments):
            i_end = df_segments['frame_end'].loc[i]
            d_frame = df_segments['frame_start'] - i_end
            d_frame = (0 < d_frame) & (d_frame <= self.skip_frames + 1)
            j_vals = np.where(d_frame)[0]
            for j in j_vals:
                segment_combos.append((i, j))

        lap = np.full((2 * n_segments, 2 * n_segments), np.infty)
        for i, j in segment_combos:
            seg_i = df_segments.iloc[i]
            seg_j = df_segments.iloc[j]
            d_frame = seg_j['frame_start'] - seg_i['frame_end']
            if 0 < d_frame <= self.skip_frames + 1:
                distance = np.sqrt((seg_j['x_start'] - seg_i['x_end'])**2 + (seg_j['y_start'] - seg_i['y_end'])**2)
                if distance <= self.max_dist:
                    lap[i, j] = distance ** 2  # From end of i to start of j

        no_link = np.full((n_segments, n_segments), np.infty)
        np.fill_diagonal(no_link, self.max_dist ** 2)
        lap[:n_segments, n_segments:] = no_link
        lap[n_segments:, :n_segments] = no_link
        lap[n_segments:, n_segments:] = lap[:n_segments, :n_segments].T

        return lap

    def track(self, df_tracks_input):
        """Do LAP tracking. df_tracks_input needs to be a dataframe with columns ['frame', 'x', 'y'];
        output will contain a new column 'track_id'.

        Parameters
        ----------
        df_tracks_input : pandas DataFrame
            Input dataframe with columns ['frame', 'frame_subindex', 'x', 'y'].

        Returns
        -------
        df_tracks : pandas DataFrame
            Output dataframe - same as input, but with added column 'track_id'.
        """

        # Prepare.
        df_tracks = df_tracks_input.copy()
        df_tracks = df_tracks.sort_values('frame')
        df_tracks['track_id'] = -1

        # Step 1: frame-to-frame linking.
        for frame1 in range(df_tracks['frame'].max() + 1):
            frame0 = frame1 - 1

            if len(df_tracks) > 0:
                blobs0 = df_tracks[df_tracks['frame'] == frame0]
                blobs1 = df_tracks[df_tracks['frame'] == frame1]

                if len(blobs0) == 0 and len(blobs1) > 0:
                    # Create new blobs1 segments.
                    for ind, blob in blobs1.iterrows():
                        df_tracks.at[ind, 'track_id'] = df_tracks['track_id'].max() + 1

                # Compute and solve LAP matrix; connect blobs.
                if len(blobs0) > 0 and len(blobs1) > 0:
                    costs = self.lap_matrix(blobs0, blobs1).T
                    b0_indices = blobs0.sort_values('frame_subindex').index.values
                    b1_indices = blobs1.sort_values('frame_subindex').index.values
                    row_ind, col_ind = linear_sum_assignment(costs)
                    for i, j in enumerate(col_ind[:len(blobs1)]):
                        if j < len(blobs0):
                            # connect
                            track_id = df_tracks['track_id'].loc[b0_indices[j]]
                        else:
                            # create
                            track_id = df_tracks['track_id'].max() + 1
                        df_tracks.at[b1_indices[i], 'track_id'] = track_id

        # Step 2: link segments.
        costs = self.lap_matrix_segments(df_tracks)
        row_ind, col_ind = linear_sum_assignment(costs)
        n_segments = df_tracks['track_id'].max() + 1
        df_tracks['track_connected_id'] = df_tracks['track_id']
        for i, j in zip(row_ind, col_ind):
            if i < n_segments and j < n_segments:
                df_track = df_tracks[df_tracks['track_id'] == j]
                new_index = (df_tracks[df_tracks['track_id'] == i])['track_connected_id'].iloc[0]
                df_tracks.loc[df_track.index.values, 'track_connected_id'] = new_index
        df_tracks['track_id'] = df_tracks['track_connected_id']
        df_tracks = df_tracks.drop('track_connected_id', axis=1)
        return df_tracks


class Colocalizer:
    """Class for determining colocalizations.

    Parameters
    ----------
    colocalization_distance : float
        Threshold distance under which two spots can be considered colocalized.
    crosstalk_matrices : numpy array of floats with shape (3, 3, 3)
        Matrices for crosstalk correction; see explanation in config.yaml under "CROSSTALK CORRECTION".
    colocalization_frames : int
        Number of frames for colocalization calculation. Set to None for all frames.
    cf_res : float
        Microns per pixel in the confocal image. Default: 0.05.
    """

    def __init__(self, colocalization_distance, crosstalk_matrices, colocalization_frames=5, cf_res=0.05):
        self.col_dist = colocalization_distance
        self.cross_mat = crosstalk_matrices
        self.col_frames = colocalization_frames
        self.cf_res = cf_res

    def _get_coloc_matrix(self, df0, df1):
        """Colocalization distance matrix between tracks in df0 and tracks in df1."""

        n0 = len(df0['trace_id'].unique())
        n1 = len(df1['trace_id'].unique())
        coloc_matrix = np.zeros((n0, n1))

        id0_to_i_track = [i_track for i_track in df0['trace_id'].unique()]
        i_track_to_id0 = dict()
        for i in range(len(id0_to_i_track)):
            i_track_to_id0[id0_to_i_track[i]] = i

        id1_to_j_track = [j_track for j_track in df1['trace_id'].unique()]
        j_track_to_id1 = dict()
        for i in range(len(id1_to_j_track)):
            j_track_to_id1[id1_to_j_track[i]] = i

        for i_track, df_i_track in df0.groupby('trace_id'):
            for j_track, df_j_track in df1.groupby('trace_id'):

                d = np.nan
                frames = np.intersect1d(df_i_track['frame'], df_j_track['frame'])  # intersecting frames
                if len(frames) > 1:  # if there is more than 1 intersecting frame, calculate mean distance.
                    dist = np.zeros(len(frames))
                    for i, frame in enumerate(frames):
                        x_dist = float(((df_i_track[df_i_track['frame'] == frame])['x_pixel']).mean()) - \
                            float((df_j_track[df_j_track['frame'] == frame])['x_pixel'])
                        y_dist = float(((df_i_track[df_i_track['frame'] == frame])['y_pixel']).mean()) - \
                            float((df_j_track[df_j_track['frame'] == frame])['y_pixel'])
                        dist[i] = np.sqrt((x_dist**2 + y_dist**2))

                    # Calculate mean position up to self.col_frames.
                    if self.col_frames is not None:
                        dist_len = int(min(len(dist), self.col_frames))
                        d = np.mean(dist[:dist_len]) * self.cf_res
                    else:
                        d = np.mean(dist) * self.cf_res

                i = i_track_to_id0[i_track]
                j = j_track_to_id1[j_track]
                coloc_matrix[i, j] = d

        return coloc_matrix, id0_to_i_track, id1_to_j_track

    def _get_coloc_trace_id(self, df0, df1):
        """Return a column with trace_id values - this column consists of track_ids, where any df1 track_ids are
        set to a df0 track_id if the corresponding tracks colocalize."""

        coloc_matrix, ind_to_trace0, ind_to_trace1 = self._get_coloc_matrix(df0, df1)
        coloc_matrix = np.where(coloc_matrix < self.col_dist, coloc_matrix, 100*self.col_dist)

        row_ind, col_ind = linear_sum_assignment(coloc_matrix)
        for i, j in zip(row_ind, col_ind):
            trace1 = df1[df1['trace_id'] == ind_to_trace1[j]]
            d = coloc_matrix[i, j]
            if d < self.col_dist:
                df1.loc[trace1.index.values, 'trace_id'] = ind_to_trace0[i]
                df1.loc[trace1.index.values, 'col_dist'] = d
        return df1.index.values, df1['trace_id'], df1['col_dist']

    def _correct_crosstalk(self, df_trace_input, laser_colors):
        """For step counting, we need frame-by-frame corrected intensities."""
        laser_to_ids = {'r': [0], 'g': [1], 'b': [2], 'rg': [0, 1],
                        'rb': [0, 2], 'gb': [1, 2], 'rgb': [0, 1, 2]}
        laser_to_str = {'r': ['r'], 'g': ['g'], 'b': ['b'], 'rg': ['r', 'g'],
                        'rb': ['r', 'b'], 'gb': ['g', 'b'], 'rgb': ['r', 'g', 'b']}
        laser_color_ids = laser_to_ids[laser_colors]
        laser_color_strs = laser_to_str[laser_colors]

        df_trace = df_trace_input.copy()
        df_trace['corrected_intensity'] = df_trace['intensity']

        # Put corrected intensity in corrected_intensity column.
        for i_c, ci in zip(laser_color_ids, laser_color_strs):
            for j_c, cj in zip(laser_color_ids, laser_color_strs):
                if i_c != j_c:
                    intensity_i = df_trace[df_trace['color_id'] == i_c][['corrected_intensity', 'frame']]\
                        .sort_values(['frame', 'corrected_intensity'])
                    intensity_j = df_trace[df_trace['color_id'] == j_c][['corrected_intensity', 'frame']]\
                        .sort_values(['frame', 'corrected_intensity'])
                    intensity_i.drop_duplicates('frame', keep='last', inplace=True)
                    intensity_j.drop_duplicates('frame', keep='last', inplace=True)
                    frames_overlap = np.intersect1d(intensity_i['frame'], intensity_j['frame'])
                    if len(frames_overlap) > 0:
                        intensity_i = intensity_i[intensity_i['frame'].isin(frames_overlap)]['corrected_intensity']
                        intensity_j = intensity_j[intensity_j['frame'].isin(frames_overlap)]['corrected_intensity']
                        crosstalk = (np.array(self.cross_mat[i_c])[laser_color_ids, j_c]).sum(axis=0)
                        correction = 0.01 * crosstalk * np.array(intensity_j)
                        df_trace.loc[intensity_i.index.values, 'corrected_intensity'] = \
                            (np.array(intensity_i) - correction)

        return df_trace.index.values, df_trace['corrected_intensity'].clip(0., None)

    def colocalize(self, df_tracks_input, laser_colors):
        """Find colocalized tracks; give colocalized tracks the same 'trace_id'.

        Parameters
        ----------
        df_tracks_input : pandas DataFrame
            Input dataframe with columns ['frame', 'color', 'intensity', 'x_pixel', 'y_pixel', 'track_id'].
        laser_colors : str
            'r', 'g', 'b', 'rg', 'rb', 'gb' or 'rgb'

        Returns
        -------
        df_tracks : pandas DataFrame
            Output dataframe - same as input, but with added columns ['colocalized', 'col_dist',
            'trace_id', 'corrected_intensity'] (bool, float, int, float).
        """

        # Prepare.
        if df_tracks_input.empty:
            return df_tracks_input
        df_tracks = df_tracks_input.copy()
        df_tracks['colocalized'] = False
        df_tracks['col_dist'] = np.nan
        df_tracks['trace_id'] = df_tracks['track_id']
        df_tracks['corrected_intensity'] = df_tracks['intensity']
        colors = df_tracks['color_id'].unique()
        # print(df_tracks)

        # If only one color, no coloc.
        if len(colors) == 1:
            return df_tracks

        # If two colors, run coloc between colors.
        elif len(colors) == 2:
            df_c0 = df_tracks[df_tracks['color_id'] == colors[0]].copy()
            df_c1 = df_tracks[df_tracks['color_id'] == colors[1]].copy()
            inds, trace_ids, col_dists = self._get_coloc_trace_id(df_c0, df_c1)
            df_tracks.loc[inds, 'trace_id'] = trace_ids
            df_tracks.loc[inds, 'col_dist'] = col_dists

        # If all three colors, run (0/1) coloc; put everything in traces, then (trace/2) coloc, add single 2 traces.
        else:
            df_c0 = df_tracks[df_tracks['color_id'] == colors[0]].copy()
            df_c1 = df_tracks[df_tracks['color_id'] == colors[1]].copy()
            inds, trace_ids, col_dists = self._get_coloc_trace_id(df_c0, df_c1)
            df_tracks.loc[inds, 'trace_id'] = trace_ids
            df_tracks.loc[inds, 'col_dist'] = col_dists

            df_c01 = df_tracks[df_tracks['color_id'] != colors[2]].copy()
            df_c2 = df_tracks[df_tracks['color_id'] == colors[2]].copy()
            inds, trace_ids, col_dists = self._get_coloc_trace_id(df_c01, df_c2)
            df_tracks.loc[inds, 'trace_id'] = trace_ids
            df_tracks.loc[inds, 'col_dist'] = col_dists

        df_tracks['colocalized'] = False
        for trace_id, df_trace in df_tracks.groupby('trace_id'):
            # Re-calculate df_trace['colocalized'] and correct crosstalk.
            if len(df_trace['track_id'].unique()) > 1:
                df_tracks.loc[df_trace.index.values, 'colocalized'] = True

                # Run crosstalk correction.
                trace_inds, corr_int = self._correct_crosstalk(df_trace, laser_colors)
                df_tracks.loc[trace_inds, 'corrected_intensity'] = corr_int

        # Done.
        return df_tracks
