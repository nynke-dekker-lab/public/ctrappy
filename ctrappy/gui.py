import streamlit as st
import os
import ctrappy.file
import ctrappy.scan
import ctrappy.tracking
import ctrappy.gui_figures
import ctrappy.gui_tabs.h5file
import ctrappy.gui_tabs.h5dir
import ctrappy.gui_tabs.tifffile
import ctrappy.gui_tabs.tiffdir
import ctrappy.gui_tabs.explore
import ctrappy.gui_tabs.analysis
import ctrappy.gui_tabs.calibration
import ctrappy.gui_tabs.analysisLc
import ctrappy.gui_tabs.mechanics
import ctrappy.gui_tabs.tools.file_caching as cache
from pathlib import Path


def _max_width_():
    """Make images wider."""
    max_width_str = f"max-width: 1000px;"
    st.markdown(
        f"""
    <style>
    .reportview-container .main .block-container{{
        {max_width_str}
    }}
    </style>    
    """,
        unsafe_allow_html=True,
    )


def main():
    """Show gui."""

    # Choose action.
    tab = st.sidebar.selectbox(label='Navigation',
                               options=['readme', 'open h5 file',
                                        'export h5 directory to tiffs',
                                        'process tiff file',
                                        'process tiff directory',
                                        'explore data table',
                                        'analyze data table',
                                        'calibration',
                                        'DNA mechanics analysis',
                                        "Contour length increment analysis"])

    cache.clear_cache_on_tab_change(tab)

    if tab == 'readme':
        gui_readme_path = os.path.join(os.path.dirname(__file__), "gui_tabs/docs/README_GUI_TABS.md")
        st.markdown(Path(gui_readme_path).read_text())

    if tab == 'open h5 file':
        ctrappy.gui_tabs.h5file.run()

    if tab == 'export h5 directory to tiffs':
        ctrappy.gui_tabs.h5dir.run()

    if tab == 'process tiff file':
        ctrappy.gui_tabs.tifffile.run()

    if tab == 'process tiff directory':
        ctrappy.gui_tabs.tiffdir.run()

    if tab == 'explore data table':
        ctrappy.gui_tabs.explore.run()

    if tab == 'analyze data table':
        ctrappy.gui_tabs.analysis.run()

    if tab == 'calibration':
        ctrappy.gui_tabs.calibration.run()

    if tab == 'DNA mechanics analysis':
        ctrappy.gui_tabs.mechanics.run()
    
    if tab == "Contour length increment analysis":
        ctrappy.gui_tabs.analysisLc.run()

if __name__ == "__main__":
    _max_width_()
    main()
