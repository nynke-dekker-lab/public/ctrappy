"""
Functions for calculating and plotting useful quantities from Experiments.
"""
import lmfit
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from collections import Counter
import ctrappy.diffusion
import ctrappy.diffusion_fit
from scipy.stats import norm
from scipy.optimize import curve_fit
import matplotlib
import types
import statsmodels.stats.proportion
font = {'size': 16}
matplotlib.rc('font', **font)


def plot_scan_spots(image, frame, display_max, offset_values, spots, radius):
    """Plot spots in an Image object.

    Parameters
    ----------
    image : ctrappy.image.Image
        Image object.
    frame : int
        Frame to plot.
    display_max : int
        Maximum pixel value.
    offset_values : 2-tuple of floats
        Offset values for marking the start and end of the DNA
    spots : pandas DataFrame
        Dataframe containing spots.
    radius : int or list of 3 ints
        Detection radius (or radii per color).

    Returns
    -------
    fig : matplotlib.figure.Figure
    """

    if isinstance(radius, list):
        radii = radius
    else:
        radii = [radius, radius, radius]

    fig = plt.figure(figsize=(10, 4))
    plt.imshow(image.data_raw[frame, :, :, :]/display_max, vmin=0, vmax=1)
    ax = plt.gca()
    c_dict = {0: 'r', 1: 'g', 2: 'b'}
    if offset_values is not None:
        plt.axvline(offset_values[0], c='w')
        plt.axvline(offset_values[1], c='w')
    for color in range(3):
        df_color = spots[spots['color'] == color]
        if not df_color.empty:
            for _, spot in df_color[df_color['frame'] == frame].iterrows():
                circle = plt.Circle((spot['x'], spot['y']), radii[color], facecolor=(0, 0, 0, 0), edgecolor=c_dict[color])
                ax.add_artist(circle)
                # plt.scatter([spot['x']], [spot['y']], marker='o',
                #             facecolors=(0, 0, 0, 0), edgecolors=c_dict[color], s=1000)
    plt.xlabel('x (pixels)')
    plt.ylabel('y (pixels)')
    plt.tight_layout()
    plt.close()
    return fig


def plot_bead_detection(intensity, bead_fit, offset_values, image):
    """Plot detected beads on an intensity profile.

    Parameters
    ----------
    intensity : numpy array of floats
        Projected intensity profile.
    bead_fit : numpy array of floats
        Fitted bead profile.
    offset_values : 2-tuple of floats
        Offset values for marking the start and end of the DNA.
    image : ctrappy.image.Image object
        Image object.

    Returns
    -------
    fig : matplotlib.figure.Figure
    """
    fig = plt.figure(figsize=(10, 3))
    plt.plot(intensity, c='k', label='signal')
    plt.plot(bead_fit, c='r', label='fit')
    plt.axvline(offset_values[0], c='b', label='bead edge')
    plt.axvline(offset_values[1], c='b')
    plt.legend(prop={'size': 10})
    plt.xlim((0, image.data.shape[2]))
    plt.xlabel('x (pixels)')
    plt.ylabel('intensity  (ADU)')
    plt.tight_layout()
    plt.close()
    return fig


def plot_force_over_time(h5file):
    """Plot force data in an h5file; returns a figure and a DataFrame."""

    x = h5file['Force LF']['Force 1x'].timestamps
    y1 = h5file['Force LF']['Force 1x'].data
    y2 = h5file['Force LF']['Force 2x'].data
    y3 = h5file['Force LF']['Force 1y'].data
    y4 = h5file['Force LF']['Force 2y'].data
    flags = {}

    fig = plt.figure(figsize=(10, 5))
    plt.title('Force over time; FD curve indices in blue, scan indices in red')
    for i, fdcurve in h5file.fdcurves.items():
        loc0 = (fdcurve.start - np.amin(x)) / 1e9
        loc1 = (fdcurve.stop - np.amin(x)) / 1e9
        if fdcurve.start in flags.keys():
            flags[fdcurve.start] = flags[fdcurve.start] + '; fdcurve start ' + str(i)
        else:
            flags[fdcurve.start] = 'fdcurve start ' + str(i)
        if loc1 in flags.keys():
            flags[fdcurve.stop] = flags[fdcurve.stop] + '; fdcurve end ' + str(i)
        else:
            flags[fdcurve.stop] = 'fdcurve end ' + str(i)
        plt.fill_between([loc0, loc1], [-100, -100], [100, 100], color=(0, 0, 1, 0.3))
        plt.text((loc0 + loc1) / 2, 80, i, horizontalalignment='center', color='b')
        # st.write(fdcurve.name, fdcurve.start, fdcurve.stop)
    for i, scan in h5file.scans.items():
        t_scan = scan.timestamps
        t_scan = t_scan[t_scan > 1e18]
        loc0 = (np.amin(t_scan) - np.amin(x)) / 1e9
        loc1 = (np.amax(t_scan) - np.amin(x)) / 1e9
        if np.amin(t_scan) in flags.keys():
            flags[np.amin(t_scan)] = flags[np.amin(t_scan)] + '; scan start ' + str(i)
        else:
            flags[np.amin(t_scan)] = 'scan start ' + str(i)
        if np.amax(t_scan) in flags.keys():
            flags[np.amax(t_scan)] = flags[np.amax(t_scan)] + '; scan end ' + str(i)
        else:
            flags[np.amax(t_scan)] = 'scan end ' + str(i)
        plt.fill_between([loc0, loc1], [-100, -100], [100, 100], color=(1, 0, 0, 0.3))
        plt.text((loc0 + loc1) / 2, -90, i, horizontalalignment='center', color='r')
    t = (x - np.amin(x)) / 1e9
    plt.plot(t, y1, label='channel 1x')
    plt.plot(t, y2, label='channel 2x')
    plt.xlabel('t (s)')
    plt.ylabel('F (pN)')
    plt.ylim((-100, 100))
    plt.legend()
    plt.tight_layout()
    plt.close()

    df_out = pd.DataFrame(columns=['timestamp', 't (s)', 'force channel 1x (pN)', 'force channel 2x (pN)',
                                   'force channel 1y (pN)', 'force channel 2y (pN)', 'flag'])
    df_out['t (s)'] = t
    df_out['force channel 1x (pN)'] = y1
    df_out['force channel 2x (pN)'] = y2
    df_out['force channel 1y (pN)'] = y3
    df_out['force channel 2y (pN)'] = y4
    df_out['flag'] = ''
    df_out['timestamp'] = x
    df_out.index = x
    for time, flag in flags.items():
        df_out.loc[time, 'flag'] = flag
        df_out.loc[time, 'timestamp'] = time
    df_out = df_out.sort_index()
    return fig, df_out


def plot_fdcurves(h5file, marker=True):
    """Plot all fd-curves in an h5file, within fd-curve markers.
    If marker=False, simply plot all fd-data in the h5file.
    Returns a figure and a DataFrame."""

    df_out = pd.DataFrame()
    if marker:
        n_curves = len(h5file.fdcurves)
        if n_curves == 0:
            return "No FD curves found.", df_out
        fdcurves = h5file.fdcurves
    else:
        n_curves = 1
        fdcurves = {0: types.SimpleNamespace()}
        t = np.array(h5file['Force LF']['Force 1x'].timestamps)
        fdcurves[0].name = "0"
        fdcurves[0].start = np.amin(t)
        fdcurves[0].stop = np.amax(t)
        fdcurves[0].distance1 = types.SimpleNamespace()
        fdcurves[0].distance1.data = np.zeros(len(t))
        fdcurves[0].f = types.SimpleNamespace()
        fdcurves[0].f.data = np.zeros(len(t))

    nrows = int(np.ceil(n_curves / 2))
    fig, axes = plt.subplots(ncols=2, nrows=nrows, figsize=(10, 3 * nrows))

    i = 0
    j = 0
    t = np.array(h5file['Force LF']['Force 1x'].timestamps)
    x1 = np.array(h5file['Force LF']['Force 1x'].data)
    x2 = np.array(h5file['Force LF']['Force 2x'].data)
    y1 = np.array(h5file['Force LF']['Force 1y'].data)
    y2 = np.array(h5file['Force LF']['Force 2y'].data)
    dx1 = np.array(h5file['Bead position']['Bead 1 X'].data)
    dy1 = np.array(h5file['Bead position']['Bead 1 Y'].data)
    dx2 = np.array(h5file['Bead position']['Bead 2 X'].data)
    dy2 = np.array(h5file['Bead position']['Bead 2 Y'].data)
    diam = np.mean(h5file['Bead diameter']['Template 1'].data)
    n_datapoints = np.amin([len(t), len(x1), len(x2), len(y1), len(y2),
                            len(dx1), len(dx2), len(dy1), len(dy2)])

    for _, fdcurve in fdcurves.items():

        # loc0 = np.argmin(np.absolute(fdcurve.start - t))
        if marker:
            loc1 = np.argmin(np.absolute(fdcurve.stop - t))
            n = len(fdcurve.f.data)
            loc0 = loc1-n
        else:
            n = n_datapoints
            loc0 = 0
            loc1 = n

        if nrows > 1:
            ax = axes[i][j]
            if j == 1:
                i = i + 1
                j = 0
            else:
                j = 1
        else:
            ax = axes[j]
            if j == 0:
                j = 1
            else:
                j = 0
        ax.plot(fdcurve.distance1.data, fdcurve.f.data)
        add_dict = {'distance ' + fdcurve.name + ' (um)': fdcurve.distance1.data[:n],
                    'force ' + fdcurve.name + ' (pN)': fdcurve.f.data[:n],
                    'distance_x ' + fdcurve.name + ' (um)': np.absolute(dx2[loc0:loc1] - dx1[loc0:loc1]) - diam,
                    'force_1x ' + fdcurve.name + ' (pN)': x1[loc0:loc1],
                    'force_2x ' + fdcurve.name + ' (pN)': x2[loc0:loc1],
                    'force_1y ' + fdcurve.name + ' (pN)': y1[loc0:loc1],
                    'force_2y ' + fdcurve.name + ' (pN)': y2[loc0:loc1],
                    'bead_1x ' + fdcurve.name + ' (pN)': dx1[loc0:loc1],
                    'bead_2x ' + fdcurve.name + ' (pN)': dx2[loc0:loc1],
                    'bead_1y ' + fdcurve.name + ' (pN)': dy1[loc0:loc1],
                    'bead_2y ' + fdcurve.name + ' (pN)': dy2[loc0:loc1]}
        df_out = pd.concat([df_out, pd.DataFrame.from_dict(add_dict)], axis=1)
        ax.set_title('FD curve ' + str(fdcurve.name))
        ax.set_xlabel('DNA extension (micron)')
        ax.set_ylabel('F (pN)')
    if nrows > 1 and j == 1:
        axes[i][j].axis('off')
    if nrows == 1 and j == 1:
        axes[j].axis('off')
    plt.tight_layout()
    plt.close()
    return fig, df_out

###########################
# PLOTS FOR ANALYSIS PAGE #
###########################


def get_bleaching_traces(experiment, time_s=False):
    """Get all bleaching traces within the Experiment. By default, use frames as time unit - set time_s=True to use
    seconds as time unit. Result is returned in a dictionary."""

    max_intensity = float(experiment.data_full['corrected_intensity'].max())
    data = dict()
    data['step_fitting_data_available'] = experiment.redo_step_fitting
    data['max_intensity'] = max_intensity
    if time_s:
        data['t_unit'] = 's'
    else:
        data['t_unit'] = 'frame'
    data['traces'] = dict()
    data['splits'] = dict()
    data['nrows'] = 0

    i = 0
    j = 0
    n = 0
    n_max = 100
    for scan in experiment.scans:
        for trace in scan.traces:
            for track in trace.tracks:
                if n >= n_max:
                    nrows = i - (1 - j) + 1
                    data['nrows'] = nrows
                    return data
                if track.step_count > 0 and n < n_max:

                    index = [i, j]
                    if j == 1:
                        i = i + 1
                        j = 0
                    else:
                        j = 1

                    max_frame = np.amax(track.frame)
                    frames = np.concatenate((track.frame, [max_frame + 1 + i for i in range(10)]))
                    intensities = np.concatenate((track.intensity, [0 for _ in range(10)]))

                    data['traces'][n] = dict()
                    data['traces'][n]['index'] = index
                    data['traces'][n]['intensity'] = intensities.tolist()
                    data['traces'][n]['color'] = str(track.color_str)
                    data['traces'][n]['fit'] = track.step_fit.tolist()
                    data['traces'][n]['scan_id'] = int(scan.scan_id)
                    data['traces'][n]['trace_id'] = int(trace.trace_id)
                    data['traces'][n]['track_id'] = int(track.track_id)
                    data['traces'][n]['step_count'] = int(track.step_count)

                    if not time_s:
                        data['traces'][n]['t'] = frames.tolist()
                        data['traces'][n]['steps'] = (np.array(track.step_frames) - 0.5).tolist()
                        data['traces'][n]['t_max'] = int(scan.n_frames)
                    else:
                        s_per_frame = experiment.frame_time_s
                        data['traces'][n]['t'] = (frames * s_per_frame).tolist()
                        data['traces'][n]['steps'] = ((np.array(track.step_frames) - 0.5) * s_per_frame).tolist()
                        data['traces'][n]['t_max'] = float(scan.n_frames * s_per_frame)

                    n = n + 1

    nrows = i - (1 - j) + 1
    data['nrows'] = nrows

    return data


def plot_bleaching_traces(data):
    """Plot bleaching traces from data dictionary, generated with get_bleaching_traces()."""

    fig, axes = plt.subplots(ncols=2, nrows=data['nrows'], figsize=(10, 3 * data['nrows']))
    _ = [axi.set_axis_off() for axi in axes.ravel()]

    for i, trace in data['traces'].items():
        if data['nrows'] > 1:
            ax = axes[trace['index'][0]][trace['index'][1]]
        else:
            ax = axes[trace['index'][1]]
        ax.set_axis_on()
        ax.scatter(trace['t'], trace['intensity'], color=trace['color'], s=4)
        ax.plot(trace['t'], trace['intensity'], color=trace['color'])
        ax.plot(trace['t'], trace['fit'], color=trace['color'], linestyle='dashed')
        for vline in trace['steps']:
            ax.axvline(vline, color='k', linestyle='dashed')
        ax.set_xlim((0, trace['t_max']))
        if data['t_unit'] == 'frame':
            ax.set_xlabel('t (frame)')
        elif data['t_unit'] == 's':
            ax.set_xlabel('t (s)')
        ax.set_ylim((0, data['max_intensity'] * 1.05))
        ax.set_ylabel('intensity (ADU)')
        ax.set_title('Bleaching trace, id ' + str((trace['scan_id'], trace['trace_id'], trace['track_id'])))

        ax.text(0.97, 0.9, 'Steps: %i' % trace['step_count'],
                horizontalalignment='right', transform=ax.transAxes, size=10)
        if not data['step_fitting_data_available']:
            ax.text(0.97, 0.8, 'Redo step fitting to see step locations',
                    horizontalalignment='right', transform=ax.transAxes, color='r', size=10)
    fig.tight_layout()
    plt.close()

    return fig


def get_location_traces(experiment, time_s=False, loc_kbp=False, show_single_spots=False):
    """Get all location traces in the Experiment. By default, time units are frames (switch to seconds with time_s=True)
    and location units are microns (switch to kbp with loc_kbp=True); disconnected spots are not shown (show them with
    show_single_spots=True). Data is returned in a dictionary."""

    data = dict()
    data['x_max'] = 0
    data['t_max'] = 0
    if time_s:
        data['t_unit'] = 's'
    else:
        data['t_unit'] = 'frame'
    if loc_kbp:
        data['x_unit'] = 'kbp'
    else:
        data['x_unit'] = 'micron'
    data['traces'] = dict()
    n = 0
    s_per_frame = experiment.frame_time_s
    for scan in experiment.scans:
        if scan.dna_length_micron > data['x_max']:
            data['x_max'] = float(scan.dna_length_micron)
        if scan.n_frames > data['t_max']:
            data['t_max'] = int(scan.n_frames)
        for trace in scan.traces:
            for track in trace.tracks:
                if show_single_spots or track.step_count > 0:

                    data['traces'][n] = dict()
                    if not time_s:
                        data['traces'][n]['t'] = track.frame.tolist()
                    else:
                        data['traces'][n]['t'] = track.time_s.tolist()
                    if not loc_kbp:
                        data['traces'][n]['x'] = track.x_micron.tolist()
                    else:
                        data['traces'][n]['x'] = track.x_kbp.tolist()
                    data['traces'][n]['color'] = str(track.color_str)
                    data['traces'][n]['scan_id'] = int(scan.scan_id)
                    data['traces'][n]['trace_id'] = int(trace.trace_id)
                    data['traces'][n]['track_id'] = int(track.track_id)
                    data['traces'][n]['count'] = int(track.step_count)

                    data['traces'][n]['split'] = False
                    data['traces'][n]['split_t'] = []
                    data['traces'][n]['split_x'] = []
                    if trace.split_parent[track.color_id] is not None:
                        for trace2 in scan.traces:
                            if trace2.trace_id == trace.split_parent[track.color_id]:
                                for track2 in trace2.tracks:
                                    if track2.color_id == track.color_id:
                                        frame_ind = trace.split_parent_frame_ind
                                        frame2 = track2.frame[frame_ind]
                                        if not time_s:
                                            data['traces'][n]['split_t'] = [frame2, track.frame[0]]
                                        else:
                                            data['traces'][n]['split_t'] = [s_per_frame * frame2,
                                                                            s_per_frame * track.frame[0]]
                                        if not loc_kbp:
                                            x1 = track.x_micron[0]
                                            x0 = float(track2.x_micron[frame_ind])
                                        else:
                                            x1 = track.x_kbp[0]
                                            x0 = float(track2.x_kbp[frame_ind])
                                        data['traces'][n]['split_x'] = [x0, x1]
                                        data['traces'][n]['split'] = True
                n = n + 1

    if time_s:
        data['t_max'] = float(data['t_max'] * s_per_frame)
    if loc_kbp:
        data['x_max'] = float(experiment.dna_length_kbp)
    return data


def plot_location_traces(data, plot_id=False, lines=None):
    """Plot all traces from data dictionary, generated with get_location_traces(). You can show the track ids on the
    resulting figure with plot_id=True; you can add horizontal lines by setting lines to a list of floats."""

    if lines is None:
        lines = []
    fig, ax = plt.subplots(figsize=(10, 6))
    for i, trace in data['traces'].items():

        plt.scatter(trace['t'], trace['x'], color=trace['color'], s=4)
        plt.plot(trace['t'], trace['x'], color=trace['color'])
        if trace['split']:
            # print(trace['split_t'], trace['split_x'])
            plt.plot(trace['split_t'], trace['split_x'], color='purple')
        if plot_id and trace['count'] > 0:
            plt.text((np.amax(trace['t']) + 1), np.amax(trace['x']),
                     str((trace['scan_id'], trace['trace_id'], trace['track_id'])), color=trace['color'])

    plt.title('Location traces')
    ax.set_xlim((0, data['t_max']))
    ax.set_ylim((0, data['x_max']))
    if data['t_unit'] == 'frame':
        ax.set_xlabel('t (frame)')
    elif data['t_unit'] == 's':
        ax.set_xlabel('t (s)')
    if data['x_unit'] == 'micron':
        ax.set_ylabel('x (micron)')
    elif data['x_unit'] == 'kbp':
        ax.set_ylabel('x (kbp)')
    for line in lines:
        plt.plot([0, data['t_max']], [line, line], color='k', linestyle='dashed')
    plt.tight_layout()
    plt.close()

    return fig


def get_stoichiometry(experiment, color_names=None, norm_per_scan=False):
    """Get stoichiometries of all Traces in the Experiment. You can set the names of the tagged proteins for each color
    by setting color_names to a list with three strings for (r, g, b), respectively. You can normalize the results
    by scan with norm_per_scan=True; by default results are normalized by the total number of traces."""

    if color_names is None:
        color_names = ['R', 'G', 'B']

    # Convert count to string.
    def convert_count_to_str(count_list, rname, gname, bname):
        if sum(count_list) >= 5:
            return "≥5 proteins"
        result = ""
        if count_list[0] > 0 and rname != "":
            result = result + str(count_list[0]) + rname + " "
        if count_list[1] > 0 and gname != "":
            result = result + str(count_list[1]) + gname + " "
        if count_list[2] > 0 and bname != "":
            result = result + str(count_list[2]) + bname + " "
        return result

    # Get stoichiometries.
    stoichs = []
    for scan in experiment.scans:
        for trace in scan.traces:
            trace_count = trace.stoichiometry
            stoich = convert_count_to_str(trace_count, color_names[0], color_names[1], color_names[2])
            stoichs.append(stoich)
    stoichs.sort()

    # Get histogram data.
    counter = Counter(stoichs)
    if not norm_per_scan:
        freqs = np.array(list(counter.values())) / np.sum(list(counter.values()))
    else:
        freqs = np.array(list(counter.values()))/experiment.n_scan_files
    names = list(counter.keys())
    x = np.arange(len(counter))

    data = dict()
    data['x'] = x.tolist()
    data['names'] = names
    data['freqs'] = freqs.tolist()
    data['norm_per_scan'] = bool(norm_per_scan)
    data['n_scans'] = int(experiment.n_scan_files)
    data['n_traces'] = int(experiment.n_traces)
    return data


def plot_stoichiometry(data):
    """Plot all stoichiometries in an experiment, using the data dictionary generated by get_stoichiometry()."""

    # Make plot.
    fig = plt.figure(figsize=(10, 6))
    ax = fig.add_subplot(111)

    # Plot histogram.
    plt.xlabel('stoichiometry')
    if not data['norm_per_scan']:
        plt.ylabel('ratio of occurrence')
    else:
        plt.ylabel('count per scan')
    ax.bar(data['x'], data['freqs'], align='center', width=0.8, color='gray')

    # Cleanup.
    ax.xaxis.set_major_locator(plt.FixedLocator(data['x']))
    ax.xaxis.set_major_formatter(plt.FixedFormatter(data['names']))
    plt.xticks(rotation=90)
    for xval, fval in zip(data['x'], data['freqs']):
        ax.text(xval, fval + 0.05, "%.2f" % fval, horizontalalignment='center')
    if data['n_traces'] > 0:
        plt.ylim((0, max(data['freqs']) + max([max(data['freqs']) // 3, 0.5])))
    plt.title("Stoichiometry, Nscans = " + str(data['n_scans']) + ", Nspots = " + str(data['n_traces']))
    fig.tight_layout()

    return fig


def get_location_histogram(experiment, n_bins=24, loc_kbp=False, from_center=False, norm_per_scan=False, cf_res=0.05):
    """Plot starting location of all Traces. You can set the number of bins in n_bins. By default,  location units
    are microns (switch to kbp with loc_kbp=True). Plot the location from the DNA center with from_center=True.
    You can normalize the results by scan with norm_per_scan=True; by default results are normalized by the total
    number of traces. Provide the confocal pixel resolution in cf_res, 0.05 micron by default."""

    locs = []
    for scan in experiment.scans:
        for trace in scan.traces:
            if np.sum(trace.stoichiometry) > 0:
                if not loc_kbp:
                    locs.append(trace.get_starting_loc(x_micron=True))
                else:
                    locs.append(trace.get_starting_loc(x_micron=False))
    locs = np.array(locs)
    # print(len(locs))

    data = dict()
    if not loc_kbp:
        dna_length_pixel = experiment.data_full['dna_end_pixel'] - experiment.data_full['dna_start_pixel']
        x_range = cf_res * float(dna_length_pixel.max())
        data['x_unit'] = 'micron'
    else:
        x_range = float(experiment.dna_length_kbp)
        data['x_unit'] = 'kbp'
    if loc_kbp and from_center:
        data['from_center'] = True
        x_range = x_range / 2
        locs = np.abs(x_range - locs)
    else:
        data['from_center'] = False

    hist, bin_edges = np.histogram(locs, bins=n_bins, range=(0, x_range))

    if norm_per_scan:
        hist = hist / experiment.n_scan_files
        data['norm_per_scan'] = True
    else:
        hist = hist / np.sum(hist)
        data['norm_per_scan'] = False
    bin_width = bin_edges[1] - bin_edges[0]
    bin_centers = bin_edges[:-1] + bin_width / 2
    data['bins'] = bin_centers.tolist()
    data['bin_width'] = float(bin_width)
    data['values'] = hist.tolist()
    data['locs_all'] = locs.tolist()
    data['n_scans'] = int(experiment.n_scan_files)

    return data


def plot_location_histogram(data):
    """Plot location histogram using the dictionary generated by get_location_histogram()."""

    fig = plt.figure(figsize=(10, 6))
    ax = fig.add_subplot(111)

    # Plot histogram.
    if data['norm_per_scan']:
        plt.ylabel('count per scan')
    else:
        plt.ylabel('ratio')
    if data['x_unit'] == 'micron':
        plt.xlabel('location (micron)')
    elif not data['from_center']:
        plt.xlabel('location (kbp)')
    else:
        plt.xlabel('distance from center (kbp)')

    ax.bar(data['bins'], data['values'], align='center', width=data['bin_width'], color='gray')
    plt.title('Location histogram')

    # Cleanup.
    fig.tight_layout()
    return fig


def get_diffusion(experiment, config, loc_kbp=False):
    """Get diffusion coefficients from traces in the experiment. We also need the config dictionary for diffusion
    calculations. Switch from microns to kbp with loc_kbp=True."""

    diff_coeffs = []
    dna_length_kbp = experiment.dna_length_kbp
    for scan in experiment.scans:
        dna_length_micron = scan.dna_length_micron
        for trace in scan.traces:
            track_diff_coeffs = []
            for track in trace.tracks:
                if track.step_count > 0:
                    x_vals = track.x_micron
                    photon_count = experiment.step_calibration['avg'][track.color_id]
                    if len(x_vals) > 5:
                        diff_coeff = ctrappy.diffusion.get_diffusion_coefficient(x_vals, config['diff_estimate'],
                                                                                 config['t_exposure'],
                                                                                 photon_count,
                                                                                 config['sigma_psf'],
                                                                                 float(experiment.frame_time_s))
                        track_diff_coeffs.append(diff_coeff)
            if len(track_diff_coeffs) > 0:
                diff_coeff = np.mean(track_diff_coeffs)
                if loc_kbp:
                    kbp_per_um = dna_length_kbp / dna_length_micron
                    diff_coeff = diff_coeff * (kbp_per_um**2)
                if not np.isnan(diff_coeff):
                    diff_coeffs.append(diff_coeff)

    return diff_coeffs


def get_diffusion_hist(experiment, config, lims=None, n_bins=30, loc_kbp=False):
    """Make diffusion coefficient histogram for the experiment. We also need the config dictionary for diffusion
    calculations. You can provide x-limits for log(D) as a 2-tuple of floats using the lims argument, and set the
    number of histogram bins with n_bins. Switch from microns to kbp with loc_kbp=True."""

    if lims is None and not loc_kbp:
        lims = (-10, 0)
    if lims is None and loc_kbp:
        lims = (-9, 1)

    diff_coeffs = get_diffusion(experiment, config, loc_kbp)
    diff_coeffs_log = np.log10(diff_coeffs)
    diff_coeffs_log = diff_coeffs_log[(diff_coeffs_log > lims[0]) & (diff_coeffs_log < lims[1])]

    counts, bin_edges = np.histogram(diff_coeffs_log, bins=n_bins, range=lims)

    # Calculate Wilson confidence interval.
    n_obs = len(diff_coeffs_log)
    errs = []
    for c in counts:
        if c > 0:
            err = list(statsmodels.stats.proportion.proportion_confint(c, n_obs, alpha=0.3173, method='wilson'))
            err[0] = c/n_obs - err[0]
            err[1] = err[1] - c/n_obs
            errs.append(err)
        else:
            errs.append([0, 0])
    errs = np.array(errs).swapaxes(0, 1)

    hist = counts / len(diff_coeffs_log)
    bins = np.array([(bin_edges[i + 1] + bin_edges[i]) / 2 for i in range(len(bin_edges) - 1)])

    data = {'bins': bins.tolist(),
            'values': hist.tolist(),
            'bin_width': float(bin_edges[1]-bin_edges[0]),
            'diff_coeffs_log': diff_coeffs_log.tolist(),
            'wilson_confint': errs.tolist()}

    if not loc_kbp:
        data['x_unit'] = 'micron'
    else:
        data['x_unit'] = 'kbp'

    return data


def fit_diffusion_hist(data, given_coeffs=None):

    logd = data['diff_coeffs_log']
    np.random.seed(0)

    if given_coeffs is None:

        binormal = ctrappy.diffusion_fit.binormal_gen(name='binormal')

        try:
            params1 = norm.fit(logd)
        except:
            print("Single peak fit failed.")
            params1 = (0, 0)  # mu, sigma

        # fit to two normal distributions
        try:
            params2 = binormal.fit(logd, floc=0, fscale=1)
        except:
            print("2 peak fit failed.")
            params2 = (0, 0, 0, 0, 0, 0, 1)  # mu, sigma, a1, mu2, sigma2

        logpdf1 = np.log10(ctrappy.diffusion_fit.gaussian(logd, *params1))
        logpdf2 = np.log10(ctrappy.diffusion_fit.bigaussian(logd, *params2[:5]))
        nll1 = -np.sum(logpdf1)  # 2 parameters
        bic1 = 2*np.log(len(logd)) + 2*nll1
        nll2 = -np.sum(logpdf2)  # 5 parameters
        bic2 = 5*np.log(len(logd)) + 2*nll2

        if bic1 <= bic2:
            return ctrappy.diffusion_fit.gaussian, params1
        else:
            return ctrappy.diffusion_fit.bigaussian, params2[:5]

    elif len(given_coeffs) == 2:
        binormal_const = ctrappy.diffusion_fit.binormal_gen_const('binormal', given_coeffs[0], given_coeffs[1])
        try:
            params2 = binormal_const.fit(logd, floc=0, fscale=1)
        except:
            print("2 peak fit failed.")
            params2 = (0, 0, 0, 0, 1)  # mu, sigma, a1, mu2, sigma2
        params_ret = (given_coeffs[0], params2[0], params2[1], given_coeffs[1], params2[2])
        return ctrappy.diffusion_fit.bigaussian, params_ret


def plot_diffusion_hist(data, fit_function=None, fit_params=None, show_wilson_confint=True):
    """Plot diffusion coefficient histogram using the dictionary generated with get_diffusion_hist()."""

    fig = plt.figure(figsize=(10, 6))
    plt.bar(data['bins'], data['values'], width=data['bin_width'], color='gray')
    plt.title("Diffusion histogram, N = " + str(len(data['diff_coeffs_log'])))
    if data['x_unit'] == 'micron':
        plt.xlabel('log(D) (micron**2/s)')
    else:
        plt.xlabel('log(D) (kbp**2/s)')
    if fit_function is not None:
        x = np.linspace(np.amin(data['bins']) - data['bin_width']/2, np.amax(data['bins']) + data['bin_width'], 100)
        plt.plot(x, data['bin_width'] * fit_function(x, *fit_params))
    if show_wilson_confint:
        plt.errorbar(data['bins'], data['values'], data['wilson_confint'],
                     capsize=4, linewidth=0, elinewidth=1, ecolor='k')
    plt.ylabel('ratio')
    plt.close()
    return fig


# Bootstrapping code by Kaley.
def get_diffusion_hist_bootstrap(experiment, config, lims=None, n_bins=30, n_samples=100,
                                 init_vals=None, loc_kbp=False):
    """Get diffusion coefficients from traces in a list of scans. Take n_samples number of bootstrap samples from the
    distribution and fit each bootstrap sample separately. Plot the mean D distribution with confidence intervals and
    average fit."""

    if lims is None and not loc_kbp:
        lims = (-10, 0)
    if lims is None and loc_kbp:
        lims = (-9, 1)
    if init_vals is None and not loc_kbp:
        init_vals = [-6.0, 1.0, 0.5, -2.0, 1.0]
    if init_vals is None and loc_kbp:
        init_vals = [-5.0, 1.0, 0.5, -1.0, 1.0]
    binormal = ctrappy.diffusion_fit.binormal_gen(name='binormal')

    data_coeffs = get_diffusion_hist(experiment, config, lims, n_bins, loc_kbp)
    diff_coeffs_log = np.array(data_coeffs['diff_coeffs_log'])
    bins = np.array(data_coeffs['bins'])
    width = data_coeffs['bin_width']

    boots, params = ctrappy.diffusion_fit.get_bootstrap(n_samples, diff_coeffs_log, init_vals)
    binmeans, binci, fit_params_log = ctrappy.diffusion_fit.fit_bootstrap(boots, params, lims, n_samples, n_bins)

    x_fit = np.arange(lims[0], lims[1], 0.1)
    y_fit = np.zeros(x_fit.shape)
    if len(fit_params_log) == 2:
        y_fit = norm.pdf(x_fit, *fit_params_log[:, 0])
    elif len(fit_params_log) == 5:
        y_fit = binormal.pdf(x_fit, *fit_params_log[:, 0])

    # Output data.
    data = {'bins': bins.tolist(),
            'bin_means': binmeans.tolist(),
            'conf_int': binci.tolist(),
            'bin_width': float(width),
            'x_fit': x_fit.tolist(),
            'y_fit': y_fit.tolist(),
            'n_samples': int(n_samples),
            'fit_params_log': fit_params_log.tolist(),
            'diff_coeffs_log': diff_coeffs_log.tolist()}

    if not loc_kbp:
        data['x_unit'] = 'micron'
    else:
        data['x_unit'] = 'kbp'

    return data


def plot_diffusion_hist_bootstrap(data):
    """Plot diffusion histogram with bootstrapping from dictionary generated with get_diffusion_hist_bootstrap()."""

    # plot a histogram of means with the c.i. as the error bar
    fig = plt.figure(figsize=(10, 6))
    plt.bar(data['bins'], data['bin_means'], width=data['bin_width'], color='gray')
    plt.errorbar(data['bins'], data['bin_means'], yerr=data['conf_int'],
                 elinewidth=1, capsize=4, ecolor="k", linewidth=0)
    plt.plot(data['x_fit'], data['y_fit'], c="k")
    plt.title("Diffusion fit, " + str(data['n_samples']) + " bootstrap samples")
    if data['x_unit'] == 'micron':
        plt.xlabel('log(D) (micron**2/s)')
    else:
        plt.xlabel('log(D) (kbp**2/s)')
    plt.ylabel('PDF')
    plt.close()

    return fig


def get_lifetime(experiment, time_s=False, single_step=True, multi_exp=False):
    """Get and fit trace lifetimes from the experiment; switch the time unit from frames to seconds with time_s=True.
    Set single_step to False if you input an experiment containing multistep bleaching data. For single step, multi-
    color data, the lifetime is taken on the Trace level by averaging over Track lifetimes. For multistep, multicolor
    data every step of every color is added to the lifetime histogram."""

    # Get max frame.
    max_frame = 0
    for scan in experiment.scans:
        if scan.n_frames > max_frame:
            max_frame = scan.n_frames

    xvals = np.arange(max_frame + 1)
    yvals = np.zeros(max_frame + 1)
    count = 0
    for scan in experiment.scans:
        for trace in scan.traces:
            trace_lifetimes = []
            for track in trace.tracks:
                if track.step_count > 0 and track.lifetime is not None:
                    if single_step:
                        trace_lifetimes.append(track.lifetime)
                    else:
                        for frame in track.step_frames:
                            t = frame - track.frame[0]
                            yvals[:t] = yvals[:t] + 1
                            count = count + 1
            if single_step and len(trace_lifetimes) > 0:
                t = int(np.mean(trace_lifetimes))
                yvals[:t] = yvals[:t] + 1
                count = count + 1

    xvals = xvals[:-1]
    yvals = yvals[1:]  # Get rid of first datapoint, because min_platform_size = 2 for stepfinding.
    yvals = yvals / yvals[0]

    # Fitting parameters
    models = []
    params = []
    results = []

    # Single-exponent fit.
    def single_exp_decay(x, a):
        return np.exp(- x / a)
    smodel = lmfit.Model(single_exp_decay)
    models.append(smodel)
    sparams = smodel.make_params(a=50.)
    params.append(sparams)

    # Double-exponent fit
    if multi_exp:
        def multi_exp_decay(x, p0, a0, p1, a1): 
            return p0 * single_exp_decay(x, a0) + p1 * single_exp_decay(x, a1)
        mmodel = lmfit.Model(multi_exp_decay)
        models.append(mmodel)
        mparams = mmodel.make_params(a0=50., a1=50., p0=.5, p1=.5)
        params.append(mparams)

    # Model selection 
    bic = np.infty
    i = -1
    for model, param in zip(models, params):
        # Start fitting
        result = model.fit(data=yvals, params=param, x=xvals)
        results.append(result)
        # Select best model based on BIC
        mbic = result.bic
        if mbic < bic:
            i += 1
    

    # params, cov = curve_fit(single_exp_decay, xvals, yvals, p0=(50,))
    # yfit = single_exp_decay(xvals, *params)
    # mean_lifetime = params[0]
            
    # Optimal parameters
    data = dict()
    if i == 0:
        mean_lifetime = results[i].values["a"]
        yfit = single_exp_decay(xvals, mean_lifetime)
        cov = results[i].covar
        data['mean_lifetime'] = float(mean_lifetime)
    elif i == 1:
        mean_lifetime0 = results[i].values["a0"]
        mean_lifetime1 = results[i].values["a1"]
        yfit = results[i].eval()
        cov = results[i].covar
        data["mean_lifetime"] = [mean_lifetime0, mean_lifetime1]

    if not time_s:
        data['t_unit'] = 'frame'
    else:
        data['t_unit'] = 's'
        xvals = xvals * experiment.frame_time_s
        mean_lifetime = mean_lifetime * experiment.frame_time_s
    data['xvals'] = xvals.tolist()
    data['yvals'] = yvals.tolist()
    data['count'] = count
    data['yfit'] = yfit.tolist()
    data['std'] = float(np.sqrt(np.diag(cov))[0])

    return data


def plot_lifetime(data):
    """Plot the lifetime data generated with get_lifetime()."""

    # Make fig.
    fig = plt.figure(figsize=(10, 6))
    plt.plot(data['xvals'], data['yvals'], color='k')
    plt.plot(data['xvals'], data['yfit'], color='k', linestyle='dashed')
    plt.title("Lifetime, N = %i,  mean lifetime = %.2f%s +/- %.2f%s" %
              (data['count'], data['mean_lifetime'], data['t_unit'], data['std'], data['t_unit']))
    plt.xlabel('t (' + data['t_unit'] + ')')
    plt.ylabel('survival probability')
    plt.close()

    return fig


def get_step_sizes(experiment, lims=None, n_bins=30):
    """Get distribution of step sizes from experiment. You can manually set the step size plotting limits with lims
    (a 2-tuple of floats), and choose the number of bins with n_bins."""

    step_sizes = []
    for scan in experiment.scans:
        for trace in scan.traces:
            for track in trace.tracks:
                step_sizes.extend(track.step_sizes)

    step_sizes = np.array(step_sizes)
    print('Minimum', np.amin(step_sizes))
    print('5th percentile', np.percentile(step_sizes, 5))
    print('10th percentile', np.percentile(step_sizes, 10))
    if lims is None:
        hist, bin_edges = np.histogram(step_sizes, bins=n_bins, range=(0, 1.1 * np.amax(step_sizes)))
    else:
        hist, bin_edges = np.histogram(step_sizes, bins=n_bins, range=lims)
    bins = np.array([(bin_edges[i + 1] + bin_edges[i]) / 2 for i in range(len(bin_edges) - 1)])

    data = dict()
    data['bins'] = bins.tolist()
    data['counts'] = hist.tolist()
    data['bin_width'] = float(bin_edges[1] - bin_edges[0])
    data['step_sizes'] = step_sizes.tolist()
    data['mean'] = float(np.mean(step_sizes))
    data['std'] = float(np.std(step_sizes))
    data['n_steps'] = len(step_sizes)

    return data


def plot_step_sizes(data, vlines=None):
    """Plot step size histogram data generated by get_step_sizes(); you can plot vertical lines using float values
    in a list passed to the argument vlines."""

    # Make fig.
    if vlines is None:
        vlines = []
    fig = plt.figure(figsize=(10, 6))
    plt.bar(data['bins'], data['counts'], color='gray', width=data['bin_width'])
    for vline in vlines:
        plt.axvline(vline, c='k', linestyle='dashed')
    plt.title('Step size distribution \n N = %i, mean = %.1f ADU, '
              'std = %.1f ADU' % (data['n_steps'], data['mean'], data['std']))
    plt.xlabel('step size (ADU)')
    plt.xlabel('count')
    plt.close()

    return fig


def get_spots_per_DNA(experiment):
    """Calculate the number of spots per DNA for an experiment; returns a dictionary."""

    data = dict()
    for scan in experiment.scans:
        n_spots = scan.total_spot_count()
        scan_id = int(scan.scan_id)
        if n_spots > 0:
            data[scan_id] = dict()
            data[scan_id]['n_spots'] = int(n_spots)
            data[scan_id]['stoichiometries'] = []
            for trace in scan.traces:
                if trace.total_count() > 0:
                    data[scan_id]['stoichiometries'].append(list([int(i) for i in trace.stoichiometry]))
            data[scan_id]['stoichiometries'].sort()
            data[scan.scan_id]['n_fluorophores'] = float(np.sum(data[scan_id]['stoichiometries']))
    return data


def plot_spots_per_DNA(data):
    """Plot the number of spots per DNA in a histogram from a dictionary generated with get_spots_per_DNA()."""

    # Number of spots.
    fig0 = plt.figure(figsize=(10, 6))
    spots_per_dna = np.array([row['n_spots'] for _, row in data.items()])
    labels, counts = np.unique(spots_per_dna, return_counts=True)
    plt.bar(labels, counts)
    plt.title('Spots per DNA')
    plt.ylabel('# spots')
    plt.ylabel('count')
    plt.close()

    # Spots and stoichiometries.
    fig1 = plt.figure(figsize=(10, 6))
    spots_per_dna_strs = np.array([str(row['n_spots']) + " spots: " + str(row['stoichiometries'])
                                   for _, row in data.items()])
    labels, counts = np.unique(spots_per_dna_strs, return_counts=True)
    plt.bar(labels, counts)
    plt.xticks(rotation=90)
    plt.title('Spot count and stoichiometry')
    plt.ylabel('count')
    plt.close()

    # Number of fluorophores
    fig2 = plt.figure(figsize=(10, 6))
    n_fluorophores = np.array([row['n_fluorophores'] for _, row in data.items()])
    labels, counts = np.unique(n_fluorophores, return_counts=True)
    plt.bar(labels, counts)
    plt.title('Fluorophores per DNA')
    plt.xlabel('# fluorophores')
    plt.ylabel('count')
    plt.close()

    return fig0, fig1, fig2


def get_processivity(experiment):
    """Get processivities, put in data dictionary."""

    # Get processivity.
    procs = []
    for scan in experiment.scans:
        for trace in scan.traces:
            if trace.total_count() > 0:
                trace_proc = []
                for track in trace.tracks:
                    if track.step_count > 0:
                        trace_proc.append(float(track.x_kbp[-1] - track.x_kbp[0]))
                # Get maximum absolute track processivity, save as trace processivity.
                procs.append(float(np.amax(np.absolute(trace_proc))) * 1000)
    data = {'processivities': procs}
    return data


def plot_processivity(data):
    """Plot result of get_processivity()."""

    # Make bins & histogram values.
    bin_size = 60  # In basepairs.
    bins = np.arange(0, np.amax(data['processivities']) + 2 * bin_size, bin_size)
    hist, bin_edges = np.histogram(data['processivities'], bins=bins)
    print(bins.shape, hist.shape)

    # Make histogram.
    fig = plt.figure(figsize=(10, 6))
    plt.bar(bins[:-1], hist / len(data['processivities']), width=bin_size)
    plt.title('Processivities')
    plt.xlabel('Processivity (bp)')
    plt.ylabel('Relative frequency')
    plt.close()
    return fig
