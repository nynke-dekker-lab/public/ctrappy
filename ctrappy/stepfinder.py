"""
Run step finding on a bleaching trace.
"""

import numpy as np
import ruptures


def find_steps(x, y, min_step_size, min_plateau_length=1):
    """Find steps in a signal using change-point analysis.

    Parameters
    ----------
    x : numpy array of floats
        x values
    y : numpy array of floats
        y values (signal).
    min_step_size : float
        Minimum step size.
    min_plateau_length : int
        Minimum length of plateaus. Default: 1.

    Returns
    -------
    step_locs : numpy array of ints
        Location indices of steps.
    step_sizes : numpy array of floats
        Sizes of steps.
    sig_det : numpy array of floats
        Detected signal, only containing steps.
    """

    # Do CPA
    est = ruptures.KernelCPD(kernel='linear', min_size=min_plateau_length, jump=1)
    signal = np.array(y).reshape((len(y), 1))
    penalty = min_step_size**2  # Because we are using the L2 cost function
    pred = [0] + est.fit_predict(signal, pen=penalty * np.log(len(y)))
    # print(pred)

    # Linear fits between change-points.
    sig_det = []
    for i in range(len(pred) - 1):
        y_seg = np.array(y)[pred[i]:pred[i + 1]]
        for j in range(len(y_seg)):
            sig_det.append(float(np.mean(y_seg)))
    sig_det = np.array(sig_det)

    # Prune away steps that are too small; start with smallest step.
    # Iterate until there are no steps that are under min_step_size.
    small_step = True
    step_locs = []
    step_sizes = []
    while small_step:

        # Store step locations and sizes.
        step_locs = []
        step_sizes = []
        for i in range(len(y) - 1):
            if sig_det[i+1] != sig_det[i]:
                step_locs.append(i+1)
                step_sizes.append(sig_det[i] - sig_det[i+1])
        step_locs = np.array(step_locs, dtype=int)
        step_sizes = np.array(step_sizes)
        # print(len(step_locs))

        if len(step_sizes) == 0:
            return [], [], sig_det

        # Get rid of smallest step.
        if np.amin(step_sizes) < min_step_size and len(step_sizes) > 0:
            i_min = step_sizes.argmin()
            if i_min > 0:
                x_start = step_locs[i_min - 1]
            else:
                x_start = 0
            if i_min < len(step_sizes) - 1:
                x_end = step_locs[i_min + 1]
            else:
                x_end = len(x)
            sig_det[x_start:x_end] = np.mean(sig_det[x_start:x_end])
        else:
            small_step = False

    # print(step_locs, step_sizes)
    # print(sig_det)
    return step_locs, step_sizes, sig_det
