# CTrapPy TIFF metadata

- [1. [Required] Property CTrapPy TIFF metadata > DNA length (micron)](#DNA_length_(micron))
- [2. [Required] Property CTrapPy TIFF metadata > bead0 center (micron)](#bead0_center_(micron))
- [3. [Required] Property CTrapPy TIFF metadata > bead1 center (micron)](#bead1_center_(micron))
- [4. [Required] Property CTrapPy TIFF metadata > red laser power (%)](#red_laser_power_(%))
- [5. [Required] Property CTrapPy TIFF metadata > green laser power (%)](#green_laser_power_(%))
- [6. [Required] Property CTrapPy TIFF metadata > blue laser power (%)](#blue_laser_power_(%))
- [7. [Optional] Property CTrapPy TIFF metadata > start time](#start_time)
- [8. [Optional] Property CTrapPy TIFF metadata > raw bead1 position x (micron)](#raw_bead1_position_x_(micron))
- [9. [Optional] Property CTrapPy TIFF metadata > raw bead1 position y (micron)](#raw_bead1_position_y_(micron))
- [10. [Optional] Property CTrapPy TIFF metadata > raw bead2 position x (micron)](#raw_bead2_position_x_(micron))
- [11. [Optional] Property CTrapPy TIFF metadata > raw bead2 position y (micron)](#raw_bead2_position_y_(micron))
- [12. [Optional] Property CTrapPy TIFF metadata > raw image width (pixels)](#raw_image_width_(pixels))
- [13. [Optional] Property CTrapPy TIFF metadata > raw DNA center position (micron)](#raw_DNA_center_position_(micron))
- [14. [Optional] Property CTrapPy TIFF metadata > raw scan volume center x (micron)](#raw_scan_volume_center_x_(micron))
- [15. [Optional] Property CTrapPy TIFF metadata > raw scan volume center y (micron)](#raw_scan_volume_center_y_(micron))
- [16. [Optional] Property CTrapPy TIFF metadata > estimated brightfield confocal shift (micron)](#estimated_brightfield_confocal_shift_(micron))

**Title:** CTrapPy TIFF metadata

|                           |                                                                           |
| ------------------------- | ------------------------------------------------------------------------- |
| **Type**                  | `object`                                                                  |
| **Required**              | No                                                                        |
| **Additional properties** | [[Any type: allowed]](# "Additional Properties of any type are allowed.") |

**Description:** Metadata file belonging to tiff file, coming from lumicks C/Q-Trap h5 data file.

<details>
<summary><strong> <a name="DNA_length_(micron)"></a>1. [Required] Property CTrapPy TIFF metadata > DNA length (micron)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** DNA length in microns.

</blockquote>
</details>

<details>
<summary><strong> <a name="bead0_center_(micron)"></a>2. [Required] Property CTrapPy TIFF metadata > bead0 center (micron)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Left bead center x-coordinate in microns; relative to center.

</blockquote>
</details>

<details>
<summary><strong> <a name="bead1_center_(micron)"></a>3. [Required] Property CTrapPy TIFF metadata > bead1 center (micron)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Right bead center x-coordinate in microns; relative to center.

</blockquote>
</details>

<details>
<summary><strong> <a name="red_laser_power_(%)"></a>4. [Required] Property CTrapPy TIFF metadata > red laser power (%)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Red laser power percentage.

</blockquote>
</details>

<details>
<summary><strong> <a name="green_laser_power_(%)"></a>5. [Required] Property CTrapPy TIFF metadata > green laser power (%)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Green laser power percentage.

</blockquote>
</details>

<details>
<summary><strong> <a name="blue_laser_power_(%)"></a>6. [Required] Property CTrapPy TIFF metadata > blue laser power (%)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Blue laser power percentage.

</blockquote>
</details>

<details>
<summary><strong> <a name="start_time"></a>7. [Optional] Property CTrapPy TIFF metadata > start time</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `string` |
| **Required** | No       |

**Description:** Start time and date of the scan.

</blockquote>
</details>

<details>
<summary><strong> <a name="raw_bead1_position_x_(micron)"></a>8. [Optional] Property CTrapPy TIFF metadata > raw bead1 position x (micron)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | No       |

**Description:** Bead 1 center x-coordinate in microns; absolute brightfield coordinates.

</blockquote>
</details>

<details>
<summary><strong> <a name="raw_bead1_position_y_(micron)"></a>9. [Optional] Property CTrapPy TIFF metadata > raw bead1 position y (micron)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | No       |

**Description:** Bead 1 center y-coordinate in microns; absolute brightfield coordinates.

</blockquote>
</details>

<details>
<summary><strong> <a name="raw_bead2_position_x_(micron)"></a>10. [Optional] Property CTrapPy TIFF metadata > raw bead2 position x (micron)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | No       |

**Description:** Bead 2 center x-coordinate in microns; absolute brightfield coordinates.

</blockquote>
</details>

<details>
<summary><strong> <a name="raw_bead2_position_y_(micron)"></a>11. [Optional] Property CTrapPy TIFF metadata > raw bead2 position y (micron)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | No       |

**Description:** Bead 2 center y-coordinate in microns; absolute brightfield coordinates.

</blockquote>
</details>

<details>
<summary><strong> <a name="raw_image_width_(pixels)"></a>12. [Optional] Property CTrapPy TIFF metadata > raw image width (pixels)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | No       |

**Description:** Confocal image width in pixels.

</blockquote>
</details>

<details>
<summary><strong> <a name="raw_DNA_center_position_(micron)"></a>13. [Optional] Property CTrapPy TIFF metadata > raw DNA center position (micron)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | No       |

**Description:** DNA center x-coordinate in microns; absolute brightfield coordinates.

</blockquote>
</details>

<details>
<summary><strong> <a name="raw_scan_volume_center_x_(micron)"></a>14. [Optional] Property CTrapPy TIFF metadata > raw scan volume center x (micron)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | No       |

**Description:** Image center x-coordinate in microns; absolute brightfield coordinates.

</blockquote>
</details>

<details>
<summary><strong> <a name="raw_scan_volume_center_y_(micron)"></a>15. [Optional] Property CTrapPy TIFF metadata > raw scan volume center y (micron)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | No       |

**Description:** Image center y-coordinate in microns; absolute brightfield coordinates.

</blockquote>
</details>

<details>
<summary><strong> <a name="estimated_brightfield_confocal_shift_(micron)"></a>16. [Optional] Property CTrapPy TIFF metadata > estimated brightfield confocal shift (micron)</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | No       |

**Description:** Estimated x-shift in microns by taking scan center minus DNA center.

</blockquote>
</details>

----------------------------------------------------------------------------------------------------------------------------
Generated using [json-schema-for-humans](https://github.com/coveooss/json-schema-for-humans) on 2023-05-01 at 16:32:43 +0200