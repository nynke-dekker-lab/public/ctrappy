# CTrapPy GUI default settings

- [1. [Required] Property CTrapPy GUI default settings > initialdir_h5](#initialdir_h5)
- [2. [Required] Property CTrapPy GUI default settings > initialdir_tiff](#initialdir_tiff)
- [3. [Required] Property CTrapPy GUI default settings > initialdir_yaml](#initialdir_yaml)
- [4. [Required] Property CTrapPy GUI default settings > laser_color_default](#laser_color_default)
- [5. [Required] Property CTrapPy GUI default settings > offset_method_default](#offset_method_default)
- [6. [Required] Property CTrapPy GUI default settings > display_max_default](#display_max_default)
- [7. [Required] Property CTrapPy GUI default settings > dna_length_micron_default](#dna_length_micron_default)
- [8. [Required] Property CTrapPy GUI default settings > log_radius_default](#log_radius_default)
- [9. [Required] Property CTrapPy GUI default settings > log_threshold_default](#log_threshold_default)
- [10. [Required] Property CTrapPy GUI default settings > tracking_skip_frames_default](#tracking_skip_frames_default)
- [11. [Required] Property CTrapPy GUI default settings > tracking_max_dist_default](#tracking_max_dist_default)
- [12. [Required] Property CTrapPy GUI default settings > brightfield_confocal_shift_default](#brightfield_confocal_shift_default)
- [13. [Required] Property CTrapPy GUI default settings > dna_length_kbp_default](#dna_length_kbp_default)
- [14. [Required] Property CTrapPy GUI default settings > frame_time_default](#frame_time_default)
- [15. [Required] Property CTrapPy GUI default settings > starting_frame_default](#starting_frame_default)
- [16. [Required] Property CTrapPy GUI default settings > loc_range_kbp_margin_default](#loc_range_kbp_margin_default)
- [17. [Required] Property CTrapPy GUI default settings > total_count_default](#total_count_default)

**Title:** CTrapPy GUI default settings

|                           |                                                                           |
| ------------------------- | ------------------------------------------------------------------------- |
| **Type**                  | `object`                                                                  |
| **Required**              | No                                                                        |
| **Additional properties** | [[Any type: allowed]](# "Additional Properties of any type are allowed.") |

**Description:** File for setting the default parameters for the CTrapPy GUI.

<details>
<summary><strong> <a name="initialdir_h5"></a>1. [Required] Property CTrapPy GUI default settings > initialdir_h5</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `string` |
| **Required** | Yes      |

**Description:** Initial directory to look in for h5 files. If you always want to look in the previously opened directory, set it to ''.

</blockquote>
</details>

<details>
<summary><strong> <a name="initialdir_tiff"></a>2. [Required] Property CTrapPy GUI default settings > initialdir_tiff</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `string` |
| **Required** | Yes      |

**Description:** Initial directory to look in for tiff files. If you always want to look in the previously opened directory, set it to ''.

</blockquote>
</details>

<details>
<summary><strong> <a name="initialdir_yaml"></a>3. [Required] Property CTrapPy GUI default settings > initialdir_yaml</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `string` |
| **Required** | Yes      |

**Description:** Initial directory to look in for yml files. If you always want to look in the previously opened directory, set it to ''.

</blockquote>
</details>

<details>
<summary><strong> <a name="laser_color_default"></a>4. [Required] Property CTrapPy GUI default settings > laser_color_default</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `string` |
| **Required** | Yes      |

**Description:** Default laser colours for manual offset; 'r', 'g', 'b', 'rg', 'rb', 'gb', or 'rgb'.

</blockquote>
</details>

<details>
<summary><strong> <a name="offset_method_default"></a>5. [Required] Property CTrapPy GUI default settings > offset_method_default</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `string` |
| **Required** | Yes      |

**Description:** Default offest method; 'metadata', 'manual', 'bead detection', or 'bead detection, different file'.

</blockquote>
</details>

<details>
<summary><strong> <a name="display_max_default"></a>6. [Required] Property CTrapPy GUI default settings > display_max_default</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Default max ADU count for setting image contrast.

</blockquote>
</details>

<details>
<summary><strong> <a name="dna_length_micron_default"></a>7. [Required] Property CTrapPy GUI default settings > dna_length_micron_default</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Default DNA length in micron for manual offset.

</blockquote>
</details>

<details>
<summary><strong> <a name="log_radius_default"></a>8. [Required] Property CTrapPy GUI default settings > log_radius_default</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Default LoG detection radius in pixels.

</blockquote>
</details>

<details>
<summary><strong> <a name="log_threshold_default"></a>9. [Required] Property CTrapPy GUI default settings > log_threshold_default</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | Yes     |

**Description:** Default LoG detection threshold; numbers for RGB separately.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="tracking_skip_frames_default"></a>10. [Required] Property CTrapPy GUI default settings > tracking_skip_frames_default</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Default max time skip for tracking (frames).

</blockquote>
</details>

<details>
<summary><strong> <a name="tracking_max_dist_default"></a>11. [Required] Property CTrapPy GUI default settings > tracking_max_dist_default</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Default max distance skip for tracking (pixels).

</blockquote>
</details>

<details>
<summary><strong> <a name="brightfield_confocal_shift_default"></a>12. [Required] Property CTrapPy GUI default settings > brightfield_confocal_shift_default</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Default brightfield-confocal shift (microns).

</blockquote>
</details>

<details>
<summary><strong> <a name="dna_length_kbp_default"></a>13. [Required] Property CTrapPy GUI default settings > dna_length_kbp_default</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Default DNA length (kbp).

</blockquote>
</details>

<details>
<summary><strong> <a name="frame_time_default"></a>14. [Required] Property CTrapPy GUI default settings > frame_time_default</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Default frame time (s/frame).

</blockquote>
</details>

<details>
<summary><strong> <a name="starting_frame_default"></a>15. [Required] Property CTrapPy GUI default settings > starting_frame_default</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Filtering parameter; tracks must start before this time (frame).

</blockquote>
</details>

<details>
<summary><strong> <a name="loc_range_kbp_margin_default"></a>16. [Required] Property CTrapPy GUI default settings > loc_range_kbp_margin_default</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Filtering parameter; tracks must start further away than this distance from a bead (kbp).

</blockquote>
</details>

<details>
<summary><strong> <a name="total_count_default"></a>17. [Required] Property CTrapPy GUI default settings > total_count_default</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Filtering parameter; tracks must have a stoichiometry lower than this count.

</blockquote>
</details>

----------------------------------------------------------------------------------------------------------------------------
Generated using [json-schema-for-humans](https://github.com/coveooss/json-schema-for-humans) on 2023-05-01 at 16:32:39 +0200