# CTrapPy configuration

- [1. [Optional] Property CTrapPy configuration > initial_bead_fit](#initial_bead_fit)
- [2. [Required] Property CTrapPy configuration > colocalization_distance](#colocalization_distance)
- [3. [Optional] Property CTrapPy configuration > diff_estimate](#diff_estimate)
- [4. [Optional] Property CTrapPy configuration > t_exposure](#t_exposure)
- [5. [Optional] Property CTrapPy configuration > sigma_psf](#sigma_psf)
- [6. [Required] Property CTrapPy configuration > step_counting_divide](#step_counting_divide)
- [7. [Required] Property CTrapPy configuration > confocal_pixel_size](#confocal_pixel_size)
- [8. [Optional] Property CTrapPy configuration > min_step_sizes_r](#min_step_sizes_r)
- [9. [Optional] Property CTrapPy configuration > min_step_sizes_g](#min_step_sizes_g)
- [10. [Optional] Property CTrapPy configuration > min_step_sizes_b](#min_step_sizes_b)
- [11. [Optional] Property CTrapPy configuration > min_step_sizes_rg](#min_step_sizes_rg)
- [12. [Optional] Property CTrapPy configuration > min_step_sizes_rb](#min_step_sizes_rb)
- [13. [Optional] Property CTrapPy configuration > min_step_sizes_gb](#min_step_sizes_gb)
- [14. [Optional] Property CTrapPy configuration > min_step_sizes_rgb](#min_step_sizes_rgb)
- [15. [Optional] Property CTrapPy configuration > min_final_step_size](#min_final_step_size)
- [16. [Optional] Property CTrapPy configuration > avg_step_sizes_r](#avg_step_sizes_r)
- [17. [Optional] Property CTrapPy configuration > avg_step_sizes_g](#avg_step_sizes_g)
- [18. [Optional] Property CTrapPy configuration > avg_step_sizes_b](#avg_step_sizes_b)
- [19. [Optional] Property CTrapPy configuration > avg_step_sizes_rg](#avg_step_sizes_rg)
- [20. [Optional] Property CTrapPy configuration > avg_step_sizes_rb](#avg_step_sizes_rb)
- [21. [Optional] Property CTrapPy configuration > avg_step_sizes_gb](#avg_step_sizes_gb)
- [22. [Optional] Property CTrapPy configuration > avg_step_sizes_rgb](#avg_step_sizes_rgb)
- [23. [Required] Property CTrapPy configuration > bleed_through](#bleed_through)

**Title:** CTrapPy configuration

|                           |                                                                           |
| ------------------------- | ------------------------------------------------------------------------- |
| **Type**                  | `object`                                                                  |
| **Required**              | No                                                                        |
| **Additional properties** | [[Any type: allowed]](# "Additional Properties of any type are allowed.") |

**Description:** Configuration file for CTrapPy scan processing.

<details>
<summary><strong> <a name="initial_bead_fit"></a>1. [Optional] Property CTrapPy configuration > initial_bead_fit</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers for automatic bead fitting; expected amplitude (ADU), left edge of left bead (px), right edge of left bead (px), left edge of right bead (px), right edge of right bead (px), avg background signal (ADU).

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="colocalization_distance"></a>2. [Required] Property CTrapPy configuration > colocalization_distance</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Colocalization distance in microns.

</blockquote>
</details>

<details>
<summary><strong> <a name="diff_estimate"></a>3. [Optional] Property CTrapPy configuration > diff_estimate</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | No       |

**Description:** Diffusion coefficient estimate in micron^2/s; needed for diffusion calculation.

</blockquote>
</details>

<details>
<summary><strong> <a name="t_exposure"></a>4. [Optional] Property CTrapPy configuration > t_exposure</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | No       |

**Description:** Estimate of exposure time for a spot in seconds; needed for diffusion calculation.

</blockquote>
</details>

<details>
<summary><strong> <a name="sigma_psf"></a>5. [Optional] Property CTrapPy configuration > sigma_psf</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | No       |

**Description:** Estimate of PSF size in microns; needed for diffusion calculation.

</blockquote>
</details>

<details>
<summary><strong> <a name="step_counting_divide"></a>6. [Required] Property CTrapPy configuration > step_counting_divide</strong>  

</summary>
<blockquote>

|              |           |
| ------------ | --------- |
| **Type**     | `boolean` |
| **Required** | Yes       |

**Description:** Do we divide detected bleaching steps by the average expected step size?

</blockquote>
</details>

<details>
<summary><strong> <a name="confocal_pixel_size"></a>7. [Required] Property CTrapPy configuration > confocal_pixel_size</strong>  

</summary>
<blockquote>

|              |          |
| ------------ | -------- |
| **Type**     | `number` |
| **Required** | Yes      |

**Description:** Confocal pixel size in microns.

</blockquote>
</details>

<details>
<summary><strong> <a name="min_step_sizes_r"></a>8. [Optional] Property CTrapPy configuration > min_step_sizes_r</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; minimum step counts for R, G, B if R laser is on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="min_step_sizes_g"></a>9. [Optional] Property CTrapPy configuration > min_step_sizes_g</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; minimum step counts for R, G, B if G laser is on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="min_step_sizes_b"></a>10. [Optional] Property CTrapPy configuration > min_step_sizes_b</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; minimum step counts for R, G, B if B laser is on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="min_step_sizes_rg"></a>11. [Optional] Property CTrapPy configuration > min_step_sizes_rg</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; minimum step counts for R, G, B if R, G lasers are on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="min_step_sizes_rb"></a>12. [Optional] Property CTrapPy configuration > min_step_sizes_rb</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; minimum step counts for R, G, B if R, B lasers are on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="min_step_sizes_gb"></a>13. [Optional] Property CTrapPy configuration > min_step_sizes_gb</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; minimum step counts for R, G, B if G, B lasers are on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="min_step_sizes_rgb"></a>14. [Optional] Property CTrapPy configuration > min_step_sizes_rgb</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; minimum step counts for R, G, B if R, G, B lasers are on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="min_final_step_size"></a>15. [Optional] Property CTrapPy configuration > min_final_step_size</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; minimum step counts for R, G, B if R, G, B lasers are on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="avg_step_sizes_r"></a>16. [Optional] Property CTrapPy configuration > avg_step_sizes_r</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; average step counts for R, G, B if R laser is on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="avg_step_sizes_g"></a>17. [Optional] Property CTrapPy configuration > avg_step_sizes_g</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; average step counts for R, G, B if G laser is on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="avg_step_sizes_b"></a>18. [Optional] Property CTrapPy configuration > avg_step_sizes_b</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; average step counts for R, G, B if B laser is on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="avg_step_sizes_rg"></a>19. [Optional] Property CTrapPy configuration > avg_step_sizes_rg</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; average step counts for R, G, B if R, G lasers are on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="avg_step_sizes_rb"></a>20. [Optional] Property CTrapPy configuration > avg_step_sizes_rb</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; average step counts for R, G, B if R, B lasers are on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="avg_step_sizes_gb"></a>21. [Optional] Property CTrapPy configuration > avg_step_sizes_gb</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; average step counts for R, G, B if G, B lasers are on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="avg_step_sizes_rgb"></a>22. [Optional] Property CTrapPy configuration > avg_step_sizes_rgb</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | No      |

**Description:** Array of 3 numbers; average step counts for R, G, B if R, G, B lasers are on.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

<details>
<summary><strong> <a name="bleed_through"></a>23. [Required] Property CTrapPy configuration > bleed_through</strong>  

</summary>
<blockquote>

|              |         |
| ------------ | ------- |
| **Type**     | `array` |
| **Required** | Yes     |

**Description:** 3x3x3 array; crosstalk matrices.

|                      | Array restrictions |
| -------------------- | ------------------ |
| **Min items**        | N/A                |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | N/A                |

</blockquote>
</details>

----------------------------------------------------------------------------------------------------------------------------
Generated using [json-schema-for-humans](https://github.com/coveooss/json-schema-for-humans) on 2023-05-01 at 16:32:30 +0200