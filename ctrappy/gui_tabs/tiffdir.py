import streamlit as st
import ctrappy.file
import ctrappy.scan
import ctrappy.tracking
import ctrappy.gui_figures
import ctrappy.gui_tabs.tools.file_caching as cache
import ctrappy.gui_tabs.tools.tiff_functions as tiff_functions
import os
import matplotlib.pyplot as plt
import numpy as np
import glob
import pandas as pd
import time


def run():

    # Get GUI default parameters.
    gui_defaults = cache.gui_defaults()

    # Select directory.
    dir_path = cache.select_dir(initialdir=gui_defaults['initialdir_h5'],
                                button_text="Select directory")
    if dir_path is None:
        st.warning('No valid directory selected.')
        return
    st.sidebar.markdown(dir_path)

    # Select and open config file.
    config_path = cache.select_file(filetypes="config yaml file (*.yml *.YML *.yaml *.YAML)",
                                    initialdir=gui_defaults['initialdir_yaml'],
                                    button_text="Select config file",
                                    cache_id=1)
    if config_path is None:
        st.warning('No valid config file selected.')
        return
    st.sidebar.markdown(config_path)
    config = cache.file_cache2(config_path, filetype='yaml')

    # Write config info.
    st.write("# Config")
    st.write("Configuration parameters:")
    st.json(config, expanded=False)

    remove_single_spots = st.sidebar.checkbox('remove disconnected spots', value=True)
    offset_autocorrelation = st.sidebar.checkbox('override manual offset with autocorrelation', value=False)
    offset_autocorrelation_color = None
    if offset_autocorrelation:
        offset_autocorrelation_color = st.sidebar.selectbox('color for autocorrelation', options=['r', 'g', 'b'])

    # Other parameters.
    dna_length_kbp = st.sidebar.number_input('dna length (kbp)', 0.000, 100.000,
                                             gui_defaults['dna_length_kbp_default'], 0.001, format='%.3f')
    frame_time = st.sidebar.number_input('time between frames (s)', 0.01, 1000.,
                                         gui_defaults['frame_time_default'], 0.01, format='%.2f')
    show_frames = st.sidebar.checkbox('show first frames', value=True)
    autosave_check = st.sidebar.checkbox('autosave to data_dir/scans<timestamp>.csv', value=False)
    remove_edge_spots = st.sidebar.checkbox('remove edge spots', value=False)

    st.markdown('# Batch tiff processing')

    # Read params.
    st.markdown('## Parameters')
    params = cache.file_cache3(os.path.join(dir_path, 'params_offset_tracking.yaml'),
                               filetype='yaml')
    st.json(params, expanded=False)

    # Read tiffs.
    st.markdown('## Files')
    tiff_paths = glob.glob(os.path.join(dir_path, '*.tiff'))
    st.write(tiff_paths)

    # Run analysis.
    if st.button('Run analysis'):
        st.markdown('## Results')
        df_full = pd.DataFrame()
        offset_values_list = np.zeros((len(tiff_paths), 2))
        for i, tiff_path in enumerate(tiff_paths):

            st.write('- Processing (%i/%i) ' % (i + 1, len(tiff_paths)) + tiff_path + '...')

            # Get image.
            keep_scan = True
            image = ctrappy.file.read_biotiff(tiff_path)

            # Get offset.
            if params['offset']['offset method'] == 'manual':

                # Manually set offset values.
                offset_values = params['offset']['offset values (pixels)']
                offset_values_list[i, :] = offset_values
                image.apply_offset_mask(offset_values)

            if params['offset']['offset method'] == 'metadata':

                # Find and read yaml file.
                tiffnoext = os.path.splitext(tiff_path)[0]
                metadata_filename = tiffnoext + '.yaml'
                metadata = ctrappy.file.read_yaml(metadata_filename, schema='tiff_metadata')

                shift = params['offset']['brightfield confocal shift (micron)']
                offset_values = tiff_functions.get_offset_from_metadata(metadata, config, shift)
                offset_values_list[i, :] = offset_values
                image.apply_offset_mask(offset_values)

            if params['offset']['offset method'] == 'bead detection':

                # Project intensity according to user input.
                intensity_data = tiff_functions.project_intensity(image, params['offset']['offset frame'],
                                                                  params['offset']['offset color'])
                bead_detector = ctrappy.tracking.BeadDetector(config['initial_bead_fit'])
                bead_params, bead_fit = bead_detector.detect(intensity_data)
                dna_center = (bead_params[2] + bead_params[3]) / 2
                dna_length_micron = params['offset']['dna length (micron)']
                offset_values = (dna_center - dna_length_micron / config['confocal_pixel_size'] / 2,
                                 dna_center + dna_length_micron / config['confocal_pixel_size'] / 2)
                offset_values_list[i, :] = offset_values
                image.apply_offset_mask(offset_values)

            if params['offset']['offset method'] == 'bead detection, different file':

                # Get offset image.
                file_offset = tiff_functions.get_offset_file(tiff_path, params['offset']['offset file'])
                image_offset = cache.file_cache4(file_offset, filetype='tiff')
                if image == image_offset:
                    keep_scan = False
                else:
                    # Project intensity according to user input.
                    intensity_data = tiff_functions.project_intensity(image_offset, 'all frames', params['offset']['offset color'])
                    bead_detector = ctrappy.tracking.BeadDetector(config['initial_bead_fit'])
                    bead_params, bead_fit = bead_detector.detect(intensity_data)
                    dna_center = (bead_params[2] + bead_params[3]) / 2
                    dna_length_micron = params['offset']['dna length (micron)']
                    offset_values = (dna_center - dna_length_micron / config['confocal_pixel_size'] / 2,
                                     dna_center + dna_length_micron / config['confocal_pixel_size'] / 2)
                    offset_values_list[i, :] = offset_values
                    image.apply_offset_mask(offset_values)

            if keep_scan:

                # Get tracked spots and tracks.
                spots = image.get_spots(params['detection']['thresholds'],
                                        params['detection']['radius (pixels)'],
                                        offset_values_list[i, :],
                                        colors=tiff_functions.get_color_inds(params['detection']['laser colors']),
                                        remove_edge_spots=remove_edge_spots)
                if show_frames:
                    fig = ctrappy.gui_figures.plot_scan_spots(image, 0, 5, offset_values_list[i, :], spots,
                                                              params['detection']['radius (pixels)'])
                    st.write(fig)

                tracks = image.track_spots(spots, params['tracking']['max frame skip'],
                                           params['tracking']['max dist between spots (pixels)'])

                if not tracks.empty:
                    tracks['scan_id'] = i
                    tracks['dna_start_pixel'] = offset_values_list[i, 0]
                    tracks['dna_end_pixel'] = offset_values_list[i, 1]
                    tracks['n_scans'] = len(tiff_paths)
                    tracks['n_frames'] = image.n_frames

                    # Prepare.
                    if remove_single_spots:
                        single_inds = np.array([])
                        for j, track in tracks.groupby('track_id'):
                            if len(track) == 1:
                                single_inds = np.concatenate((single_inds, track.index.values))
                        tracks = tracks[~tracks.index.isin(single_inds)]

                    # Run colocalization.
                    colocalizer = ctrappy.tracking.Colocalizer(config['colocalization_distance'],
                                                               config['bleed_through'],
                                                               cf_res=config['confocal_pixel_size'])
                    df_tracks_converted = tiff_functions.tracks_to_scan_df(tracks, tiff_path)
                    tracks_col = colocalizer.colocalize(df_tracks_converted, params['detection']['laser colors'])

                    # Append to scans.
                    if not tracks_col.empty:
                        df_full = pd.concat((df_full, tracks_col), ignore_index=True)

                del image  # This probably doesn't help to free up memory at all...

        if df_full.empty:
            st.warning('No spots detected.')
            return

        # Get experiment object.
        experiment = ctrappy.scan.Experiment(df_full, config, params['detection']['laser colors'])
        experiment.set_t(frame_time)

        # Do offset correction.
        if params['offset']['offset method'] == 'manual' and offset_autocorrelation:

            # Try 5 pixels left and right.
            try_offsets = np.arange(-5, 6)
            def_marg = float(gui_defaults['loc_range_kbp_margin_default'])
            loc_filter = [def_marg, dna_length_kbp - def_marg]
            corr_vals = experiment.convert_to_kbp_autocorrelation(dna_length_kbp, loc_filter=loc_filter,
                                                                  color=offset_autocorrelation_color,
                                                                  n_bins=32, offsets=try_offsets)

            if np.amax(corr_vals) > 0:
                corr_vals = corr_vals / np.amax(corr_vals)
            fig = plt.figure(figsize=(10, 4))
            plt.plot(try_offsets, corr_vals)
            plt.xlabel('offset shift (pixel)')
            plt.ylabel('correlation / max correlation')
            plt.close()
            st.write(fig)

        else:
            experiment.convert_to_kbp(dna_length_kbp)

        # Plot locations over time.
        if not remove_single_spots:
            data = ctrappy.gui_figures.get_location_traces(experiment, time_s=True, loc_kbp=True,
                                                           show_single_spots=True)
        else:
            data = ctrappy.gui_figures.get_location_traces(experiment, time_s=True, loc_kbp=True,
                                                           show_single_spots=False)
        fig = ctrappy.gui_figures.plot_location_traces(data)
        plt.tight_layout()
        st.write(fig)

        # Export entire dataframe.
        st.markdown('## Entire dataset')
        df = experiment.to_dataframe()
        st.dataframe(df)
        link_csv = ctrappy.file.download_link(df, 'scans.csv', 'save table to csv')
        st.markdown(link_csv, unsafe_allow_html=True)
        if autosave_check:
            autocsv_path = os.path.join(dir_path, 'scans' + str(int(time.time())) + '.csv')
            df.to_csv(autocsv_path)
            st.write("Scans file saved to "+autocsv_path)
