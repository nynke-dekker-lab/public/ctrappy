import os
import glob
import streamlit as st
import pandas as pd
import numpy as np
from lmfit import Model
import ctrappy.file
import ctrappy.gui_tabs.tools.file_caching as cache
import ctrappy.fd_curve as fd_curve

def run():

    # Get GUI default parameters.
    gui_defaults = cache.gui_defaults()

        # Select directory.
    dir_path = cache.select_dir(initialdir=gui_defaults['initialdir_fd'],
                                 button_text="Select directory"
                                 )
    
    if dir_path is None:
        st.warning('No valid directory selected.')
        return
    st.sidebar.markdown(dir_path)

    # Select params_fd file.
    params_fd_path = cache.select_file(filetypes="config yaml file (*.yml *.YML *.yaml *.YAML)",
                                    initialdir=gui_defaults['initialdir_yaml'],
                                    button_text="Select params_fd file",
                                    cache_id=1)
    
    if params_fd_path is None:
        st.warning('No valid config file selected.')
        return
    
    st.sidebar.markdown(params_fd_path)
    params_fd = cache.file_cache2(params_fd_path, filetype='yaml')
    params_fd['data_dir'] = dir_path

    # Config
    st.write("# Config")
    st.write("Configuration parameters:")
    st.json(params_fd, expanded=False)

    # User input parameters
    st.write('### Fitting parameters')
    params_fd['Lc'] = st.number_input(label=r'Contour length expected: $\hat{L}_c$ $[ \, nm \, ]$', min_value=0., value=float(8000), step=0.001, format='%.3f')
    params_fd['Lc_lower'], params_fd['Lc_upper'] = st.slider(label=r'Contour length boundaries in $[ \, nm \, ]$', min_value=0., max_value=10000., value=(0., 10000.), step =1., format='%.0f')
    params_fd['Lp'] = st.number_input(label=r'Persistence length expected: $\hat{L}_p$ $[ \, nm \, ]$', min_value=0., value=float(50), step=0.001, format='%.3f')
    params_fd['Lp_lower'], params_fd['Lp_upper'] = st.slider(label=r'Persistence length boundaries in $[ \, nm \, ]$', min_value=0., max_value=100., value=(0., 100.), step =1., format='%.0f')
    params_fd['S'] = st.number_input(label=r'Stretch modulus expected: $\hat{S}$ $[ \, pN \, ]$', min_value=0., value=float(1500), step=0.001, format='%.3f')
    params_fd['S_lower'], params_fd['S_upper'] = st.slider(label=r'Stretch modulus boundaries in $[ \, nm \, ]$', min_value=0., max_value=12000., value=(0., 10000.), step =1., format='%.0f')

    st.write('### Experiment parameters')
    params_fd['T'] = st.number_input(label='Temperature: $T$ $[^\circ C \, ]$', min_value=0., value=float(gui_defaults['T_default']), step=0.01, format='%.2f')
    kB = 0.013806 # Boltzmann constant in pN*nm*K-1
    params_fd['kBT'] = kB * (273.15 + params_fd['T'])
    params_fd['d_start'] = st.number_input(label=r'Distance at the start point: $d_0$ $[ \, \mu m \, ]$', value =2.0, min_value=0., step=0.1, format='%.1f')
    params_fd['dna_length_kbp'] = st.number_input(label=r'DNA length: $L_{\text{DNA}}$ $[ \, kbp \, ]$', min_value=0., value=float(gui_defaults['dna_length_default_DNA']), step=0.001, format='%.3f')

    # Read csv files.
    st.markdown('## Files')
    files = glob.glob(os.path.join(dir_path, "*_fdcurves.csv")) + glob.glob(os.path.join(dir_path, "*_fddata.csv"))
    st.write(files)
    
    # Fitting
    st.write("# Fitting")
    wlc_model_user = st.radio("Select elasticity model:", ["Odijk eWLC", "WLC", "WLC+7th order corrections", "WLC+enthalpic correction", "WLC+7th order corrections + enthalpic correction"], captions=["Odijk, T. (1995)", "Bustamante C, Marko JF, Siggia ED, Smith S. (1994)", "C. Bouchiat, M.D. Wang, J.-F. Allemand, T. Strick, S.M. Block, V. Croquette (1999)", "Wang, Y ; McAllister, T. A. ; Zobell, D. R. ; Pickard, M. D. ; Rode, L. M. ; Mir, Z. ; Cheng, K. J. (1997)", "C. Bouchiat, M.D. Wang, J.-F. Allemand, T. Strick, S.M. Block, V. Croquette (1999)"])
    params_fd['force_min'], params_fd['force_max'] = st.slider(label=r'Define the high/low force threshold in $[ \, pN \, ]$', min_value=0., max_value=50., value=(5., 35.), step =0.5, format='%.1f')
    params_fd['d_min'], params_fd['d_max'] = st.slider(label=r'Define the high/low distance threshold in $[ \, \mu m \, ]$', min_value=0., max_value=10., value=(2., 6.), step =0.5, format='%.1f')

    if st.button("Start fitting"):
        # Transform model names
        if wlc_model_user == "WLC+7th order corrections":
            wlc_model = "bouchiat"
            method = "basinhopping"
        elif wlc_model_user == "Odijk eWLC":
            wlc_model = "odijk"
            method = "leastsq"
        elif wlc_model_user == "WLC":
            wlc_model = "WLC"
            method = "basinhopping"
        elif wlc_model_user == "WLC+enthalpic correction":
            wlc_model = "extWLC"
            method = "basinhopping"
        elif wlc_model_user == "WLC+7th order corrections + enthalpic correction":
            wlc_model = "extbouchiat"
            method = "leastsq"

        # Empty list to store models.
        models = []

        # Empty dataframe to store results.
        df_full = pd.DataFrame(columns=['opt_Lc[nm]', 'opt_Lp[nm]', 'opt_S[pN]', 'nFittings', 'filename'])

        for filename in files:
            st.divider()
            st.info(os.path.basename(filename))

            d, F, d_raw, F_raw = fd_curve.read_force_file(filename, params_fd)
            if (len(d) == 0 | len(F) == 0):
                st.warning('File gives empty dataset, check the data')
                continue
            else:
                data = d, F

            model = fd_curve.WormLikeChain(model=wlc_model)
            model.compile(params_fd)
            df_file = model.fit(data, params_fd['min_delta'], params_fd['max_iters'], os.path.basename(filename), method=method)
            avg_Chisqr = model.df_full.groupby('filename').mean().loc[filename[len(dir_path) + 1:]]['Chisqr']
            if avg_Chisqr <= np.infty:
                st.write("Chi squared: {:.5f}".format(avg_Chisqr))
                df_full = pd.concat([df_full, df_file], ignore_index=True)
            else:
                st.warning('Chi squared too large after fit, data filtered out')

            fig = model.plot(data, os.path.basename(filename))
            st.write(fig)
            models.append(model)

        # Save fitted values summary for all the files
        mean = df_full[['opt_Lc[nm]', 'opt_Lp[nm]', 'opt_S[pN]']].mean().values
        sem = df_full[['opt_Lc[nm]', 'opt_Lp[nm]', 'opt_S[pN]']].sem().values
        df_sum = pd.DataFrame([mean, sem])
        df_sum.columns = ['Lc[nm]', 'Lp[nm]', 'S[pN]']
        df_sum.index = ['mean', 'sem']

        # Full table
        st.divider()
        st.write("### Full dataframe")
        st.dataframe(df_full)
        link_df = ctrappy.file.download_link(df_full, 'fd_fitting_full.csv', 'Download dataframe')
        st.write(link_df, unsafe_allow_html=True)
        # Summary table
        st.divider()
        st.write("### Summary dataframe")
        st.dataframe(df_sum)
        link_df = ctrappy.file.download_link(df_sum, 'fd_fitting_summary.csv', 'Download dataframe')
        st.write(link_df, unsafe_allow_html=True)
        