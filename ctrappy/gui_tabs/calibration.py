import streamlit as st
import ctrappy.file
import ctrappy.scan
import ctrappy.tracking
import ctrappy.gui_figures
import matplotlib.pyplot as plt
import ctrappy.gui_tabs.tools.file_caching as cache
import numpy as np


def run():

    # Get GUI default parameters.
    gui_defaults = cache.gui_defaults()

    # Select and open file.
    csv_path = cache.select_file(filetypes="scans csv file (*.csv *.CSV)",
                                 initialdir=gui_defaults['initialdir_tiff'],
                                 button_text="Select scans csv file",
                                 cache_id=0)
    if csv_path is None:
        st.warning('No valid csv file selected.')
        return
    st.sidebar.markdown(csv_path)
    df_scans = cache.read_file(csv_path, filetype='csv')

    # Select and open config file.
    config_path = cache.select_file(filetypes="config yaml file (*.yml *.YML *.yaml *.YAML)",
                                    initialdir=gui_defaults['initialdir_yaml'],
                                    button_text="Select config file",
                                    cache_id=1)
    if config_path is None:
        st.warning('No valid config file selected.')
        return
    st.sidebar.markdown(config_path)
    config = cache.file_cache2(config_path, filetype='yaml')
    config['step_counting_divide'] = False

    # Write config info.
    st.write("# Config")
    st.write("Configuration parameters:")
    st.json(config, expanded=False)

    # Set color; select allowed stoichiometries.
    st.markdown('# Fluorophore parameters')
    f_color = st.selectbox(label='fluorophore color', options=['r', 'g', 'b'])
    if f_color == 'r':
        r_stoich = (1, 10000)
        g_stoich = (0, 0)
        b_stoich = (0, 0)
        r_stoich_single = (1, 1)
        g_stoich_single = (0, 0)
        b_stoich_single = (0, 0)
    elif f_color == 'g':
        r_stoich = (0, 0)
        g_stoich = (1, 10000)
        b_stoich = (0, 0)
        r_stoich_single = (0, 0)
        g_stoich_single = (1, 1)
        b_stoich_single = (0, 0)
    else:
        r_stoich = (0, 0)
        g_stoich = (0, 0)
        b_stoich = (1, 10000)
        r_stoich_single = (0, 0)
        g_stoich_single = (0, 0)
        b_stoich_single = (1, 1)

    # Get experiment object; only select data of selected color.
    df_color = df_scans[df_scans['color_str'] == f_color]  # TODO: can be cached
    if df_color.empty:
        st.warning('Warning: no spots found in this color')
    else:
        experiment = ctrappy.scan.Experiment(df_color, config,  # TODO: can be cached
                                             df_scans['laser_colors'].iloc[0])

        # Set minimum step size.
        min_step_size = st.number_input(label='minimum step size', min_value=1, max_value=10000, value=100, step=1)
        experiment.step_calibration = {'min': [min_step_size, min_step_size, min_step_size],
                                       'avg': [0, 0, 0]}  # Does nothing.
        for scan in experiment.scans:
            for trace in scan.traces:
                trace.run_calibration_step_count(experiment.step_calibration)

        st.write('Vary the minimum step size *d<sub>I</sub>*, until you get a normal distribution with '
                 '*d<sub>I</sub> <= μ<sub>I</sub> - 2 σ<sub>I</sub>*. The resulting values for *d<sub>I</sub>* '
                 'and *μ<sub>I</sub>* can be used as `min_step_size` and `avg_step_size`, respectively.',
                 unsafe_allow_html=True)
        expected_peak = st.number_input(label='expected fluorophore location (kbp)', min_value=0.,
                                        max_value=experiment.dna_length_kbp, step=0.001, format='%.3f')
        selection_margin = st.number_input(label='margin around peak for trace selection (kbp)', min_value=0.,
                                           max_value=experiment.dna_length_kbp, step=0.001, format='%.3f')

        # Filtering.
        st.markdown('# Initial filtering')
        st.write('starting time up to, location, total count')

        starting_frame = st.number_input(label='include traces starting up to frame', min_value=0, max_value=1000,
                                         value=gui_defaults['starting_frame_default'], step=1)
        def_marg = float(gui_defaults['loc_range_kbp_margin_default'])
        kbp_locs = st.slider(label='include traces within location range (kbp)', min_value=float(0.),
                             max_value=float(experiment.dna_length_kbp),
                             value=(def_marg, float(experiment.dna_length_kbp) - def_marg), format='%.3f', step=0.001)
        total_count = st.number_input(label='include traces up to total count', min_value=0, max_value=1000,
                                      value=gui_defaults['total_count_default'], step=1)

        experiment_filtered = experiment.filter_by_starting_frame(starting_frame)
        experiment_filtered = experiment_filtered.filter_by_loc_kbp(kbp_locs[0], kbp_locs[1], keep_coloc=False)
        experiment_filtered = experiment_filtered.filter_by_total_count(total_count + 1, keep_coloc=False)

        experiment_filtered = experiment_filtered.filter_by_stoichiometry(r_stoich, g_stoich, b_stoich)
        experiment_filtered_single = experiment_filtered.filter_by_stoichiometry(r_stoich_single, g_stoich_single,
                                                                                 b_stoich_single)


        # Plotting.
        st.markdown('# Analysis')

        experiment_filtered.count_traces()
        if experiment_filtered.n_traces == 0:
            st.warning('Warning: no traces to analyze.')

        if st.button('generate figures'):

            output_data = dict()

            st.write('## Results for single fluorophore traces')

            data = ctrappy.gui_figures.get_step_sizes(experiment_filtered_single)
            fig = ctrappy.gui_figures.plot_step_sizes(data, vlines=[min_step_size])
            output_data['step size dist'] = data
            st.write(fig)

            data = ctrappy.gui_figures.get_lifetime(experiment_filtered_single, time_s=True)
            fig = ctrappy.gui_figures.plot_lifetime(data)
            output_data['lifetime (s)'] = data
            st.write(fig)

            st.write('## Results for full experiment')

            data = ctrappy.gui_figures.get_bleaching_traces(experiment_filtered, time_s=True)
            fig = ctrappy.gui_figures.plot_bleaching_traces(data)
            output_data['bleaching traces'] = data
            st.write(fig)

            data = ctrappy.gui_figures.get_stoichiometry(experiment_filtered)
            fig = ctrappy.gui_figures.plot_stoichiometry(data)
            output_data['stoichiometry'] = data
            st.write(fig)

            data = ctrappy.gui_figures.get_location_traces(experiment_filtered, time_s=True, loc_kbp=True)
            peak0 = expected_peak
            peak1 = experiment.dna_length_kbp - expected_peak
            d = selection_margin
            fig = ctrappy.gui_figures.plot_location_traces(data, lines=[peak0 - d, peak0, peak0 + d,
                                                                        peak1 - d, peak1, peak1 + d])
            output_data['location traces (kbp)'] = data
            plt.tight_layout()
            st.write(fig)

            # Calculate brightfield to confocal offset.
            # TODO: move this into a neat function
            experiment_peak0 = experiment_filtered.filter_by_loc_kbp(peak0 - d, peak0 + d)
            experiment_peak1 = experiment_filtered.filter_by_loc_kbp(peak1 - d, peak1 + d)
            peak0_locs = []
            micron_per_kbp = []
            for scan in experiment_peak0.scans:
                micron_per_kbp.append(scan.dna_length_micron / experiment_peak0.dna_length_kbp)
                for trace in scan.traces:
                    peak0_locs.append(trace.get_starting_loc(x_micron=False))
            peak1_locs = []
            for scan in experiment_peak1.scans:
                for trace in scan.traces:
                    peak1_locs.append(trace.get_starting_loc(x_micron=False))
            micron_per_kbp = np.mean(micron_per_kbp)
            shift0 = (np.mean(peak0_locs) - peak0) * micron_per_kbp
            shift1 = (np.mean(peak1_locs) - peak1) * micron_per_kbp
            shift = np.mean([shift0, shift1])
            shifted_data = experiment_filtered.to_dataframe()
            shifted_data['dna_start_pixel'] = shifted_data['dna_start_pixel'] + shift / config['confocal_pixel_size']
            shifted_data['dna_end_pixel'] = shifted_data['dna_end_pixel'] + shift / config['confocal_pixel_size']
            experiment_shifted = ctrappy.scan.Experiment(shifted_data, config, shifted_data['laser_colors'].iloc[0])
            experiment_shifted.convert_to_kbp(experiment_shifted.dna_length_kbp)
            if f_color == 'r':
                experiment_shifted.step_calibration = {'min': [min_step_size, 10000, 10000],
                                                       'avg': [min_step_size * 2, 20000, 20000],
                                                       'min_final': [min_step_size, 10000, 10000]}
            elif f_color == 'g':
                experiment_shifted.step_calibration = {'min': [10000, min_step_size, 10000],
                                                       'avg': [20000, min_step_size * 2, 20000],
                                                       'min_final': [10000, min_step_size, 10000]}
            else:
                experiment_shifted.step_calibration = {'min': [10000, 10000, min_step_size],
                                                       'avg': [20000, 20000, min_step_size * 2],
                                                       'min_final': [10000, 10000, min_step_size]}
            for scan in experiment_shifted.scans:
                for trace in scan.traces:
                    trace.run_calibration_step_count(experiment.step_calibration)

            # Show corrected data.
            st.write('## Shift correction')
            st.write('Detected shift (micron):')
            st.write(shift)
            st.write('Individual spots are shifted by (micron, left/right respectively)')
            st.write(shift0, shift1)
            st.write(' ')
            st.write('Individual spots are shifted by (kbp, left/right respectively)')
            st.write(shift0 / micron_per_kbp, shift1 / micron_per_kbp)
            st.write(' ')
            dist_spots_expected_kbp = experiment_shifted.dna_length_kbp - 2 * peak0
            stretch_found_kbp = (shift1 / micron_per_kbp - shift0 / micron_per_kbp)
            stretch_factor = 1 - stretch_found_kbp / dist_spots_expected_kbp
            calculated_pixel_size = config['confocal_pixel_size'] * stretch_factor
            st.write('Calculated stretch factor & calculated pixel size:')
            st.write(stretch_factor, calculated_pixel_size)

            data = ctrappy.gui_figures.get_location_traces(experiment_shifted, time_s=True, loc_kbp=True)
            fig = ctrappy.gui_figures.plot_location_traces(data, lines=[peak0, peak1])
            output_data['location traces, shift corrected (kbp)'] = data
            st.write(fig)

            link_yaml = ctrappy.file.download_link(output_data, 'figure_data.yaml', 'save figure data')
            st.markdown(link_yaml, unsafe_allow_html=True)

            lens = []
            for scan in experiment_filtered.scans:
                lens.append(scan.dna_length_micron)
            st.write(lens)
