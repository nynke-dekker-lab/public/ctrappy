import streamlit as st
import pandas as pd
import numpy as np
import os
import glob
import ctrappy.file
import ctrappy.gui_tabs.tools.file_caching as cache
import ctrappy.fd_curve as fd_curve

def run():

    # Get GUI default parameters.
    gui_defaults = cache.gui_defaults()

    # Select directory.
    dir_path = cache.select_dir(initialdir=gui_defaults['initialdir_fd'],
                                 button_text="Select directory"
                                 )
    
    if dir_path is None:
        st.warning('No valid directory selected.')
        return
    st.sidebar.markdown(dir_path)
    
    # Select params_fd file.
    params_fd_path = cache.select_file(filetypes="config yaml file (*.yml *.YML *.yaml *.YAML)",
                                    initialdir=gui_defaults['initialdir_yaml'],
                                    button_text="Select params_fd file",
                                    cache_id=1)
    
    if params_fd_path is None:
        st.warning('No valid config file selected.')
        return
    st.sidebar.markdown(params_fd_path)
    params_fd = cache.file_cache2(params_fd_path, filetype='yaml')
    params_fd['data_dir'] = dir_path

    # Config
    st.write("# Config")
    st.write("Configuration parameters:")
    st.json(params_fd, expanded=False)

    # User input parameters
    st.write('### Experiment parameters')
    params_fd['T'] = st.number_input(label='Temperature: $T$ $[^\circ C \, ]$', min_value=0., value=float(gui_defaults['T_default']), step=0.01, format='%.2f')
    kB = 0.013806 # Boltzmann constant in pN*nm*K-1
    params_fd['kBT'] = kB * (273.15 + params_fd['T'])
    params_fd['d_start'] = st.number_input(label=r'Distance at the start point: $d_0$ $[ \, \mu m \, ]$', value =1.5, min_value=0., step=0.1, format='%.1f')
    st.write('### DNA parameters')
    params_fd['Lp'] = st.number_input(label=r'Persistence length: $L_p$ $[ \, nm \, ]$', min_value=0., value=float(gui_defaults['Lp_default']), step=0.001, format='%.3f')
    params_fd['S'] = st.number_input(label=r'Stretch modulus: $S$ $[ \, pN \, ]$', min_value=0., value=float(gui_defaults['S_default']), step=0.001, format='%.3f')
    params_fd['dna_length_kbp'] = st.number_input(label=r'DNA length: $L_{\text{DNA}}$ $[ \, kbp \, ]$', value=float(gui_defaults['dna_length_default']), min_value=0., step=0.001, format='%.3f')


    # Analysis
    st.markdown("# Analysis")
    if st.button("Generate analysis"):
        files = glob.glob(os.path.join(dir_path, "*_fddata.csv")) + glob.glob(os.path.join(dir_path, "*_fddata.csv"))

        # Empty dataframe to store results.
        df_summary = pd.DataFrame(columns=['filename', 'n_jumps', 'final_Lc[nm]', 'avg_delta_Lc[nm]'])
        df_full = pd.DataFrame(columns=['filename', 'breaking_force[pN]', 'delta_Lc[nm]'])

        # Read and process each file
        for filename in files:
            st.divider()
            st.info(filename[len(dir_path) + 1:])
            
            # Read file.
            d, F, d_raw, F_raw = fd_curve.read_force_file(filename, params_fd)
            if (len(d) == 0 | len(F) == 0):
                st.warning('File gives empty dataset, check the data')
                continue
            
            # Calculate the contour length in nm
            Lc = 1000 * d / (1 - 0.5 * (params_fd['kBT'] / (F * params_fd['Lp']))**0.5 + F / params_fd['S'])
            
            # Run step finder.
            cpa_result = fd_curve.changepoint_analysis(F, Lc, params_fd['min_platform_length'], params_fd['cpa_penalty'])
            x_fit, y_fit, break_F, delta_Lc, residuals = cpa_result
            
            # Check residuals.
            mse = np.mean(residuals**2)
            st.write('Mean squared error: {:.3f}'.format(mse))
            if mse <= params_fd['max_mse']:
                # Filter and store results
                df_res = pd.DataFrame({'filename': [filename for _ in break_F],
                                    'breaking_force[pN]': break_F, 
                                    'delta_Lc[nm]': delta_Lc})
                df_res = df_res[(df_res['breaking_force[pN]'] >= params_fd['delta_Lc_min']) & \
                                (df_res['breaking_force[pN]'] <= params_fd['delta_Lc_max'])]
                df_file_info = pd.DataFrame({'filename': [filename], 
                                            'n_jumps': [len(df_res)], 
                                            'final_Lc[nm]': [y_fit[-1][0]], 
                                            'avg_delta_Lc[nm]': [np.mean(delta_Lc)]})
                # Filter by Lc
                if params_fd['min_Lc'] <= df_file_info['final_Lc[nm]'][0] <= params_fd['max_Lc']:     
                    st.write('Final Lc value within filtering bounds.')
                    df_full = pd.concat((df_full, df_res), ignore_index=True)
                    df_summary = pd.concat((df_summary, df_file_info), ignore_index=True)
            else:
                st.warning('MSE too large after fit, data filtered out')
                
            # Plot the contour length-force curve as well as the fitted steps
            fig = fd_curve.plot_Lc_analysis(filename[len(dir_path) + 1:], d_raw, F_raw, F, Lc, x_fit, y_fit)
            st.write(fig)

        # Summary table.
        st.divider()
        st.write("### Summary dataframe")
        # Change columns order
        cols = df_summary.columns.tolist()
        cols = cols[1:] + cols[:1]
        st.dataframe(df_summary[cols])
        link_df = ctrappy.file.download_link(df_summary, 'fd_analysis_summary.csv', 'Download summary dataframe')
        st.write(link_df, unsafe_allow_html=True)

        # Full table
        st.divider()
        st.write("### Full dataframe")
        # Change columns order
        cols = df_full.columns.tolist()
        cols = cols[1:] + cols[:1]
        st.dataframe(df_full[cols])
        link_df = ctrappy.file.download_link(df_full, 'fd_analysis_full.csv', 'Download full dataframe')
        st.write(link_df, unsafe_allow_html=True)
