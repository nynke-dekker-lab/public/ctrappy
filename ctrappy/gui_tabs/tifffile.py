import streamlit as st
import ctrappy.file
import ctrappy.scan
import ctrappy.tracking
import ctrappy.gui_figures
import ctrappy.gui_tabs.tools.file_caching as cache
import ctrappy.gui_tabs.tools.tiff_functions as tiff_functions
import ctrappy.image
import os
import matplotlib.pyplot as plt
import numpy as np


def run():

    # Get GUI default parameters.
    gui_defaults = cache.gui_defaults()

    # Select and open tiff file.
    file_path = cache.select_file(filetypes="tiff (*.tiff *.TIFF *.tif *.TIF)",
                                  initialdir=gui_defaults['initialdir_tiff'],
                                  button_text="Select tiff file",
                                  cache_id=0)
    if file_path is None:
        st.warning('No valid file selected.')
        return
    st.sidebar.markdown(file_path)
    image = cache.file_cache(file_path, filetype='tiff')

    # Select and open config file.
    config_path = cache.select_file(filetypes="config yaml file (*.yml *.YML *.yaml *.YAML)",
                                    initialdir=gui_defaults['initialdir_yaml'],
                                    button_text="Select config file",
                                    cache_id=1)
    if config_path is None:
        st.warning('No valid config file selected.')
        return
    st.sidebar.markdown(config_path)
    config = cache.file_cache2(config_path, filetype='yaml')

    # Write config info.
    st.write("# Config")
    st.write("Configuration parameters:")
    st.json(config, expanded=False)

    # Choose offset method; autocorrelation makes no sense for single scan.]
    dict_options = {'manual': 0,
                    'metadata': 1,
                    'bead detection (experimental)': 2,
                    'bead detection, different file (very experimental)': 3}
    offset_method = st.sidebar.selectbox('offset method', options=list(dict_options.keys()),
                                         index=dict_options[gui_defaults['offset_method_default']])

    # Choose laser colors.
    laser_colors = ''
    if offset_method != 'metadata':
        laser_options = ['r', 'g', 'b', 'rg', 'rb', 'gb', 'rgb']
        laser_dict = {laser_options[i]: i for i in range(len(laser_options))}
        laser_colors = st.sidebar.selectbox('laser colors', options=['r', 'g', 'b', 'rg', 'rb', 'gb', 'rgb'],
                                            index=laser_dict[gui_defaults['laser_color_default']])

    dna_length_micron = None
    brightfield_confocal_shift = None
    offset_values = None
    offset_color = None
    offset_frame = None
    offset_file = None
    if offset_method == 'manual':

        # Manually set offset values.
        dna_length_micron = st.sidebar.number_input('dna length (micron)', 0.1, 1000.,
                                                    gui_defaults['dna_length_micron_default'], 0.01,
                                                    format='%.3f')
        center = image.data.shape[2] / 2
        default_offset = int(center - dna_length_micron / 2 / config['confocal_pixel_size'])
        default_offset = np.clip(default_offset, -1000, 1000)
        offset_left = st.sidebar.number_input('left bead edge (pixels)', -1000, 1000, default_offset, 1)
        offset_right = int(np.round(offset_left + dna_length_micron / config['confocal_pixel_size']))
        offset_values = (offset_left, offset_right)

    if offset_method == 'metadata':

        # Find and read yaml file.
        st.write("# Metadata")
        tiffnoext = os.path.splitext(file_path)[0]
        metadata_filename = tiffnoext + '.yaml'
        metadata = ctrappy.file.read_yaml(metadata_filename, schema='tiff_metadata')
        st.json(metadata, expanded=False)

        laser_colors = ''
        if metadata['red laser power (%)'] > 0:
            laser_colors = laser_colors + 'r'
        if metadata['green laser power (%)'] > 0:
            laser_colors = laser_colors + 'g'
        if metadata['blue laser power (%)'] > 0:
            laser_colors = laser_colors + 'b'
        st.write('Detected laser colors: ' + laser_colors)

        brightfield_confocal_shift = st.number_input('shift between brightfield and confocal images (micron)', -100.,
                                                     100., gui_defaults['brightfield_confocal_shift_default'], 0.001,
                                                     format='%.d')
        offset_values = tiff_functions.get_offset_from_metadata(metadata, config, brightfield_confocal_shift)

    if offset_method == 'bead detection (experimental)':

        # Get dna_length, color and frame(s) for intensity projection.
        dna_length_micron = st.sidebar.number_input('dna length (micron)', 0.1, 1000.,
                                                    gui_defaults['dna_length_micron_default'], 0.01, format='%.3f')
        offset_color = st.sidebar.selectbox('offset color', options=['r', 'g', 'b', 'rg', 'rb', 'gb', 'rgb'])
        offset_frame = st.sidebar.selectbox('offset frame', options=['first frame', 'all frames', 'final frame'])

        # Project intensity according to user input.
        intensity_data = tiff_functions.project_intensity(image, offset_frame, offset_color)
        bead_detector = ctrappy.tracking.BeadDetector(config['initial_bead_fit'])
        params, bead_fit = bead_detector.detect(intensity_data)
        dna_center = (params[2] + params[3]) / 2
        offset_values = (dna_center - dna_length_micron/config['confocal_pixel_size']/2,
                         dna_center + dna_length_micron/config['confocal_pixel_size']/2)

        # Plot result and apply offset.
        st.write("# Bead detection")
        fig = ctrappy.gui_figures.plot_bead_detection(intensity_data, bead_fit, offset_values, image)
        st.write(fig)

    if offset_method == 'bead detection, different file (very experimental)':

        # Get dna_length, color and frame(s) for intensity projection.
        dna_length_micron = st.sidebar.number_input('dna length (micron)', 0.1, 1000.,
                                                    gui_defaults['dna_length_micron_default'], 0.01, format='%.3f')
        offset_color = st.sidebar.selectbox('offset color', options=['r', 'g', 'b', 'rg', 'rgb'])
        offset_file = st.sidebar.selectbox('offset file',
                                           options=['first scan in h5 file', 'last scan in h5 file'])

        # Find offset file.
        offset_file_path = tiff_functions.get_offset_file(file_path, offset_file)
        st.sidebar.markdown(f'offset file: {offset_file_path}')
        image_offset = cache.file_cache3(offset_file_path, filetype='tiff')

        # Project intensity according to user input.
        intensity_data = tiff_functions.project_intensity(image_offset, 'all frames', offset_color)
        bead_detector = ctrappy.tracking.BeadDetector(tuple(map(float,
                                                                config['initial_bead_fit'][1:-1].split(', '))))
        params, bead_fit = bead_detector.detect(intensity_data)
        dna_center = (params[2] + params[3]) / 2
        offset_values = (dna_center - dna_length_micron/config['confocal_pixel_size']/2,
                         dna_center + dna_length_micron/config['confocal_pixel_size']/2)

        # Plot result and apply offset.
        st.write("# Bead detection")
        fig = ctrappy.gui_figures.plot_bead_detection(intensity_data, bead_fit, offset_values, image)
        st.write(fig)

    # Apply offset to image.
    image.apply_offset_mask(offset_values)

    # Detection threshold, frame and max display selection.
    st.markdown('# Spot detection')
    if not isinstance(gui_defaults['log_threshold_default'], list):
        def_thres = [gui_defaults['log_threshold_default'], gui_defaults['log_threshold_default'],
                     gui_defaults['log_threshold_default']]
    else:
        def_thres = gui_defaults['log_threshold_default']

    threshold_r = st.number_input(label='LoG detection threshold r', min_value=0.01, max_value=10.,
                                  value=def_thres[0], step=0.01)
    threshold_g = st.number_input(label='LoG detection threshold g', min_value=0.01, max_value=10.,
                                  value=def_thres[1], step=0.01)
    threshold_b = st.number_input(label='LoG detection threshold b', min_value=0.01, max_value=10.,
                                  value=def_thres[2], step=0.01)
    radius = st.number_input(label='LoG radius', min_value=1., max_value=10.,
                             value=gui_defaults['log_radius_default'], step=0.1)
    if image.n_frames > 1:
        frame = st.slider(label='frame', min_value=0, max_value=image.n_frames - 1, value=0)
    else:
        frame = 0
    display_max = st.slider(label='display max', min_value=1, max_value=20,
                            value=gui_defaults['display_max_default'])

    # Plot single frame with spot detections.
    spots = cache.cache_frame_spots(image.data, [threshold_r, threshold_g, threshold_b], radius,
                                    offset_values, laser_colors, frame=frame)
    fig = ctrappy.gui_figures.plot_scan_spots(image, frame, display_max, offset_values, spots, radius)
    st.write(fig)

    # Do spot detection on all frames and run tracking.
    if st.checkbox('run spot tracking'):

        st.markdown('# Spot tracking')

        # Params.
        frame_skip = st.number_input(label='max frame skip', min_value=0, max_value=100,
                                     value=gui_defaults['tracking_skip_frames_default'], step=1)
        spot_dist = st.number_input(label='max dist between spots (pixels)', min_value=0.5, max_value=40.,
                                    value=gui_defaults['tracking_max_dist_default'], step=0.5)

        # Get tracked spots and tracks.
        spots = cache.cache_all_spots(image.data, [threshold_r, threshold_g, threshold_b], radius, offset_values, laser_colors)
        tracks = cache.cache_tracks(spots, frame_skip, spot_dist).copy()

        if not tracks.empty:

            # Make experiment object.
            colocalizer = ctrappy.tracking.Colocalizer(config['colocalization_distance'],
                                                       config['bleed_through'],
                                                       cf_res=config['confocal_pixel_size'])
            tracks['scan_id'] = 0
            tracks['n_scans'] = 1
            tracks['n_frames'] = image.n_frames
            tracks['dna_start_pixel'] = offset_values[0]
            tracks['dna_end_pixel'] = offset_values[1]
            df_tracks = tiff_functions.tracks_to_scan_df(tracks, file_path)
            tracks_col = colocalizer.colocalize(df_tracks, laser_colors)
            experiment = ctrappy.scan.Experiment(tracks_col, config, laser_colors)

            if experiment.n_traces > 0:

                # Plot locations over time.
                data = ctrappy.gui_figures.get_location_traces(experiment, show_single_spots=True)
                fig = ctrappy.gui_figures.plot_location_traces(data, plot_id=True)
                plt.tight_layout()
                st.write(fig)

                # Plot intensities over time.
                data = ctrappy.gui_figures.get_bleaching_traces(experiment)
                fig = ctrappy.gui_figures.plot_bleaching_traces(data)
                st.pyplot(fig)

                # Show tracks in dataframe.
                # st.dataframe(tracks_col)
                # link_csv = ctrappy.file.download_link(tracks_col, 'tracks.csv', 'save table to csv')
                # st.markdown(link_csv, unsafe_allow_html=True)
                df = experiment.to_dataframe()
                st.dataframe(df)
                link_csv = ctrappy.file.download_link(df, 'scans.csv', 'save table to csv')
                st.markdown(link_csv, unsafe_allow_html=True)

                # Download detection/tracking/offset configuration to yaml.
                offset_dict = {'offset method': offset_method}
                if offset_method == 'manual':
                    offset_dict['offset values (pixels)'] = list(offset_values)
                    offset_dict['dna length (micron)'] = dna_length_micron
                elif offset_method == 'metadata':
                    offset_dict['brightfield confocal shift (micron)'] = brightfield_confocal_shift
                elif offset_method == 'bead detection (experimental)':
                    offset_dict['offset color'] = offset_color
                    offset_dict['offset frame'] = offset_frame
                    offset_dict['dna length (micron)'] = dna_length_micron
                elif offset_method == 'bead detection, different file':
                    offset_dict['offset color'] = offset_color
                    offset_dict['offset file'] = offset_file
                    offset_dict['dna length (micron)'] = dna_length_micron
                detection_dict = {'laser colors': laser_colors,
                                  'thresholds': [threshold_r, threshold_g, threshold_b],
                                  'radius (pixels)': radius}
                tracking_dict = {'max frame skip': frame_skip,
                                 'max dist between spots (pixels)': spot_dist}
                params_dict = {'offset': offset_dict,
                               'detection': detection_dict,
                               'tracking': tracking_dict}
                link_yaml = ctrappy.file.download_link(params_dict, 'params_offset_tracking.yaml',
                                                       'save offset & tracking parameters')
                str_yaml = link_yaml + ' (*put this file named **params_offset_tracking.yaml** ' \
                                       'in the same directory as the tiffs for batch processing*) '
                st.markdown(str_yaml, unsafe_allow_html=True)

            else:

                st.warning('No traces found. If there are detections, '
                           'try decreasing the step sizes in config.yaml')

        else:

            st.warning('No spots detected.')
