# CTrapPy GUI

## Open h5 file

On this page you can open an h5 file, get an idea of the data contained in it,
and choose scans to export to tiff files for further processing.

1. First, select an h5 file. Once it has loaded, select a scan contained in the 
   h5 file under 'scan number'. The scan will appear on the page.
2. Under the scan itself, you can see the intensity signal per color projected
   onto the x-axis. From this graph, you can already estimate how many fluorophores
   are in the frame. At the very bottom you see the total intensity of each frame
   over time. In that graph you can get an idea of to what extent the fluorophores
   have bleached during the scan capture.
3. You can change the image contrast with the 'display max' slider. Change the
   displayed frame under 'frame'.
4. Select the scans that you would like to export to tiff files under 'scans to
   export to tiff'. Press 'select directory and export' to save these files. The
   metadata is saved alongside the tiff files, in yaml format.

## Export h5 directory to tiffs (batch processing)

Here you can select a directory, from which all h5 files will be converted to tiffs
(plus metadata files) into a selected output directory. The input directory will be 
searched in a recursive manner, so the h5 files can be in subdirectories of the input 
directory.

1. Select an input directory. All the h5 files found in this input directory will
   be listed under 'input files'.
2. Select an export directory; the page will start showing what scans have been
   exported.
   
**Please note** that any h5 files exported to tiff using this page might still contain
test scans, empty scans and outliers with large aggregates. In order to filter out
these kinds of scans you are strongly advised to use the 'open h5 file' tab instead.
There you can manually remove these scans so that they do not end up in your tiff 
dataset.

## Process tiff file

In this tab you can open a tiff file, try out different parameters for offset
determination and spot detection/tracking, and save all the corresponding parameters
to a file. This file is the configuration file for batch processing in the next step.

1. Choose a tiff file to open.
2. You need to tell the GUI which lasers were on in the frames for which you would like
   to run spot detection.
3. Next, you need to set what offset method you would like to use in the sidebar.
   The offset method determines where the edges of the DNA are. You
   can choose to set the offset manually, to use the metadata, or to run a 
   simple bead detection algorithm. If the bead detection does not work, you can try
   to change the initial fit parameters in `config.yaml`.
4. On the main page you need to set some parameters for spot detection: the LoG
   detection threshold (lower values detects more spots) and the LoG radius (should
   be around the radius of the PSF).
5. Once you select 'run spot tracking', spot detection will be run on all frames and
   the spots will be connected frame-to-frame through the LAP method. You can set
   two tracking parameters, namely 'max frame skip' and 'max dist between 
   spots (pixels)'.
6. Once the tracking is done you will see the locations of the spots over time,
   as well as the corresponding bleaching traces and a table with tracked spots.
   This table can be saved to csv for further analysis.
7. If you are happy with the result you can save the offset & tracking parameters
   to a file named `params_offset_tracking.yaml`. If you place this file in a folder
   with tiffs, you can use the next navigation tab to run batch processing on all 
   these tiffs, with the offset and detection/tracking settings contained in this 
   yaml file.

## Process tiff directory (batch processing)

On this page you can open a directory for batch tiff processing. This directory
needs to contain not only a list of tiffs, but also a file named 
`params_offset_tracking.yaml` that contains offset, detection & tracking parameters.
You can make a file like this in the 'process tiff file' tab, or copy an existing 
parameter file if you would like to reuse the same parameters. Once the directory
has been processed the page will show all location traces. The corresponding
table can be saved (*scans.csv*) to generate figures using the final tab.

1. Open the directory you want to use for batch processing.
2. You can choose to remove any detected spots that are not connected to other spots.
3. You need to set the DNA length in kbp as well as the time between frames in seconds.
4. The main page will show the detection/tracking settings, as well as all the files
   that were found in the chosen directory.
5. Press 'Run analysis' to run the analysis.
6. Check the resulting location traces and save the output table for further analysis.

If you save the resulting output table to csv, the table columns include:

- **file_name**: name of file on file system
- **color_str**: str, 'r' or 'g' or 'b'
- **scan_id**: identifier for scan
- **trace_id**: identifier for colocalized tracks of different colors
- **track_id**: a spot of a single color, tracked over time (unique after selecting a **scan_id** and **frame**)
- **frame**: integer frame number
- **x_pixel**: float, x-location of spot in pixels in absolute coordinates
- **x_micron**: float, distance to left bead edge in micron
- **x_kbp**: float, distance to left bead edge in kbp
- **intensity**: integer, sum of pixel values in detected spot
- **corrected_intensity**: intensity corrected for crosstalk

**Notes**

You can manually change the field `['detection']['radius (pixels)']`  
in `params_offset_tracking.yaml` to be a list with 3 elements, 
to set different radii for different colors.

You can also manually change the fields `['tracking']['max frame skip']` and 
`['tracking']['max dist between spots (pixels)']` to be
lists with 3 elements, rather than single numbers. This will set the values
for each color individually. You have to change *both* fields to lists if
you want to do this.

## Explore data table

Here you can open a *scans.csv* file and explore its contents file by file.

## Analyze data table

In this tab you can open the data table containing all scans (*scans.csv*),
filter the traces that it contains, and make different plots.

Initial filtering includes:
- Filtering by starting frame. It is recommended to only keep traces that start
  before frame 5. Any traces that start later are probably traces that were not 
  properly connected to an earlier trace. Including those late traces will
  cause inaccuracies in your total spot count.
- Filtering by location range (kbp). It is recommended to filter out about 0.5
  kbp on each side of your dna, to get rid of spots on beads.
- Filtering by total count. It is recommended to remove traces above count 10,
  these are large aggregates that will cause inaccuracies in further processing.
  
After that, filtering by stoichiometry can give you the subset of traces that
you wish to analyze. For example, if you want to look at blue spots with single
or double bleaching steps, you set RED and GREEN to 0, and you set BLUE to 1-2.

Finally, you can choose the plots you wish to show. The resulting plots can be
exported and customized using a notebook.

## Calibration

In the final tab we can calibrate step sizes and the brightfield-to-confocal shift.

1. Open the scans.csv file, usually calibration data with dCas9. In order for the
   shift calibration to work, this file needs to be generated using metadata for
   offset determination, with an initial guess for the shift value.
2. Input the fluorophore color, and the expected fluorophore location.
3. You need to make an initial guess for the minimum step size; 
   this initial guess will probably change when we take a good look at the data after
   a first run.
4. The 'margin around peak' sets a margin around the expected location; we select traces
   within those margins to calculate the brightfield-to-confocal shift. We usually
   use a margin of 1 kbp. You can increase the margin if your initial guess for the shift
   was very much off.
5. Set initial filtering parameters (same as analysis tab).

Now, you will see a step size histogram, as well as bleaching traces that you can visually 
inspect. After visual inspection of the bleaching traces you will have some idea weather
your guess for the minimum step size was reasonable: if the analysis overfits the steps, you will
need to decrease your guess, and if it underfits the steps you will need to increase it.
It is advisable to end up with a minimum step size that is smaller than the mean step size minus
two sigma.

After the step fitting analysis there is a section where the brightfield-confocal shift is
calculated. The detected shift is given in micron - this value needs to be
added to the shift that was initially used to generate the `scans.csv` file used
for calibration to end up with the final calibrated shift value.

If you notice that there might be cross-talk from a certain fluorophore, the amount of 
cross-talk can also be determined in this tab. If, for example, you expect green 
cross-talk from a red fluorophore, you can take a dataset of the red fluorophore 
(if necessary with an increased red laser intensity, such that the cross-talk is noticeable). 
Then, you can run the calibration tab on both the red and green channels. The ratio between 
the resulting step size averages will give you the crosstalk correction factor. This can be 
entered into `config.yaml` under `bleed\_through[i][j][k] = C`, where `i` is 
the color that is being corrected (green in this case), `j` is the laser color that is 
causing the crosstalk (red in this case) and `k` is the fluorophore that is leaking 
(red in this case); `C = mu_I_g / mu_I_r * 100` is the cross-talk percentage.

## DNA mechanics analysis

In this tab, we can obtain persistance length, contour length and stretch modulus as a result of fitting our Force-Distance data. 

1. Open the directory containing files `*_fdcurves.csv` and `*_fddata.csv`. Remember that force and distance columns must be fifth and third, respectively.
2. Select the `params_fd.yaml` file. This file include necessary parameters such as: 
   - `max_iters`: Maximun number of iterations during fitting.
   - `min_delta`: Stopping criteria to check the convergence. The multiple fit stops when the increment in persistance length is lower than this value. Units: nm.
3. Set initial guess for contour length, persistance length and stretch modulus and its boundaries. Experiment parameters such as temperature, distance at the start point and DNA length also need to be specify. Finally, you can set the limits in our plots to be displayed.

The fitted curve for each file is shown together with the observed data once the fitting is completed. The results are stored in two dataframes, the full dataframe contains the individual results of each file and the summary dataframe is an average of the individual results. The uncertainty in our final results comes from the standard error of the mean (sem). You can download both of them.

## Contour length increment analysis

Here you can run a contour length increment analysis. 

1. Open the directory containing files `*_fdcurves.csv` and `*_fddata.csv`. Remember that force and distance columns must be fifth and third, respectively.
2. Select the `params_fd.yaml` file. This file include necessary parameters such as: 
   - `force_max`, `force_min`: define the high/low force threshold.
   - `d_max`, `d_min`: define the high/low distance threshold.
   - `delta_Lc_max`, `delta_Lc_min`: define short and long contour length increment threshold.
   - `max_Lc`, `min_Lc`: filter out final contour lengths that are too short/long.
3. Set experiment and DNA parameters. The values of persistance length and stretch modulus are the DNA mechanics analysis results after fitting.

The analysis visualization consist of one figure per file showing the contour length increment analysis together with the observed data. The analysis summary results contain information such as: number of jumps, final contour length or average increment size. On the other hand, the full dataframe include the values of each jump. You can download both of them.