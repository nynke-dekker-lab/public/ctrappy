import sys
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtWidgets import QApplication, QFileDialog, QWidget


def select_dir(initialdir='', prompt='select directory'):
    """Use PyQt5 to select a directory."""
    app = QApplication(sys.argv)
    # Using this invisible widget makes sure the dialog appears on top.
    widget = QWidget(flags=Qt.WindowStaysOnTopHint)
    widget.setFixedSize(QSize(0, 0))
    widget.show()
    widget.setVisible(False)
    dir_path = QFileDialog.getExistingDirectory(parent=widget,
                                                directory=initialdir,
                                                caption=prompt)
    widget.close()
    app.quit()
    print(dir_path)


if __name__ == "__main__":
    select_dir(*sys.argv[1:])
