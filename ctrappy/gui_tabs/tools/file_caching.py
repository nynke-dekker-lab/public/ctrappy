import streamlit as st
import ctrappy.file
import ctrappy.scan
import ctrappy.tracking
import ctrappy.gui_figures
import ctrappy.gui_tabs.tools.file_selection
import ctrappy.gui_tabs.tools.tiff_functions as tiff_functions
import os
import pandas as pd
import ctrappy.image


PATH_CACHE_SIZE = 2


def clear():
    """Clear cache, except for last_dir."""

    # Get current last_dir value.
    dir_cache = last_dir('')[0]

    # Clear caches.
    st.cache_data.clear()
    st.cache_resource.clear()

    # Put last_dir value back.
    if dir_cache != '':
        new_dir_cache = last_dir('')
        new_dir_cache[0] = dir_cache


@st.cache_resource
def last_dir(_initialdir=''):
    # Because we use st.cache_resource, the output is mutable.
    # We can change the first (and only) element in this cached list without streamlit complaining.
    return [_initialdir]


@st.cache_data
def gui_defaults():
    gui_dir = os.path.dirname(os.path.dirname(__file__))
    try:
        gui_settings = ctrappy.file.read_yaml(os.path.join(gui_dir, 'defaults', 'gui_custom.yml'), 
                                              schema='gui_defaults')
    except FileNotFoundError:
        gui_settings = ctrappy.file.read_yaml(os.path.join(gui_dir, 'defaults', 'gui_defaults.yml'), 
                                              schema='gui_defaults')
    return gui_settings


@st.cache_resource
def tab_cache():
    # Because we use st.cache_resource, the output is mutable.
    # We can change the first (and only) element in this cached list without streamlit complaining.
    return [None]


@st.cache_resource
def experiment_cache(csv_path, config, redo_step_fitting):
    df_scans = pd.read_csv(csv_path)
    return ctrappy.scan.Experiment(df_scans, config, df_scans['laser_colors'].iloc[0],
                                   redo_step_fitting=redo_step_fitting)


@st.cache_resource
def filter_experiment_cache(df_scans, config, filename, starting_frame,
                            kbp_locs, total_count, keep_coloc, redo_step_fitting):
    if filename is not None:
        df_scan = df_scans[df_scans['file_name'] == filename]
        experiment = ctrappy.scan.Experiment(df_scan, config, df_scan['laser_colors'].iloc[0],
                                             redo_step_fitting=redo_step_fitting)
    else:
        experiment = ctrappy.scan.Experiment(df_scans, config, df_scans['laser_colors'].iloc[0],
                                             redo_step_fitting=redo_step_fitting)
    experiment_filtered = ctrappy.scan.filter_experiment(experiment, starting_frame=starting_frame,
                                                         kbp_locs=kbp_locs, total_count=total_count,
                                                         keep_coloc=keep_coloc)
    if filename is not None:
        experiment_filtered.n_scan_files = 1

    if experiment_filtered:
        df_spots = experiment_filtered.to_dataframe()
        return df_spots, experiment_filtered
    else:
        return pd.DataFrame(), False


@st.cache_resource
def file_cache(path, filetype):
    """Read file from path; save in cache."""
    return read_file(path, filetype)


@st.cache_resource
def file_cache2(path, filetype):
    """Read file from path; save in cache (slot 2)."""
    return read_file(path, filetype)


@st.cache_resource
def file_cache3(path, filetype):
    """Read file from path; save in cache (slot 3)."""
    return read_file(path, filetype)


@st.cache_resource
def file_cache4(path, filetype):
    """Read file from path; save in cache (slot 4)."""
    return read_file(path, filetype)


@st.cache_resource
def path_cache():
    """Choose filename, save path in cache."""
    # Because we use st.cache_resource, the output is mutable.
    # We can change an element in this cached list without streamlit complaining.
    return [None for _ in range(PATH_CACHE_SIZE)]


def clear_cache_on_tab_change(tab_new):
    tab_old = tab_cache()
    if tab_new == tab_old[0]:
        return False
    else:
        clear()
        tab_old = tab_cache()
        tab_old[0] = tab_new
        return True


def read_file(path, filetype, schema=None):
    if filetype == 'h5':
        return ctrappy.file.read_h5file(path)
    elif filetype == 'yaml':
        return ctrappy.file.read_yaml(path, schema)
    elif filetype == 'tiff':
        return ctrappy.file.read_biotiff(path)
    elif filetype == 'csv':
        return pd.read_csv(path)
    else:
        st.warning(f'Filetype {filetype} not recognized.')


def select_file(filetypes='all files (*)', initialdir='', button_text='Select file', cache_id=0, remember_dir=True):

    # Get cached paths and last_dir.
    cached_paths = path_cache()
    dir_cache = last_dir(initialdir)

    # Set default dir to last_dir.
    if remember_dir:
        default_dir = dir_cache[0]
    else:
        default_dir = initialdir

    # Select file.
    if st.sidebar.button(button_text):
        cached_paths[cache_id] = ctrappy.gui_tabs.tools.file_selection.load_file(filetypes=filetypes,
                                                                                 initialdir=default_dir)

    # If canceled by user, set path to None.
    if cached_paths[cache_id] == '':
        cached_paths[cache_id] = None

    # Set last_path to selected directory.
    if remember_dir and cached_paths[cache_id] is not None:
        dir_cache[0] = os.path.dirname(cached_paths[cache_id])

    return cached_paths[cache_id]


def select_dir(initialdir='', button_text='Select directory', remember_dir=True):
    cached_path = path_cache()
    dir_cache = last_dir(initialdir)
    if remember_dir:
        default_dir = dir_cache[0]
    else:
        default_dir = initialdir
    if st.sidebar.button(button_text):
        cached_path[0] = ctrappy.gui_tabs.tools.file_selection.select_dir(default_dir)
    if cached_path[0] == '':
        cached_path[0] = None
    if remember_dir and cached_path[0] is not None:
        dir_cache[0] = cached_path[0]
    return cached_path[0]


@st.cache_data
def cache_frame_spots(data, thresholds, radius, offset_values, laser_colors, frame):
    return ctrappy.image.get_spots(data, thresholds, radius, offset_values=offset_values, frame=frame,
                                   colors=tiff_functions.get_color_inds(laser_colors))


@st.cache_data
def cache_all_spots(data, threshold, radius, offset_values, laser_colors):
    return ctrappy.image.get_spots(data, threshold, radius, offset_values=offset_values,
                                   colors=tiff_functions.get_color_inds(laser_colors))


@st.cache_data
def cache_tracks(spots, frame_skip, spot_dist):
    return ctrappy.image.track_spots(spots, frame_skip, spot_dist)
