import numpy as np
import os
import glob


def get_offset_from_metadata(metadata, config, brightfield_confocal_shift):
    dna_length_micron = metadata['DNA length (micron)']
    left_bead = metadata['bead0 center (micron)']
    bead_diameter = (metadata['bead1 center (micron)'] - metadata[
        'bead0 center (micron)']) - dna_length_micron
    left_bead_cf = (left_bead + brightfield_confocal_shift) / 0.05
    dna_start_cf = left_bead_cf + (bead_diameter / 2) / config['confocal_pixel_size']
    dna_end_cf = dna_start_cf + dna_length_micron / config['confocal_pixel_size']
    offset_values = (dna_start_cf, dna_end_cf)
    return offset_values


def project_intensity(image, offset_frame, offset_color):
    if offset_frame == 'first frame':
        intensity_proj = np.sum(image.data_raw[0, :, :, :], axis=0)
    elif offset_frame == 'final frame':
        intensity_proj = np.sum(image.data_raw[-1, :, :, :], axis=0)
    else:
        intensity_proj = np.sum(image.data_raw, axis=(0, 1)) / image.n_frames
    if offset_color == 'r':
        intensity_data = intensity_proj[:, 0]
    elif offset_color == 'g':
        intensity_data = intensity_proj[:, 1]
    elif offset_color == 'b':
        intensity_data = intensity_proj[:, 2]
    elif offset_color == 'rg':
        intensity_data = np.sum(intensity_proj[:, 0:2], axis=1) / 2
    elif offset_color == 'rb':
        intensity_data = np.sum(intensity_proj[:, 0::2], axis=1) / 2
    elif offset_color == 'gb':
        intensity_data = np.sum(intensity_proj[:, 1:], axis=1) / 2
    else:
        intensity_data = np.sum(intensity_proj[:, :], axis=1) / 3
    return intensity_data


def get_color_inds(laser_colors):
    """Get color indices from string.

    Parameters
    ----------
    laser_colors : str
        Laser colors ('r', 'g', 'b', 'rg', 'rb', 'gb', or 'rgb').

    Returns
    -------
    color_ids : array of ints
        List of laser color indices in experiment.
    """

    if laser_colors == 'r':
        color_ids = [0]
    elif laser_colors == 'g':
        color_ids = [1]
    elif laser_colors == 'b':
        color_ids = [2]
    elif laser_colors == 'rg':
        color_ids = [0, 1]
    elif laser_colors == 'rb':
        color_ids = [0, 2]
    elif laser_colors == 'gb':
        color_ids = [1, 2]
    else:
        color_ids = [0, 1, 2]
    return color_ids


def get_info_from_tiff_path(tiff_path):

    tiff_noext = os.path.splitext(tiff_path)[0]
    h5_name, scan_name = tiff_noext.split('_scan')
    return h5_name, int(scan_name)


def get_offset_file(original_path, offset_file):

    h5file, scan_nr = get_info_from_tiff_path(original_path)
    tiff_dir, _ = os.path.split(h5file)
    all_tiffs = glob.glob(os.path.join(tiff_dir, '*.tiff'))
    tiffs = dict()
    for tiff in all_tiffs:
        t_h5file, t_scan_nr = get_info_from_tiff_path(tiff)
        if t_h5file == h5file:
            tiffs[t_scan_nr] = tiff

    offset_index = 0
    indices = list(tiffs.keys())
    indices.sort()
    if offset_file == 'last scan in h5 file':
        offset_index = np.amax(list(tiffs.keys()))
    elif offset_file == 'first scan in h5 file':
        offset_index = np.amin(list(tiffs.keys()))

    return tiffs[offset_index]


def tracks_to_scan_df(tracks, file_path):
    if not tracks.empty:
        c_dict = {0: 'r', 1: 'g', 2: 'b'}
        df = tracks.copy()
        df['x_pixel'] = df['x']
        df['y_pixel'] = df['y']
        df['spot_id'] = df.index.values
        df['color_id'] = df['color']
        df['color_str'] = [c_dict[c] for c in df['color']]
        df['file_name'] = file_path
        df = df.drop(['x', 'y', 'color'], axis=1)
        return df
    else:
        return tracks
