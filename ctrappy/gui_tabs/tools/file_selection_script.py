import sys
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtWidgets import QApplication, QFileDialog, QWidget


def load_file(filetypes='', initialdir='', prompt='select file'):
    """Use PyQt5 to select file to load.

    Formatting examples for `filetypes` parameter:
    - "csv (*.csv *.CSV)"
    - "h5 (*.h5 *.H5 *.hdf5 *.HDF5)"
    - "yaml (*.yml *.YML *.yaml *.YAML)"
    - "tiff (*.tiff *.TIFF *.tif *.TIF)"
    """
    app = QApplication([])
    # Using this invisible widget makes sure the dialog appears on top.
    widget = QWidget(flags=Qt.WindowStaysOnTopHint)
    widget.setFixedSize(QSize(0, 0))
    widget.show()
    widget.setVisible(False)
    file_path = QFileDialog.getOpenFileName(parent=widget,
                                            directory=initialdir,
                                            caption=prompt,
                                            filter=filetypes)
    widget.close()
    app.quit()
    print(file_path[0])


if __name__ == "__main__":
    load_file(*sys.argv[1:])
