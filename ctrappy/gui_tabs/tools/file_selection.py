import os


def load_file(filetypes="all files|*", initialdir='', prompt='select file'):
    """Use external script to select a file."""
    run_file = os.path.join(os.path.dirname(__file__), "file_selection_script.py")
    file_path = os.popen(f'python "{run_file}" "{filetypes}" "{initialdir}" "{prompt}"').read()
    return file_path.strip()


def select_dir(initialdir='', prompt='select directory'):
    """Use external script to select a directory."""
    run_file = os.path.join(os.path.dirname(__file__), "dir_selection_script.py")
    file_path = os.popen(f'python "{run_file}" "{initialdir}" "{prompt}"').read()
    return file_path.strip()
