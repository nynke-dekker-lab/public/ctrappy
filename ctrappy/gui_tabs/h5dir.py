import streamlit as st
import ctrappy.file
import ctrappy.scan
import ctrappy.tracking
import ctrappy.gui_figures
import ctrappy.gui_tabs.tools.file_caching as cache
import ctrappy.gui_tabs.tools.file_selection
import os
import glob


def run():

    # Get GUI default parameters.
    gui_defaults = cache.gui_defaults()

    # Select directory.
    dir_path = cache.select_dir(initialdir=gui_defaults['initialdir_h5'],
                                button_text="Select directory")
    if dir_path is None:
        st.warning('No valid directory selected.')
        return
    st.sidebar.markdown(dir_path)

    st.markdown('# Batch h5 processing')

    # Read tiffs.
    st.markdown('## Input files')
    h5_paths = glob.glob(os.path.join(dir_path, '**', '*.h5'), recursive=True)
    st.write(h5_paths)

    # Output.
    if st.sidebar.button('Select directory and export'):
        st.markdown('## Output')
        tiff_path = ctrappy.gui_tabs.tools.file_selection.select_dir(gui_defaults['initialdir_tiff'])
        st.markdown(f'Directory: `{tiff_path}`')
        if tiff_path is not None:
            for file_path in h5_paths:
                try:
                    pylake_h5file, images = ctrappy.file.read_h5file(file_path)
                    st.markdown(f'reading `{file_path}`')
                    h5noext = os.path.splitext(file_path)[0]
                    h5dir, h5file = os.path.split(h5noext)
                    _, df = ctrappy.gui_figures.plot_fdcurves(pylake_h5file)
                    n_output = 0
                    if not df.empty:
                        fd_filename = h5file + '_fdcurves.csv'
                        df.to_csv(os.path.join(tiff_path, fd_filename))
                        st.markdown('- exported fdcurves')
                        n_output = n_output + 1
                    else:
                        st.markdown('- no fdcurves found')
                    for i, image in images.items():
                        tiff_filename = h5file + '_scan' + str(i) + '.tiff'
                        metadata_filename = h5file + '_scan' + str(i) + '.yaml'
                        scan_force_distance_filename = h5file + '_scan' + str(i) + '_FD.csv'
                        image.export(os.path.join(tiff_path, tiff_filename),
                                     os.path.join(tiff_path, metadata_filename),
                                     os.path.join(tiff_path, scan_force_distance_filename))
                        st.markdown('- exported scan ' + str(i))
                        n_output = n_output + 1
                    if n_output == 0:
                        _, df = ctrappy.gui_figures.plot_fdcurves(pylake_h5file, marker=False)
                        fd_filename = h5file + '_fddata.csv'
                        df.to_csv(os.path.join(tiff_path, fd_filename))
                        st.markdown('- no scans or fdcurves found, exported fd data')
                except:
                    st.warning('error exporting ' + file_path)
