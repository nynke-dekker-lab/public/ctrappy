import streamlit as st
import ctrappy.file
import ctrappy.scan
import ctrappy.gui_figures
import ctrappy.tracking
import pandas as pd
import ctrappy.gui_tabs.tools.file_caching as cache


def shorten_filename(long_filename):
    if len(long_filename) > 30:
        return "..." + long_filename[-28:]
    return long_filename


def run():

    # Get GUI default parameters.
    gui_defaults = cache.gui_defaults()

    # Select and open file.
    csv_path = cache.select_file(filetypes="scans csv file (*.csv *.CSV)",
                                 initialdir=gui_defaults['initialdir_tiff'],
                                 button_text="Select scans csv file",
                                 cache_id=0)
    if csv_path is None:
        st.warning('No valid csv file selected.')
        return
    st.sidebar.markdown(csv_path)
    df_scans = cache.read_file(csv_path, filetype='csv')
    dna_length_kbp = df_scans['dna_length_kbp'].iloc[0]

    # Select and open config file.
    config_path = cache.select_file(filetypes="config yaml file (*.yml *.YML *.yaml *.YAML)",
                                    initialdir=gui_defaults['initialdir_yaml'],
                                    button_text="Select config file",
                                    cache_id=1)
    if config_path is None:
        st.warning('No valid config file selected.')
        return
    st.sidebar.markdown(config_path)
    config = cache.file_cache2(config_path, filetype='yaml')

    # Write config info.
    st.write("# Config")
    st.write("Configuration parameters:")
    st.json(config, expanded=False)

    # Get experiment object.
    full_filenames = [file for file in df_scans['file_name'].unique()]
    filenames = [file.split('\\')[-1].split('/')[-1] for file in full_filenames]
    file_dict = {filename: full_filename for filename, full_filename in zip(filenames, full_filenames)}
    filename = file_dict[st.sidebar.selectbox(label='scan name', options=filenames, format_func=shorten_filename)]
    st.write(filename)

    # Set filtering.
    st.markdown('# Initial filtering')

    keep_coloc = st.checkbox('Remove tracks that are colocalized with filtered out tracks, ' 
                             'i.e., perform filtering on Trace level instead of Track level,'
                             'for the "location range" and "total count" filters.', value=True)
    redo_step_fitting = True  # st.checkbox('Redo step fitting with provided config file.')
    starting_frame = st.number_input(label='include tracks starting up to frame', min_value=0, max_value=1000,
                                     value=gui_defaults['starting_frame_default'], step=1)
    def_marg = float(gui_defaults['loc_range_kbp_margin_default'])
    kbp_locs = st.slider(label='include tracks within location range (kbp)', min_value=float(0.),
                         max_value=float(dna_length_kbp),
                         value=(def_marg, float(dna_length_kbp) - def_marg), format='%.3f', step=0.001)
    total_count = st.number_input(label='include tracks up to total count', min_value=0, max_value=1000,
                                  value=gui_defaults['total_count_default'], step=1)
    df_spots, experiment_filtered = cache.filter_experiment_cache(df_scans, config, filename, starting_frame,
                                                                  kbp_locs, total_count, keep_coloc, redo_step_fitting)

    if experiment_filtered:

        # Show plots.
        st.markdown('# Spot detections')
        image = ctrappy.file.read_biotiff(filename)
        offset_values = (df_spots['dna_start_pixel'].iloc[0], df_spots['dna_end_pixel'].iloc[0])
        radius = gui_defaults['log_radius_default']
        # User input.
        if image.n_frames > 1:
            frame = st.slider(label='frame', min_value=0, max_value=image.n_frames - 1, value=0)
        else:
            frame = 0
        display_max = st.slider(label='display max', min_value=1, max_value=20,
                                value=gui_defaults['display_max_default'])
        spots = pd.DataFrame()
        spots['frame'] = df_spots['frame']
        spots['color'] = df_spots['color_id']
        spots['x'] = df_spots['x_pixel']
        spots['y'] = df_spots['y_pixel']
        st.write(ctrappy.gui_figures.plot_scan_spots(image, frame, display_max, offset_values, spots, radius))

        data = ctrappy.gui_figures.get_location_traces(experiment_filtered, time_s=True, loc_kbp=True,
                                                       show_single_spots=False)
        fig = ctrappy.gui_figures.plot_location_traces(data, plot_id=True)
        st.write(fig)

        data = ctrappy.gui_figures.get_bleaching_traces(experiment_filtered, time_s=True)
        fig = ctrappy.gui_figures.plot_bleaching_traces(data)
        st.write(fig)

        data = ctrappy.gui_figures.get_stoichiometry(experiment_filtered)
        fig = ctrappy.gui_figures.plot_stoichiometry(data)
        st.write(fig)

    else:

        st.write('No spots found.')
