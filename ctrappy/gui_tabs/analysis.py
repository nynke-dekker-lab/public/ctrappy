import streamlit as st
import ctrappy.file
import ctrappy.scan
import ctrappy.tracking
import ctrappy.gui_figures
import ctrappy.gui_cpa_figs
import matplotlib.pyplot as plt
import pandas as pd
import ctrappy.gui_tabs.tools.file_caching as cache


def run():

    # Get GUI default parameters.
    gui_defaults = cache.gui_defaults()

    # Select and open file.
    csv_path = cache.select_file(filetypes="scans csv file (*.csv *.CSV)",
                                 initialdir=gui_defaults['initialdir_tiff'],
                                 button_text="Select scans csv file",
                                 cache_id=0)
    if csv_path is None:
        st.warning('No valid csv file selected.')
        return
    st.sidebar.markdown(csv_path)

    # Select and open config file.
    config_path = cache.select_file(filetypes="config yaml file (*.yml *.YML *.yaml *.YAML)",
                                    initialdir=gui_defaults['initialdir_yaml'],
                                    button_text="Select config file",
                                    cache_id=1)
    if config_path is None:
        st.warning('No valid config file selected.')
        return
    st.sidebar.markdown(config_path)
    config = cache.file_cache2(config_path, filetype='yaml')

    # Write config info.
    st.write("# Config")
    st.write("Configuration parameters:")
    st.json(config, expanded=False)

    # Filtering.
    st.markdown('# Initial filtering')
    rotate_scans = st.checkbox('Rotate scans with file names ending in "_R.tiff"; '
                               'keep only file names ending in "_L.tiff" or "_R.tiff".')
    keep_coloc = st.checkbox('Remove tracks that are colocalized with filtered out tracks, ' 
                             'i.e., perform filtering on Trace level instead of Track level,'
                             'for the "location range" and "total count" filters.', value=True)
    redo_step_fitting = True  # st.checkbox('Redo step fitting with provided config file. '
                              #             '(THIS MUST BE CHECKED IF YOU ARE DOING CROSSTALK CORRECTION!)', value=True)

    # Get experiment object.
    experiment = cache.experiment_cache(csv_path, config, redo_step_fitting=redo_step_fitting)

    # Filtering.
    starting_frame = st.number_input(label='include tracks starting up to frame', min_value=0, max_value=1000,
                                     value=gui_defaults['starting_frame_default'], step=1)
    def_marg = float(gui_defaults['loc_range_kbp_margin_default'])
    kbp_locs = st.slider(label='include tracks within location range (kbp)', min_value=float(0.),
                         max_value=float(experiment.dna_length_kbp),
                         value=(def_marg, float(experiment.dna_length_kbp) - def_marg), format='%.3f', step=0.001)
    total_count = st.number_input(label='include tracks up to total count', min_value=0, max_value=1000,
                                  value=gui_defaults['total_count_default'], step=1)

    if st.button('Generate overview table after initial filtering'):

        # Initial filtering.
        experiment_filtered = ctrappy.scan.filter_experiment(experiment, starting_frame=starting_frame,
                                                             kbp_locs=kbp_locs, total_count=total_count,
                                                             keep_coloc=keep_coloc)

        # Make overview table.
        df_summary = experiment_filtered.get_summary_table()
        st.dataframe(df_summary)
        link_csv = ctrappy.file.download_link(df_summary, 'scans_overview.csv',
                                              'save experiment overview table (after initial filtering)')
        st.markdown(link_csv, unsafe_allow_html=True)

    st.markdown('# Filter by stoichiometry')
    r_stoich = st.slider(label='include traces with RED counts in range', min_value=0, max_value=total_count,
                         value=(0, total_count), step=1)
    g_stoich = st.slider(label='include traces with GREEN counts in range', min_value=0, max_value=total_count,
                         value=(0, total_count), step=1)
    b_stoich = st.slider(label='include traces with BLUE counts in range', min_value=0, max_value=total_count,
                         value=(0, total_count), step=1)

    # Plotting.
    st.markdown('# Analysis')
    fig_options = ['anomalous diffusion', 'bleaching traces', 'diffusion (micron)',
                   'diffusion (kbp)', 'diffusion fit (micron)',
                   'diffusion fit (kbp)', 'lifetime (frame)', 'lifetime (s)', 'location hist (kbp)',
                   'location hist (micron)', 'location hist from center (kbp)', 'location traces (kbp)',
                   'location traces (micron)', 'location traces with splitting (kbp)', 'processivity (bp)',
                   'spots per DNA', 'stoichiometry', 'velocity (kbp/s)']
    to_plot = st.multiselect('figures to plot', fig_options, default=['location traces (kbp)', 'stoichiometry'])

    if st.button('generate figures'):

        # Rotate files.
        if rotate_scans:
            df_scans = pd.read_csv(csv_path[0])
            df_scans_R = df_scans.loc[df_scans['file_name'].str[-7:].str.contains(r"_R.tiff")].copy()
            df_scans_L = df_scans.loc[df_scans['file_name'].str[-7:].str.contains(r"_L.tiff")].copy()
            df_scans_R['x_pixel'] = df_scans_R['dna_end_pixel'] - df_scans_R['x_pixel'] + df_scans_R['dna_start_pixel']
            if 'x_kbp' in df_scans_R.columns.values:
                df_scans_R['x_kbp'] = df_scans_R['dna_length_kbp'] - df_scans_R['x_kbp']
            df_rotated = df_scans_L.append(df_scans_R)
            if df_rotated.empty:
                st.warning('Error: no file names containing "_L" or "_R" found.')
                return
            experiment = ctrappy.scan.Experiment(df_rotated, config, df_rotated['laser_colors'].iloc[0],
                                                 redo_step_fitting=redo_step_fitting)

        # Initial filtering.
        experiment_filtered = ctrappy.scan.filter_experiment(experiment, starting_frame=starting_frame,
                                                             kbp_locs=kbp_locs, total_count=total_count,
                                                             keep_coloc=keep_coloc)
        # TODO: filtered experiment can be cached

        # Stoichiometry filtering.
        experiment_filtered = experiment_filtered.filter_by_stoichiometry(r_stoich, g_stoich, b_stoich)

        if experiment_filtered.n_traces == 0:
            st.warning('Warning: no traces to analyze.')
            return

        output_data = dict()

        if 'bleaching traces' in to_plot:
            data = ctrappy.gui_figures.get_bleaching_traces(experiment_filtered, time_s=True)
            # st.write(data)
            fig = ctrappy.gui_figures.plot_bleaching_traces(data)
            output_data['bleaching traces'] = data
            st.write(fig)

        if 'diffusion (micron)' in to_plot:
            data = ctrappy.gui_figures.get_diffusion_hist(experiment_filtered, config)
            fig = ctrappy.gui_figures.plot_diffusion_hist(data)
            output_data['diffusion (micron)'] = data
            st.write(fig)

        if 'diffusion (kbp)' in to_plot:
            data = ctrappy.gui_figures.get_diffusion_hist(experiment_filtered, config, loc_kbp=True)
            fig = ctrappy.gui_figures.plot_diffusion_hist(data)
            output_data['diffusion (kbp)'] = data
            st.write(fig)

        if 'diffusion fit (micron)' in to_plot:
            data = ctrappy.gui_figures.get_diffusion_hist(experiment_filtered, config)
            fit_function, fit_params = ctrappy.gui_figures.fit_diffusion_hist(data)
            fig = ctrappy.gui_figures.plot_diffusion_hist(data, fit_function=fit_function, fit_params=fit_params)
            output_data['diffusion fit (micron)'] = data
            st.write(fig)
            st.write(f"Fit parameters ((mu, sig) or (mu1, sig1, a, mu2, sig2)): {fit_params}")

        if 'diffusion fit (kbp)' in to_plot:
            data = ctrappy.gui_figures.get_diffusion_hist(experiment_filtered, config, loc_kbp=True)
            fit_function, fit_params = ctrappy.gui_figures.fit_diffusion_hist(data)
            fig = ctrappy.gui_figures.plot_diffusion_hist(data, fit_function=fit_function, fit_params=fit_params)
            output_data['diffusion fit (kbp)'] = data
            st.write(fig)
            st.write(f"Fit parameters ((mu, sig) or (mu1, sig1, a, mu2, sig2)): {fit_params}")

        if 'lifetime (frame)' in to_plot:
            data = ctrappy.gui_figures.get_lifetime(experiment_filtered, time_s=False)
            fig = ctrappy.gui_figures.plot_lifetime(data)
            output_data['lifetime (frame)'] = data
            st.write(fig)

        if 'lifetime (s)' in to_plot:
            data = ctrappy.gui_figures.get_lifetime(experiment_filtered, time_s=True)
            fig = ctrappy.gui_figures.plot_lifetime(data)
            output_data['lifetime (s)'] = data
            st.write(fig)

        if 'location hist (kbp)' in to_plot:
            data = ctrappy.gui_figures.get_location_histogram(experiment_filtered, loc_kbp=True,
                                                              cf_res=config['confocal_pixel_size'])
            fig = ctrappy.gui_figures.plot_location_histogram(data)
            output_data['location hist (kbp)'] = data
            st.write(fig)

        if 'location hist (micron)' in to_plot:
            data = ctrappy.gui_figures.get_location_histogram(experiment_filtered, loc_kbp=False,
                                                              cf_res=config['confocal_pixel_size'])
            fig = ctrappy.gui_figures.plot_location_histogram(data)
            output_data['location hist (micron)'] = data
            st.write(fig)

        if 'location hist from center (kbp)' in to_plot:
            data = ctrappy.gui_figures.get_location_histogram(experiment_filtered, n_bins=12,
                                                              loc_kbp=True, from_center=True,
                                                              cf_res=config['confocal_pixel_size'])
            fig = ctrappy.gui_figures.plot_location_histogram(data)
            output_data['location hist from center (kbp)'] = data
            st.write(fig)

        if 'location traces (kbp)' in to_plot:
            data = ctrappy.gui_figures.get_location_traces(experiment_filtered, time_s=True, loc_kbp=True)
            fig = ctrappy.gui_figures.plot_location_traces(data)
            output_data['location traces (kbp)'] = data
            plt.tight_layout()
            st.write(fig)

        if 'location traces (micron)' in to_plot:
            data = ctrappy.gui_figures.get_location_traces(experiment_filtered, time_s=True, loc_kbp=False)
            fig = ctrappy.gui_figures.plot_location_traces(data)
            output_data['location traces (micron)'] = data
            plt.tight_layout()
            st.write(fig)

        if 'location traces with splitting (kbp)' in to_plot:
            experiment_filtered.get_splits(d=10)
            data = ctrappy.gui_figures.get_location_traces(experiment_filtered, time_s=True, loc_kbp=True)
            fig = ctrappy.gui_figures.plot_location_traces(data)
            output_data['location traces (kbp)'] = data
            plt.tight_layout()
            st.write(fig)

        if 'spots per DNA' in to_plot:
            data = ctrappy.gui_figures.get_spots_per_DNA(experiment_filtered)
            figs = ctrappy.gui_figures.plot_spots_per_DNA(data)
            output_data['spots per DNA'] = data
            st.write(figs[0])
            st.write(figs[1])
            st.write(figs[2])

        if 'processivity (bp)' in to_plot:
            data = ctrappy.gui_figures.get_processivity(experiment_filtered)
            fig = ctrappy.gui_figures.plot_processivity(data)
            output_data['processivity (bp)'] = data
            st.write(fig)

        if 'stoichiometry' in to_plot:
            data = ctrappy.gui_figures.get_stoichiometry(experiment_filtered)
            fig = ctrappy.gui_figures.plot_stoichiometry(data)
            output_data['stoichiometry'] = data
            st.write(fig)

        if 'velocity (kbp/s)' in to_plot:
            data = ctrappy.gui_cpa_figs.get_velocity(experiment_filtered)
            figs = ctrappy.gui_cpa_figs.plot_velocity(data)
            output_data['velocity (kbp/s)'] = data
            st.write(figs[0])
            st.write(figs[1])

        if 'anomalous diffusion' in to_plot:
            data = ctrappy.gui_cpa_figs.get_anom_diff_fit_log(experiment_filtered)
            fig = ctrappy.gui_cpa_figs.plot_anom_diff_fit_log(data)
            output_data['anomalous diffusion'] = data
            st.write(fig)

        link_yaml = ctrappy.file.download_link(output_data, 'figure_data.yml', 'save figure data')
        st.markdown(link_yaml, unsafe_allow_html=True)
