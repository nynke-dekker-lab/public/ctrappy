import streamlit as st
import ctrappy.file
import ctrappy.scan
import ctrappy.tracking
import ctrappy.gui_figures
import ctrappy.gui_tabs.tools.file_caching as cache
import ctrappy.gui_tabs.tools.file_selection
import os


def run():

    # Get GUI default parameters.
    gui_defaults = cache.gui_defaults()

    # Select and open file.
    file_path = cache.select_file(filetypes="lumicks h5 file (*.h5 *.H5 *.hdf5 *.HDF5)",
                                  initialdir=gui_defaults['initialdir_h5'],
                                  button_text="Select h5 file")
    if file_path is None:
        st.warning('No valid file selected.')
        return
    st.sidebar.markdown(file_path)

    # Make Image object from h5 file.
    h5file, images = cache.file_cache(file_path, filetype='h5')
    image_numbers = list(images.keys())
    image_numbers.sort()
    n_image = st.sidebar.selectbox(label='attributes / scan number', options=['force data'] + image_numbers)

    if n_image != 'force data':

        # Get scan.
        image = images[n_image]

        # Select max display value & frame.
        st.sidebar.markdown('-----')
        st.sidebar.markdown('### View')
        display_max = st.sidebar.slider(label='display max', min_value=1, max_value=20,
                                        value=gui_defaults['display_max_default'])
        if image.n_frames == 1:
            frame = 0
        else:
            frame = st.sidebar.slider(label='frame', min_value=0, max_value=image.n_frames - 1, value=0)

        # Show scan.
        st.markdown('# Scan ' + str(n_image))
        st.write(image.plot(frame, display_max))

        # TODO: user sets filename
        # if st.button('Export video'):
        #     image.export_video('output_video.avi', display_max)
        #     st.write('Video saved in the repo root.')

    elif n_image == 'force data':

        # Show force graph.
        st.markdown('# Force data')
        fig, df = ctrappy.gui_figures.plot_force_over_time(h5file)
        st.write(fig)
        if not df.empty:
            st.dataframe(df)
            link_csv = ctrappy.file.download_link(df, 'force_data.csv', 'save table to csv')
            st.markdown(link_csv, unsafe_allow_html=True)

        # Show FD curves.
        fig, df = ctrappy.gui_figures.plot_fdcurves(h5file)
        st.write(fig)
        if not df.empty:
            st.dataframe(df)
            link_csv = ctrappy.file.download_link(df, 'fdcurves.csv', 'save table to csv')
            st.markdown(link_csv, unsafe_allow_html=True)

    # Export images[scan_numbers].
    st.sidebar.markdown('-----')
    st.sidebar.markdown('### Export for further processing')
    scan_numbers = st.sidebar.multiselect('scans to export to tiff', image_numbers, default=image_numbers)
    if st.sidebar.button('select directory and export'):
        dir_path = ctrappy.gui_tabs.tools.file_selection.select_dir(gui_defaults['initialdir_tiff'])
        for i in scan_numbers:
            h5noext = os.path.splitext(file_path)[0]
            h5dir, h5file = os.path.split(h5noext)
            tiff_filename = h5file + '_scan' + str(i) + '.tiff'
            metadata_filename = h5file + '_scan' + str(i) + '.yaml'
            scan_force_distance_filename = h5file + '_scan' + str(i) + '_FD.csv'
            images[i].export(os.path.join(dir_path, tiff_filename),
                             os.path.join(dir_path, metadata_filename),
                             os.path.join(dir_path, scan_force_distance_filename))
