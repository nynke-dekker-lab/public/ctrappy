"""Do diffusion bootstrapping and fit. Code by Kaley McCluskey, 2020."""
import numpy as np
from scipy.stats import norm, rv_continuous


def gaussian(x, mu, sigma):
    return np.exp(-(x - mu) ** 2 / (2 * sigma ** 2)) / (sigma * np.sqrt(2 * np.pi))


def bigaussian(x, mu1, sig1, a1, mu2, sig2):
    return a1 * np.exp(-(x - mu1) ** 2 / (2 * sig1 ** 2)) / (sig1 * np.sqrt(2 * np.pi)) + \
           (1.0-a1) * np.exp(-(x - mu2) ** 2 / (2 * sig2 ** 2)) / (sig2 * np.sqrt(2 * np.pi))


class binormal_gen(rv_continuous):
    """Double Gaussian distribution"""

    def _pdf(self, x, m1, s1, a, m2, s2):
        g1 = np.exp(-0.5 * ((x - m1) / s1) ** 2) / np.sqrt(2.0 * np.pi * s1 ** 2)
        g2 = np.exp(-0.5 * ((x - m2) / s2) ** 2) / np.sqrt(2.0 * np.pi * s2 ** 2)
        return a * g1 + (1.0 - a) * g2

    def _argcheck(self, m1, s1, a, m2, s2):
        ma = m1 <= 0
        sa = s1 > 0
        ar = 1 >= a >= 0
        mb = m2 <= 0
        sb = s2 > 0
        return ma & sa & ar & mb & sb

    def _fitstart(self, data):
        return -7.0, 0.5, 0.5, -2.0, 0.5, 0, 1  # TODO: these could be config.yml inputs instead of hardcoded params


class binormal_gen_const(rv_continuous):
    """Double Gaussian distribution"""

    def __init__(self, name, m1, m2):
        self.m1 = m1
        self.m2 = m2
        super(binormal_gen_const, self).__init__(name)

    def _pdf(self, x, s1, a, s2):
        g1 = np.exp(-0.5 * ((x - self.m1) / s1) ** 2) / np.sqrt(2.0 * np.pi * s1 ** 2)
        g2 = np.exp(-0.5 * ((x - self.m2) / s2) ** 2) / np.sqrt(2.0 * np.pi * s2 ** 2)
        return a * g1 + (1.0 - a) * g2

    def _argcheck(self, s1, a, s2):
        ma = self.m1 <= 0
        sa = s1 > 0
        ar = 1 >= a >= 0
        mb = self.m2 <= 0
        sb = s2 > 0
        return ma & sa & ar & mb & sb

    def _fitstart(self, data):
        return 0.5, 0.5, 0.5, 0, 1


def get_bootstrap(n_samples, diff_coeffs_log, init_vals, random_seed=0):
    """Draw n_samples bootstrap samples of length N from the diff_coeffs distribution (length N) and fit them."""

    np.random.seed(random_seed)
    binormal = binormal_gen(name='binormal')

    boots = np.zeros((len(diff_coeffs_log), n_samples))
    par1 = np.array([]).reshape(0, 2)
    par2 = np.array([]).reshape(0, 5)  # np.zeros((n_samples, 5))
    counter = np.zeros(1)

    for i in range(n_samples):

        numel = len(diff_coeffs_log)
        x = np.random.randint(numel, size=numel)

        # store this list as a new column in an array
        bootsam = diff_coeffs_log[x]
        boots[:, i] = bootsam

        # fit this distribution to one normal distribution
        try:
            params1 = norm.fit(bootsam)
        except:
            print("Single peak fit failed.")
            params1 = (0, 0)  # mu, sigma

        # fit to two normal distributions
        try:
            params2 = binormal.fit(bootsam, init_vals[0], init_vals[1], init_vals[2], init_vals[3], init_vals[4], floc=0, fscale=1)
        except:
            print("2 peak fit failed.")
            params2 = (0, 0, 0, 0, 0, 0, 1)  # mu, sigma, a1, mu2, sigma2

        # if init_vals[0] > np.amax(diff_coeffs_log) or init_vals[0] < np.amin(diff_coeffs_log) or init_vals[3] > np.amax(
        #         diff_coeffs_log) or init_vals[3] < np.amin(diff_coeffs_log):
        #     # removes several of the worst incorrect 2-peak fits
        #     print("2 peak fit failed.")
        #     params2 = (0, 0, 0, 0, 0, 0, 1)

        # determine which is preferable using negative log likelihood
        logpdf1 = np.log10(gaussian(bootsam, *params1))
        logpdf2 = np.log10(bigaussian(bootsam, *params2[:5]))
        nll1 = -np.sum(logpdf1)  # 2 parameters
        BIC1 = 2*np.log(numel) + 2*nll1
        nll2 = -np.sum(logpdf2)  # 5 parameters
        BIC2 = 5*np.log(numel) + 2*nll2

        # print([nll1, BIC1, nll2, BIC2])

        if BIC1 <= BIC2:
            par1 = np.concatenate((par1, [params1]), axis=0)
            counter = counter + 1
        else:
            par2 = np.concatenate((par2, [params2[:5]]), axis=0)

        if counter >= n_samples/2:
            params = par1
        else:
            params = par2

    return boots, params


def fit_bootstrap(boots, params, lims, n_samples, n_bins=30):
    """For a set of bootstrap samples and their individual fits, determine the mean D coefficient distribution,  its
    95% confidence interval, and the average fit parameters. """
    from scipy.stats import t, sem

    hist = np.zeros((n_bins, n_samples))
    bin_edges = np.zeros((n_bins + 1, n_samples))
    ci = np.zeros((2, n_bins))

    # extract the fit parameters. 1 is the slow peak, and 2 is the fast peak
    means = np.mean(params, axis=0)
    ses = sem(params, axis=0)

    # take the average and 95% c.i. of the fit parameters
    m = len(means)  # 2 for a 1-peak fit, 5 for a 2-peak fit
    fit_params_log = np.zeros((m, 3))
    for i in range(m):
        mean = means[i]
        se = ses[i]
        lower, upper = t.interval(0.95, n_samples-1, loc=mean, scale=se)
        fit_params_log[i, :] = [mean, lower, upper]

    # normalized with areas area1,2 because of the definition of our gaussian function (which includes
    # coefficient a and normalization term)

    for k in range(n_samples):
        data = boots[:, k]

        # identically bin each data column
        hist[:, k], bin_edges[:, k] = np.histogram(data, bins=n_bins, range=lims, density=True)
        hist = np.array(hist)
        bin_edges = np.array(bin_edges)

        # bins = np.array([(bin_edges[q + 1, k] + bin_edges[q, k]) / 2 for q in range(n_bins)])

    binmeans = np.mean(hist, axis=1)  # each row is n_samples possible fillings of one bin
    binsems = sem(hist, axis=1)

    for p in range(n_bins):
        # determine the 95% confidence interval
        ci[0, p], ci[1, p] = np.array(t.interval(0.95, n_samples-1, loc=binmeans[p], scale=binsems[p]))
        ci[0, p] = np.absolute(binmeans[p] - ci[0, p])
        ci[1, p] = np.absolute(ci[1, p] - binmeans[p])
    return binmeans, ci, fit_params_log
