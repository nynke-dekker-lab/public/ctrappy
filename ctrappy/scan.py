"""
Main ctrappy functionality.

An Experiment contains multiple Scans. A Scan contains multiple Traces, consisting of colocalized Tracks.
This Experiment object can then be used as input for plotting functions in ctrappy.gui_figures
"""

import numpy as np
import ctrappy.stepfinder
import ctrappy.diffusion
import ctrappy.tracking
import copy
import pandas as pd


def rerun_colocalization(experiment):
    """Rerun colocalization on an Experiment, using ctrappy.tracking.Colocalizer, which takes a dataframe input."""
    config = experiment.config_calibration
    colocalizer = ctrappy.tracking.Colocalizer(config['colocalization_distance'],
                                               config['bleed_through'],
                                               cf_res=config['confocal_pixel_size'])
    df_exp = experiment.to_dataframe()
    df_exp_rerun_col = pd.DataFrame()
    for scan_id, df_scan in df_exp.groupby('scan_id'):
        df_scan_rerun_col = colocalizer.colocalize(df_scan, experiment.laser_colors)
        # df_exp_rerun_col = df_exp_rerun_col.append(df_scan_rerun_col)  # TODO: DataFrame.append() deprecated
        df_exp_rerun_col = pd.concat([df_exp_rerun_col, df_scan_rerun_col])
    if df_exp_rerun_col.empty:
        return False
    return ctrappy.scan.Experiment(df_exp_rerun_col, config, experiment.laser_colors,
                                   redo_step_fitting=experiment.redo_step_fitting)


def filter_experiment(experiment, starting_frame, kbp_locs, total_count, keep_coloc=False):
    """Filter experiment tracks (or traces) by starting frame, kbp location interval, and total count.

    Parameters
    ----------
    experiment : Experiment
        Input experiment object.
    starting_frame : int
        Any tracks starting from this frame onwards are filtered out.
    kbp_locs : 2-tuple of floats
        Any tracks outside this location interval (in kbps) are filtered out.
    total_count : int
        Any tracks above this count are filtered out.
    keep_coloc : bool
        If True, do the filtering on the Trace level, preserving colocalizations. Default: False.

    Returns
    -------
    experiment_filtered : Experiment
        Filtered experiment object.
    """
    experiment_filtered = experiment.filter_by_starting_frame(starting_frame)
    if keep_coloc:
        experiment_filtered = ctrappy.scan.rerun_colocalization(experiment_filtered)
    experiment_filtered = experiment_filtered.filter_by_loc_kbp(kbp_locs[0], kbp_locs[1], keep_coloc=keep_coloc)
    experiment_filtered = experiment_filtered.filter_by_total_count(total_count + 1, keep_coloc=keep_coloc)
    if not keep_coloc:
        experiment_filtered = ctrappy.scan.rerun_colocalization(experiment_filtered)
    return experiment_filtered


def get_step_calibration(config_calibration, laser_colors):
    """Convenience wrapper for set_laser_colors()."""
    min_step_sizes, avg_step_sizes, color_ids, color_strs = set_laser_colors(laser_colors, config_calibration)
    step_cal = {'min': min_step_sizes, 'avg': avg_step_sizes}
    return step_cal, color_ids, color_strs


def set_laser_colors(laser_colors, config):
    """Get appropriate parameters from config, depending on laser colors.

    Parameters
    ----------
    laser_colors : str
        Laser colors ('r', 'g', 'b', 'rg', 'rb', 'gb', or 'rgb').
    config : dict
        Config dictionary from yaml file.

    Returns
    -------
    min_step_sizes : array of floats
        List of minimum step sizes for stoichiometry with step counting for [r, g, b].
    avg_step_sizes : array of floats
        List of average step sizes for stoichiometry with step counting for [r, g, b].
    color_ids : array of ints
        List of laser color indices in experiment.
    color_strs : array of strs
        List of laser colors in experiment.
    """

    if laser_colors == 'r':
        min_step_sizes = config['min_step_sizes_r']
        avg_step_sizes = config['avg_step_sizes_r']
        color_ids = [0]
        color_strs = ['r']
    elif laser_colors == 'g':
        min_step_sizes = config['min_step_sizes_g']
        avg_step_sizes = config['avg_step_sizes_g']
        color_ids = [1]
        color_strs = ['g']
    elif laser_colors == 'b':
        min_step_sizes = config['min_step_sizes_b']
        avg_step_sizes = config['avg_step_sizes_b']
        color_ids = [2]
        color_strs = ['b']
    elif laser_colors == 'rg':
        min_step_sizes = config['min_step_sizes_rg']
        avg_step_sizes = config['avg_step_sizes_rg']
        color_ids = [0, 1]
        color_strs = ['r', 'g']
    elif laser_colors == 'rb':
        min_step_sizes = config['min_step_sizes_rb']
        avg_step_sizes = config['avg_step_sizes_rb']
        color_ids = [0, 2]
        color_strs = ['r', 'b']
    elif laser_colors == 'gb':
        min_step_sizes = config['min_step_sizes_gb']
        avg_step_sizes = config['avg_step_sizes_gb']
        color_ids = [1, 2]
        color_strs = ['g', 'b']
    else:
        min_step_sizes = config['min_step_sizes_rgb']
        avg_step_sizes = config['avg_step_sizes_rgb']
        color_ids = [0, 1, 2]
        color_strs = ['r', 'g', 'b']
    return min_step_sizes, avg_step_sizes, color_ids, color_strs


class Experiment:
    """An Experiment contains all data that comes from processing a tiff directory. It is initialized with a DataFrame,
    usually named 'scans.csv'.

    This input DataFrame (data) contains (at least) the columns: file_name, scan_id, trace_id, track_id
    color_id, color_str, frame, corrected_intensity, x_pixel, dna_start_pixel, dna_end_pixel, n_scans, n_frames. The
    config_calibration dictionary is read from a config.yaml file. Laser colors are 'r', 'g', 'b', 'rg', 'rb', 'gb' or
    'rgb', corresponding to which lasers were on during the experiment. The dna length in kbp is given by
    dna_length_kbp."""

    def __init__(self, data, config_calibration, laser_colors, dna_length_kbp=None, redo_step_fitting=True):

        # Prepare.
        self.data_full = data
        self.config_calibration = config_calibration
        self.laser_colors = laser_colors
        self.redo_step_fitting = redo_step_fitting
        self.scans = []
        self.n_scan_files = data['n_scans'].iloc[0]
        step_calibration, color_ids, color_strs = get_step_calibration(self.config_calibration, self.laser_colors)
        self.step_calibration = step_calibration
        self.color_ids = color_ids
        self.color_strs = color_strs
        self.data_full = self.data_full[self.data_full['color_id'].isin(self.color_ids)]

        # Fill scans.
        for i_scan, df_scan in self.data_full.groupby('scan_id'):
            if 'step_counting_divide' in self.config_calibration:
                step_divide = self.config_calibration['step_counting_divide']
            else:  # Fallback to old behavior.
                step_divide = True
            scan = Scan(df_scan, self.step_calibration, self.config_calibration['confocal_pixel_size'],
                        step_divide, redo_step_fitting)
            self.scans.append(scan)

        # Convert location data to kbp
        self.dna_length_kbp = dna_length_kbp
        if dna_length_kbp is not None:
            self.convert_to_kbp(dna_length_kbp)
        elif 'dna_length_kbp' in data.columns.values:
            self.dna_length_kbp = data['dna_length_kbp'].iloc[0]

        # Convert time data to seconds
        self.frame_time_s = None
        if 'time_s' in data.columns.values:
            df_frame1 = self.data_full[self.data_full['frame'] > 0]
            if not df_frame1.empty:
                self.frame_time_s = df_frame1['time_s'].iloc[0] / df_frame1['frame'].iloc[0]

        self.n_traces = 0
        self.n_tracks = 0
        self.count_traces()

    def count_traces(self):
        """Count total number of traces in Experiment."""
        self.n_traces = 0
        self.n_tracks = 0
        for scan in self.scans:
            for trace in scan.traces:
                if np.sum(trace.stoichiometry) >= 1:
                    self.n_traces = self.n_traces + 1
                    for track in trace.tracks:
                        if track.step_count > 0:
                            self.n_tracks = self.n_tracks + 1

    def get_splits(self, d):
        """For each scan, check if any traces split off of other traces, with a maximum distance of d.
        Detected splits are stored in trace.split_parent. It is important not to filter by starting frame
        before running this method."""
        for scan in self.scans:
            for trace in scan.traces:
                for track in trace.tracks:

                    # Get initial position x_start if t_start > 0
                    t_start = track.frame[0]
                    if t_start > 0:
                        trace_id = trace.trace_id
                        track_color = track.color_id
                        intensity = track.intensity[0]
                        x_start = track.x_pixel[0]
                        x_tracks = []
                        frame_inds = []
                        track_inds = []
                        pre_intensity_tracks = []
                        post_intensity_tracks = []
                        # Get other track positions for same colored tracks in all traces
                        # at frame t_start-1, put in x_tracks list
                        for trace2 in scan.traces:
                            for track2 in trace2.tracks:
                                if (trace2.trace_id != trace_id) \
                                        and (t_start - 1 in track2.frame) \
                                        and (t_start in track2.frame) \
                                        and (track2.color_id == track_color):
                                    ind = np.argwhere(track2.frame == t_start - 1)[0]
                                    frame_inds.append(ind)
                                    track_inds.append(trace2.trace_id)
                                    x_tracks.append(float(track2.x_pixel[ind]))
                                    pre_intensity_tracks.append(float(track2.intensity[ind]))
                                    post_intensity_tracks.append(float(track2.intensity[ind+1]))
                                # Single frame skip is allowed.
                                # TODO: make nicer
                                if (trace2.trace_id != trace_id) \
                                        and (t_start - 2 in track2.frame) \
                                        and (t_start in track2.frame) \
                                        and (track2.color_id == track_color):
                                    ind = np.argwhere(track2.frame == t_start - 2)[0]
                                    frame_inds.append(ind)
                                    track_inds.append(trace2.trace_id)
                                    x_tracks.append(float(track2.x_pixel[ind]))
                                    pre_intensity_tracks.append(float(track2.intensity[ind]))
                                    post_intensity_tracks.append(float(track2.intensity[ind+1]))
                        x_tracks = np.array(x_tracks)

                        # Find closest x in x_tracks
                        x_dist = np.absolute(x_tracks - x_start)
                        if len(x_dist) > 0:
                            x_tracks_ind = int(np.argmin(x_dist))
                            weight = pre_intensity_tracks[x_tracks_ind] / (post_intensity_tracks[x_tracks_ind] + intensity)
                            if weight < 1:
                                weight = 1 / weight

                            # Calculate s: distance weighted by intensity ratio
                            s = x_dist[x_tracks_ind] * weight
                            # print(x_dist[x_tracks_ind], weight, s, d,
                            #       [scan.scan_id, trace.trace_id, track.track_id],
                            #       [scan.scan_id] + x_inds[x_tracks_ind])

                            # If s <= d, save other trace as trace.split_parent
                            if s <= d:
                                print(scan.filename)
                                trace.split_parent[track_color] = track_inds[x_tracks_ind]
                                trace.split_parent_frame_ind[track_color] = frame_inds[x_tracks_ind]

    def convert_to_kbp(self, dna_length_kbp):
        """Convert track locations to kbp; only input is the DNA length in kbp."""
        self.dna_length_kbp = dna_length_kbp
        for scan in self.scans:
            for trace in scan.traces:
                for track in trace.tracks:
                    track.x_kbp = track.x_micron * self.dna_length_kbp / scan.dna_length_micron

    def convert_to_kbp_autocorrelation(self, dna_length_kbp, loc_filter=None, color='r', n_bins=32, offsets=None):
        """Use autocorrelation to convert track locations to kbp.

        Parameters
        ----------
        dna_length_kbp : float
            Dna length in kilobasepairs
        loc_filter : 2-tuple of floats
            Keep spots within these kbp-location values, to perform autocorrelation.
        color : str
            'r', 'g' or 'b'.
        n_bins : int
            Number of bins for binning before calculating autocorrelation.
        offsets : list of floats or ints
            List of offsets to try (in pixels), relative to initial guess.

        Returns
        -------
        corr_val : list of floats
            Correlation values corresponding to offsets list.
        """

        # Prepare
        if offsets is None:
            offsets = [0]
        self.dna_length_kbp = dna_length_kbp
        corr_val = np.zeros(len(offsets))

        # Filter out edge signal.
        if loc_filter is None:
            loc_filter = [0, dna_length_kbp]
        self.convert_to_kbp(dna_length_kbp)
        experiment_filtered = self.filter_by_loc_kbp(loc_filter[0], loc_filter[1])

        for i, offset in enumerate(offsets):

            # Try offset.
            for scan in experiment_filtered.scans:
                for trace in scan.traces:
                    for track in trace.tracks:
                        track.x_micron = (track.x_pixel - track.dna_start_pixel + offset) * \
                                         self.config_calibration['confocal_pixel_size']
            experiment_filtered.convert_to_kbp(dna_length_kbp)

            # Calculate list of trace starting location values.
            locs = []
            for scan in experiment_filtered.scans:
                for trace in scan.traces:
                    for track in trace.tracks:
                        if track.color_str == color:
                            locs.append(track.get_starting_loc(x_micron=False))

            # Make histogram
            hist_range = (0, dna_length_kbp)
            hist, bin_edges = np.histogram(locs, bins=n_bins, range=hist_range)
            # print(hist)
            # print(np.power(hist[len(hist) // 2:], 2), np.power(hist[:len(hist) // 2][::-1], 2))
            corr_val[i] = np.dot(np.power(hist[len(hist) // 2:], 2), np.power(hist[:len(hist) // 2][::-1], 2))

        # Set offset to maximum corr_val.
        if np.amax(corr_val) > 0:
            max_ind = int(np.argmax(corr_val))
            for scan in self.scans:
                for trace in scan.traces:
                    for track in trace.tracks:
                        track.x_micron = (track.x_pixel - track.dna_start_pixel + offsets[max_ind]) * \
                                         self.config_calibration['confocal_pixel_size']
            self.convert_to_kbp(dna_length_kbp)
            self.data_full['dna_start_pixel'] = self.data_full['dna_start_pixel'] + offsets[max_ind]
            self.data_full['dna_end_pixel'] = self.data_full['dna_end_pixel'] + offsets[max_ind]

        return corr_val

    def set_t(self, frame_time_s):
        """Convert track frame values to time (s) values; only input is the frame time in seconds."""
        self.frame_time_s = frame_time_s
        for scan in self.scans:
            for trace in scan.traces:
                for track in trace.tracks:
                    track.time_s = track.frame * frame_time_s

    def to_dataframe(self):
        """Export Experiment to pandas DataFrame."""
        data_export = self.data_full.copy().reset_index(drop=True)
        data_export['include'] = False
        data_export = data_export.sort_values(['scan_id', 'frame'])
        data_export['dna_length_kbp'] = self.dna_length_kbp
        data_export['laser_colors'] = self.laser_colors
        data_export['time_s'] = np.nan
        data_export['x_kbp'] = np.nan
        data_export['x_micron'] = np.nan
        data_export['step_count'] = np.nan
        data_export['lifetime'] = np.nan

        for scan in self.scans:
            scan_id = scan.scan_id
            for trace in scan.traces:
                trace_id = trace.trace_id
                for track in trace.tracks:
                    track_id = track.track_id
                    data_inds = data_export[(data_export['scan_id'] == scan_id) &
                                            (data_export['trace_id'] == trace_id) &
                                            (data_export['track_id'] == track_id)].index.values
                    data_export.loc[data_inds, 'time_s'] = track.time_s
                    data_export.loc[data_inds, 'x_kbp'] = track.x_kbp
                    data_export.loc[data_inds, 'x_micron'] = track.x_micron
                    data_export.loc[data_inds, 'step_count'] = track.step_count
                    data_export.loc[data_inds, 'lifetime'] = track.lifetime
                    data_export.loc[data_inds, 'include'] = True

        data_export = data_export[data_export['include']]
        data_export = data_export.drop('include', axis=1)
        return data_export

    def get_summary_table(self):
        """Output a summary table of the Experiment."""

        df_full = self.to_dataframe()
        df_summary = pd.DataFrame()
        for scan_id, scan in df_full.groupby('scan_id'):
            for trace_id, trace in scan.groupby('trace_id'):
                trace_data = dict()
                trace_data['scan_id'] = scan_id
                trace_data['trace_id'] = trace_id
                trace_data['lifetime_frames'] = 0
                trace_data['steps_r'] = 0
                trace_data['steps_g'] = 0
                trace_data['steps_b'] = 0
                trace_data['file_name'] = trace['file_name'].iloc[0]
                trace_data['n_total_scans'] = df_full['n_scans'].iloc[0]
                starting_locations = []
                for track_id, track in trace.groupby('track_id'):
                    if track['color_str'].iloc[0] == 'r':
                        trace_data['steps_r'] = track['step_count'].iloc[0]
                    if track['color_str'].iloc[0] == 'g':
                        trace_data['steps_g'] = track['step_count'].iloc[0]
                    if track['color_str'].iloc[0] == 'b':
                        trace_data['steps_b'] = track['step_count'].iloc[0]
                    trace_data['lifetime_frames'] = max(trace_data['lifetime_frames'], track['lifetime'].iloc[0])
                    starting_locations.append(track[:5]['x_kbp'].mean())
                trace_data['starting_location_kbp'] = np.mean(starting_locations)
                df_summary = df_summary.append(trace_data, ignore_index=True)
        return df_summary

    # Filtering methods.
    def filter_by_starting_frame(self, frame):
        """Filter tracks by starting frame - return new Experiment object."""

        experiment_filtered = copy.deepcopy(self)
        n_tot = 0
        n_keep = 0
        for scan, scan_filt in zip(self.scans, experiment_filtered.scans):
            scan_filt.traces = []
            for trace in scan.traces:
                keep_tracks = []
                for track in trace.tracks:  # Find minimum frame in track.
                    n_tot = n_tot + 1
                    track_min_frame = np.amin(track.frame)
                    if track_min_frame < frame:
                        keep_tracks.append(copy.deepcopy(track))
                        n_keep = n_keep + 1
                if len(keep_tracks) > 0:  # Keep trace.
                    keep_trace = copy.deepcopy(trace)
                    keep_trace.tracks = keep_tracks
                    keep_trace.stoichiometry = [0, 0, 0]
                    for keep_track in keep_tracks:
                        keep_trace.stoichiometry[keep_track.color_id] = keep_track.step_count
                    scan_filt.traces.append(keep_trace)
        experiment_filtered.count_traces()
        print('frame filtering, total:', n_tot, 'kept:', n_keep)
        return experiment_filtered

    def filter_by_loc_kbp(self, x0, x1, keep_coloc=False):
        """Filter tracks by starting location (kbp), keep locations between x0 and x1 - return new Experiment object.
        keep_coloc=True does the filtering on Trace level."""

        n_tot = 0
        n_keep = 0
        experiment_filtered = copy.deepcopy(self)
        for scan, scan_filt in zip(self.scans, experiment_filtered.scans):
            scan_filt.traces = []
            for trace in scan.traces:
                keep_tracks = []
                for track in trace.tracks:
                    n_tot = n_tot + 1
                    if not keep_coloc:
                        start_loc = track.get_starting_loc(x_micron=False)
                    else:
                        start_loc = trace.get_starting_loc(x_micron=False)
                    if x0 < start_loc < x1:  # Keep track.
                        keep_tracks.append(copy.deepcopy(track))
                        n_keep = n_keep + 1
                if len(keep_tracks) > 0:  # Keep trace.
                    keep_trace = copy.deepcopy(trace)
                    keep_trace.tracks = keep_tracks
                    keep_trace.stoichiometry = [0, 0, 0]
                    for keep_track in keep_tracks:
                        keep_trace.stoichiometry[keep_track.color_id] = keep_track.step_count
                    scan_filt.traces.append(keep_trace)
        experiment_filtered.count_traces()
        print('loc filtering, total:', n_tot, 'kept:', n_keep)
        return experiment_filtered

    def filter_by_loc_interval_kbp(self, intervals, reverse=False, keep_coloc=False):
        """Filter tracks by their entire location interval (kbp), keep (or remove, by setting reverse=True)
        tracks that overlap with the input intervals [(x0, x1), ...] - return new Experiment object.
        keep_coloc=True does the filtering on Trace level."""

        n_tot = 0
        n_keep = 0
        experiment_filtered = copy.deepcopy(self)
        for scan, scan_filt in zip(self.scans, experiment_filtered.scans):
            scan_filt.traces = []
            for trace in scan.traces:
                keep_tracks = []
                for track in trace.tracks:
                    n_tot = n_tot + 1

                    # Get location interval of track/trace.
                    if not keep_coloc:
                        loc_interval = (np.amin(track.x_kbp), np.amax(track.x_kbp))
                    else:
                        loc_interval = trace.get_loc_interval(x_micron=False)

                    # Check for loverlap of track/trace with input intervals.
                    no_overlap = True
                    for x0, x1 in intervals:
                        if (loc_interval[0] <= x0 and loc_interval[1] <= x0) or \
                           (loc_interval[0] >= x1 and loc_interval[1] >= x1):
                            pass  # Still no overlap...
                        else:
                            no_overlap = False

                    # Decide whether or not to keep the track.
                    if not reverse:
                        if not no_overlap:  # Keep track.
                            keep_tracks.append(copy.deepcopy(track))
                            n_keep = n_keep + 1
                    else:
                        if no_overlap:  # Keep track.
                            keep_tracks.append(copy.deepcopy(track))
                            n_keep = n_keep + 1

                if len(keep_tracks) > 0:  # Keep trace.
                    keep_trace = copy.deepcopy(trace)
                    keep_trace.tracks = keep_tracks
                    keep_trace.stoichiometry = [0, 0, 0]
                    for keep_track in keep_tracks:
                        keep_trace.stoichiometry[keep_track.color_id] = keep_track.step_count
                    scan_filt.traces.append(keep_trace)
        experiment_filtered.count_traces()
        print('loc filtering, total:', n_tot, 'kept:', n_keep)
        return experiment_filtered

    def filter_by_total_count(self, count, keep_coloc=False):
        """Filter tracks by count, keep tracks under input - return new Experiment object.
        keep_coloc=True does the filtering on Trace level."""

        n_tot = 0
        n_keep = 0
        experiment_filtered = copy.deepcopy(self)
        for scan, scan_filt in zip(self.scans, experiment_filtered.scans):
            scan_filt.traces = []
            for trace in scan.traces:
                keep_tracks = []
                for track in trace.tracks:
                    n_tot = n_tot + 1
                    if not keep_coloc:
                        track_count = track.step_count
                    else:
                        track_count = np.amax(trace.stoichiometry)
                    if (0 < track_count) and (track_count < count):  # Keep track.
                        keep_tracks.append(copy.deepcopy(track))
                        n_keep = n_keep + 1
                if len(keep_tracks) > 0:  # Keep trace.
                    keep_trace = copy.deepcopy(trace)
                    keep_trace.tracks = keep_tracks
                    keep_trace.stoichiometry = [0, 0, 0]
                    for keep_track in keep_tracks:
                        keep_trace.stoichiometry[keep_track.color_id] = keep_track.step_count
                    scan_filt.traces.append(keep_trace)
        experiment_filtered.count_traces()
        print('count filtering, total:', n_tot, 'kept:', n_keep)
        return experiment_filtered

    def filter_by_stoichiometry(self, r_range, g_range, b_range):
        """Filter traces by stoichiometry, keep traces within color ranges - return new Experiment object.
        Input ranges are 2-tuples of integers."""

        experiment_filtered = copy.deepcopy(self)
        keep_trace_dict = dict()
        for scan, scan_filt in zip(self.scans, experiment_filtered.scans):
            scan_id = scan.scan_id
            keep_trace_dict[scan_id] = []
            scan_filt.traces = []
            for trace in scan.traces:
                trace_id = trace.trace_id
                if (r_range[0] <= trace.stoichiometry[0] <= r_range[1]) & \
                   (g_range[0] <= trace.stoichiometry[1] <= g_range[1]) & \
                   (b_range[0] <= trace.stoichiometry[2] <= b_range[1]):
                    keep_trace_dict[scan_id].append(trace_id)
                    scan_filt.traces.append(copy.deepcopy(trace))
        experiment_filtered.count_traces()
        return experiment_filtered

    def filter_by_color(self, color_str):
        """Filter entire Experiment, keep only data of 1 color given by color_str (string)."""
        df_experiment = self.to_dataframe()
        df_experiment = df_experiment[df_experiment['color_str'] == color_str]
        experiment_filtered = Experiment(df_experiment, self.config_calibration, self.laser_colors,
                                         self.dna_length_kbp, self.redo_step_fitting)
        experiment_filtered = rerun_colocalization(experiment_filtered)
        experiment_filtered.count_traces()
        return experiment_filtered


class Scan:
    """Contains spots, tracks and traces extracted from a single tiff file."""
    def __init__(self, data, step_calibration, confocal_pixel_size, step_counting_divide, redo_step_fitting=True):
        self.scan_id = data['scan_id'].iloc[0]
        self.filename = data['file_name'].iloc[0]
        self.traces = []
        self.n_frames = data['n_frames'].iloc[0]
        self.dna_start_pixel = data['dna_start_pixel'].iloc[0]
        self.dna_end_pixel = data['dna_end_pixel'].iloc[0]
        self.dna_length_micron = (self.dna_end_pixel - self.dna_start_pixel) * confocal_pixel_size
        for i_trace, df_trace in data.groupby('trace_id'):
            trace = Trace(df_trace, step_calibration, confocal_pixel_size,
                          step_counting_divide, redo_step_fitting)
            self.traces.append(trace)

    def total_spot_count(self):
        """Get total spot (Trace) count in Scan."""
        n_spots = 0
        for trace in self.traces:
            if trace.total_count() > 0:
                n_spots = n_spots + 1
        return n_spots


class Trace:
    """Contains colocalized Tracks."""
    def __init__(self, data, step_calibration, confocal_pixel_size,
                 step_counting_divide, redo_step_fitting=True):
        self.trace_id = data['trace_id'].iloc[0]
        self.step_counting_divide = step_counting_divide
        self.tracks = []
        self.stoichiometry = [0, 0, 0]
        self.split_parent = [None, None, None]
        self.split_parent_frame_ind = [None, None, None]
        for i_track, df_track in data.groupby('track_id'):
            track = Track(df_track, confocal_pixel_size)
            if redo_step_fitting:
                track.count_steps(step_calibration, divide_by_avg=self.step_counting_divide)
            else:
                track.set_steps_from_data()
            self.tracks.append(track)
            self.stoichiometry[track.color_id] = track.step_count

    def get_starting_loc(self, x_micron=True):
        """Get average starting location of tracks within trace.
        Default units: micron; witch to kbp with x_micron=False."""
        loc = []
        for track in self.tracks:
            loc.append(track.get_starting_loc(x_micron))
        loc = np.array(loc)
        loc = loc[~np.isnan(loc)]
        if len(loc) > 0:
            return np.mean(np.array(loc))
        else:
            return np.nan

    def get_loc_interval(self, x_micron=True):
        """Get location interval of tracks within trace.
        Default units: micron; witch to kbp with x_micron=False."""
        loc_min = np.infty
        loc_max = -np.infty
        for track in self.tracks:
            if x_micron:
                loc_min = min(np.amin(track.x_micron), loc_min)
                loc_max = max(np.amax(track.x_micron), loc_max)
            else:
                loc_min = min(np.amin(track.x_kbp), loc_min)
                loc_max = max(np.amax(track.x_kbp), loc_max)
        return loc_min, loc_max

    def run_calibration_step_count(self, step_calibration):
        """Run the step counter on all Tracks in the Trace, using a step_calibration dictionary containing
        min_step_sizes and avg_step_sizes."""
        self.stoichiometry = [0, 0, 0]
        for track in self.tracks:
            track.count_steps(step_calibration, divide_by_avg=self.step_counting_divide)
            self.stoichiometry[track.color_id] = track.step_count

    def total_count(self):
        """Get total fluorophore step count of the Trace."""
        return np.sum(self.stoichiometry)


class Track:
    """Contains single color spots that have been tracked throughout different frames."""
    def __init__(self, data, confocal_pixel_size):
        data_sorted = data.sort_values('frame')
        self.data = data_sorted
        self.track_id = int(data_sorted['track_id'].iloc[0])
        self.color_id = int(data_sorted['color_id'].iloc[0])
        self.color_str = str(data_sorted['color_str'].iloc[0])
        self.frame = np.array(data_sorted['frame'])
        self.intensity = np.array(data_sorted['corrected_intensity'])
        self.x_pixel = np.array(data_sorted['x_pixel'])
        self.dna_start_pixel = np.array(data_sorted['dna_start_pixel'])
        self.x_micron = (self.x_pixel - self.dna_start_pixel) * confocal_pixel_size
        if 'time_s' in data_sorted.columns.values:
            self.time_s = np.array(data_sorted['time_s'])
        else:
            self.time_s = None
        if 'x_kbp' in data_sorted.columns.values:
            self.x_kbp = np.array(data_sorted['x_kbp'])
        else:
            self.x_kbp = None
        self.step_count = None
        self.lifetime = None  # Total lifetime in frames.
        self.step_sizes = []
        self.step_frames = []
        self.step_fit = None
        self.lifetime_final_step = None  # Lifetime of the final step in frames.

    def count_steps(self, step_calibration, divide_by_avg=True):
        """Count steps using ctrappy.stepfinder, using a step_calibration dictionary. By default we
        divide by the average step size to process large steps correctly; for pure step counting, set
        divide_by_avg to False."""

        # Prepare.
        min_step_size = step_calibration['min'][self.color_id]
        avg_step_size = step_calibration['avg'][self.color_id]
        xvals = list(self.frame)
        yvals = list(self.intensity)

        # Get peak count.
        if len(xvals) > 1:

            # Get intensity data and add some values on the end to get final step.
            xvals = xvals + [max(xvals) + 1 + i for i in range(10)]
            yvals = yvals + [0 for _ in range(10)]

            # Get steps.
            peaks, sizes, peak_sig = ctrappy.stepfinder.find_steps(xvals, yvals, min_step_size, 2)

            # Store; fix lifetime.
            peaks = np.array(xvals)[peaks]
            if divide_by_avg:
                n_steps = sum(np.array(np.clip(np.around(np.array(sizes) / avg_step_size), 1, None), dtype=int))
            else:
                n_steps = len(peaks)

            self.step_count = n_steps
            self.step_sizes = list(sizes)
            self.step_frames = list(peaks)
            self.step_fit = peak_sig
            if len(peaks) > 0:
                self.lifetime = peaks[-1] - self.frame[0]
            else:
                self.lifetime = 0
            if len(peaks) == 1:
                self.lifetime_final_step = peaks[-1] - self.frame[0]
            elif len(peaks) > 1:
                self.lifetime_final_step = peaks[-1] - peaks[-2]
            else:
                self.lifetime_final_step = 0

        else:
            self.step_count = 0
            self.step_sizes = []
            self.step_frames = []
            self.step_fit = np.zeros(len(self.frame) + 10)
            self.lifetime = 1
            self.lifetime_final_step = 1

    def set_steps_from_data(self):

        self.step_count = int(self.data['step_count'].iloc[0])
        self.lifetime = int(self.data['lifetime'].iloc[0])

        # TODO: what do we do about these?
        self.step_sizes = []
        self.step_frames = []
        self.step_fit = np.zeros(len(self.frame) + 10)
        self.lifetime_final_step = 1

    def get_starting_loc(self, x_micron=True):
        """Get starting location by averaging the first 5 frames. Units are microns by default; switch to kbp with
        x_micron=False."""

        min_len = np.inf

        if self.step_count > 0:
            if x_micron:
                loc = self.x_micron
            else:
                loc = self.x_kbp
            if len(self.frame) < min_len:
                min_len = len(self.frame)
            return np.mean(loc[:min((5, min_len))])

        else:
            return np.nan
