"""
Functions for calculating and plotting useful quantities from Experiments using Change Point Analysis (CPA).
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import ctrappy.diffusion
from scipy.optimize import curve_fit
import matplotlib
import ruptures
from pykalman import KalmanFilter
font = {'size': 16}
matplotlib.rc('font', **font)


# Diffusion function to fit.
def fitfunc_log(tau, gen_diff, anom_exp):
    return np.log(gen_diff) + anom_exp * np.log(tau)


def get_anom_diff_fit_log(experiment, sigma_error=0.066, threshold=0.0025, min_segment_size=3,
                          penalty=0.3, jump=1):
    """Get anomalous diffusion fit using log transformation method from https://doi.org/10.1371/journal.pone.0117722."""
    v_data = get_velocity(experiment, penalty, min_segment_size, fit_type='piecewise_linear', jump=jump)

    anom_diff_list = []
    anom_exp_list = []

    for scan in experiment.scans:
        scan_id = scan.scan_id
        for trace in scan.traces:
            trace_id = trace.trace_id
            for track in trace.tracks:
                track_id = track.track_id
                unique_id = str("(" + str(scan_id) + ", " + str(trace_id) + ", " + str(track_id) + ")")
                if track.step_count > 0 and len(track.x_kbp) > 5:
                    if np.amax(np.absolute(v_data['traces'][unique_id]['v_fit'])) > threshold:
                        frame_time = np.amin(np.diff(track.time_s))
                        t_array = np.array([frame_time * i for i in range(1, len(track.x_kbp + 1))])
                        msd = ctrappy.diffusion.get_msd(track.x_kbp) - 2 * sigma_error ** 2
                        t_array = t_array[msd > 0]
                        msd = msd[msd > 0]
                        if len(msd) > 5:
                            msd_log = np.log(msd)
                            best_fit = max(5, int(len(msd) * 0.33))
                            best_fit = min(best_fit, 50)
                            popt, pcov = curve_fit(fitfunc_log, t_array[:best_fit], msd_log[:best_fit],
                                                   bounds=([0, 0], [np.inf, 2]), p0=(0.001, 1))
                            anom_diff_list.append(popt[0])
                            anom_exp_list.append(popt[1])

    data = dict()
    data['anomalous_diffusion_coefficient'] = [float(d) for d in anom_diff_list]
    data['anomalous_diffusion_exponent'] = [float(a) for a in anom_exp_list]
    return data


def plot_anom_diff_fit_log(data):
    """Plot anomalous diffusion data."""

    # fig = plt.figure(figsize=(10, 6))
    # plt.hist(data['anomalous_diffusion_coefficient'])
    # plt.title('anomalous diffusion coefficient')
    # plt.xlabel('anomalous diffusion coefficient')
    # plt.ylabel('frequency')
    # plt.close()

    bin_width = 0.25
    bins = np.arange(0, 2 + bin_width, bin_width)
    hist, bin_edges = np.histogram(data['anomalous_diffusion_exponent'], bins=bins)
    fig = plt.figure(figsize=(10, 6))
    plt.bar(bins[:-1] + bin_width / 2, hist / len(data['anomalous_diffusion_exponent']), width=bin_width)
    plt.title('anomalous diffusion exponent')
    plt.xlabel('anomalous diffusion exponent')
    plt.ylabel('relative frequency')
    plt.close()

    return fig



# TODO: move the analysis part to a new file velocity.py
# TODO: move function inputs into config.yml
def get_velocity(experiment, penalty=0.5, min_segment_size=3, fit_type='piecewise_linear', jump=1):
    """Get trace locations over time; do Kalman filtering and change-point analysis with given values for the
    penalty and minimum segment size. References:
    - Kalman filter: https://pykalman.github.io/
    - Changepoint detection: https://centre-borelli.github.io/ruptures-docs/user-guide/detection/kernelcpd/
    - See also: http://www.laurentoudre.fr/publis/TOG-SP-19.pdf / https://arxiv.org/pdf/1801.00826.pdf
    """

    data = dict()
    data['velocities'] = []
    data['traces'] = dict()

    for scan in experiment.scans:
        scan_id = scan.scan_id
        for trace in scan.traces:
            trace_id = trace.trace_id
            for track in trace.tracks:
                track_id = track.track_id
                unique_id = str("(" + str(scan_id) + ", " + str(trace_id) + ", " + str(track_id) + ")")
                if track.step_count > 0 and len(track.x_kbp) > 5:

                    # Kalman filter.
                    measurements = [[x] for x in track.x_kbp]
                    kf = KalmanFilter(initial_state_mean=track.x_kbp[0], n_dim_obs=1)
                    p = kf.em(measurements).smooth(measurements)[0]
                    x_kal = p.flatten()

                    # Changepoint detection.
                    if fit_type == 'piecewise_linear':
                        est = ruptures.Pelt(custom_cost=ruptures.costs.costlinear.CostLinear,
                                            min_size=min_segment_size, jump=jump)
                    elif fit_type == 'piecewise_constant':
                        est = ruptures.KernelCPD(kernel='linear', min_size=min_segment_size, jump=jump)
                    else:
                        raise ValueError(f"Unknown fit_type provided: {fit_type}")
                    signal = np.array(x_kal).reshape((len(x_kal), 1))
                    pred = [0] + est.fit_predict(signal, pen=penalty)

                    # Linear fits between changepoints.
                    t = []
                    x_fit = []
                    v_fit = []
                    for i in range(len(pred) - 1):
                        t_data = np.array(track.time_s)[pred[i]:pred[i + 1]]
                        x_data = np.array(x_kal)[pred[i]:pred[i + 1]]
                        p1, p0 = np.polyfit(t_data, x_data, deg=1)
                        x_pfit = p0 + p1 * t_data
                        for j in range(len(t_data)):
                            t.append(float(t_data[j]))
                            x_fit.append(float(x_pfit[j]))
                            v_fit.append(float(p1))
                            data['velocities'].append(float(p1))

                    # Store data.
                    data['traces'][unique_id] = dict()
                    data['traces'][unique_id]['scan_id'] = int(scan_id)
                    data['traces'][unique_id]['trace_id'] = int(trace_id)
                    data['traces'][unique_id]['track_id'] = int(track_id)
                    data['traces'][unique_id]['color'] = str(track.color_str)
                    data['traces'][unique_id]['step_count'] = int(track.step_count)
                    data['traces'][unique_id]['changepoints'] = list([int(p) for p in pred])
                    data['traces'][unique_id]['t'] = list(t)
                    data['traces'][unique_id]['x'] = list([float(x) for x in track.x_kbp])
                    data['traces'][unique_id]['x_kalman'] = list([float(x) for x in x_kal])
                    data['traces'][unique_id]['x_fit'] = list(x_fit)
                    data['traces'][unique_id]['v_fit'] = list(v_fit)

    return data


def plot_velocity(data, vel_bin_width_bp=0.5):
    """Plot the result of get_velocity(); use bin width vel_bin_width_bp for the velocity histogram."""

    # Trace fits.
    fig0 = plt.figure(figsize=(10, 20))
    for key, track in data['traces'].items():
        plt.scatter(track['t'], track['x'], c=track['color'])
        plt.plot(track['t'], track['x_kalman'], c=track['color'])
        for i in range(len(track['changepoints']) - 1):
            start = track['changepoints'][i]
            stop = track['changepoints'][i + 1]
            plt.plot(track['t'][start:stop], track['x_fit'][start:stop], c='k')
            if i < len(track['changepoints']) - 2:
                x = (np.array(track['t'])[stop] + np.array(track['t'])[stop - 1]) / 2
                y = np.array(track['x_kalman'])[stop]
                plt.plot([x, x], [y - 0.4, y + 0.4], c='k', linestyle='dotted')
    plt.xlabel('t (s)')
    plt.ylabel('x (kbp)')
    plt.title('Linear trace fitting with Kalman filter + changepoint analysis')
    plt.close()

    # Velocity histogram.
    max_velocity = 1000 * np.amax(np.absolute(data['velocities']))
    n_bins = int(max_velocity / vel_bin_width_bp)
    bins = np.array([vel_bin_width_bp * i for i in range(n_bins)])
    hist, bin_edges = np.histogram(1000 * np.absolute(data['velocities']), bins=bins)
    fig1 = plt.figure(figsize=(10, 6))
    plt.bar(bins[:-1] + vel_bin_width_bp / 2, hist / len(data['velocities']), width=vel_bin_width_bp)
    plt.xlabel('velocity (bp/s)')
    plt.ylabel('relative frequency')
    plt.title('Velocity histogram')
    plt.close()

    return fig0, fig1
