# ------------------------------------------------------------------------#
# This Code is based on Matlab code developed by Sam Leachman, 10/2016.   #
# And the force extension models from Bouchiat et al., 1999.              #
# And was adapted by Vincent Kruit, 05/2021                               #
# ------------------------------------------------------------------------#
"""
This code is not yet integrated into the GUI. We could add a 'FD curve analysis' tab
and use the WLC fit there.
"""

import numpy as np
import scipy.optimize
import matplotlib.pyplot as plt
from scipy.stats import norm
import csv
import os
import operator


def read(csv_file, directory):
    """ This function reads a formatted csv_file and extracts the extension and force data.
    Input parameters;
    csv_file: name of csv file containing numbering in the first column,
        extension data (um) in the second column
        and force extension data (pN) in the third column.
    directory: directory leading to the folder containing the csv_file.
    Output;
    extension_data: numpy array containing the extension data (nm).
    force_data: numpy array containing the force data (pN).
    """

    path = os.path.join(directory, csv_file)
    with open(path, 'r') as f:
        csv_reader = csv.DictReader(f)

        # initialise empty np arrays for storage of extension and force data
        extension_data = np.array([])
        force_data = np.array([])

        for row in csv_reader:
            try:
                # get extension data in microns
                extension_data = np.append(extension_data, float(row.get(csv_reader.fieldnames[1])))
                # get force data in pN
                force_data = np.append(force_data, float(row.get(csv_reader.fieldnames[2])))
            except ValueError:
                break
        f.close()
    return extension_data * 1E3, force_data


def force_to_extended_worm_like_chain(force_data, KbT, Lp, Lo, S):
    """
    This function calculates the theoretical extension (nm) calc_extension,
    corresponding to a force and the given values for
    Boltzmann's constant times the temperature (pNnm) KbT, the persistence length (nm) Lp,
    the contour length (nm) Lo and the stretch modulus (pN) S.
    Input parameters;
    force_data: np array containing force data (pN).
    KbT: Boltzmann's constant times temperature (pNnm)
    Lp: persistence length (nm)
    Lo: contour length (nm)
    S: stretch modulus (pN)
    returns;
    calc_extension: calculated extension data corresponding (nm)
    """
    a2 = -0.5164228
    a3 = -2.737418
    a4 = 16.07497
    a5 = -38.87607
    a6 = 39.49944
    a7 = -14.17718
    A9 = 4 * a7
    A8 = 4 * a6 - 8 * a7
    A7 = 4 * a5 - 8 * a6 + 4 * a7
    A6 = 4 * a4 - 8 * a5 + 4 * a6
    A5 = 4 * a3 - 8 * a4 + 4 * a5
    A4 = 4 * a2 - 8 * a3 + 4 * a4
    A3 = -8 * a2 + 4 * a3 + 4
    calc_extension = np.array([])  # initialise to empty array
    for force in force_data:
        A2 = 4 * a2 - 9 - 4 * force * Lp / KbT
        A1 = 6 + 2 * 4 * force * Lp / KbT
        A0 = -4 * force * Lp / KbT
        coefficients = [A9, A8, A7, A6, A5, A4, A3, A2, A1, A0]
        r = np.roots(coefficients).real  # find roots and discard the imaginary part
        # append calculated extension value to the array
        calc_extension = np.append(calc_extension, (r[-1] + force / S) * Lo)
    return calc_extension


def sum_squares(LoLpS, extension_data, force_data, KbT):
    """
    This function takes a tuple containing values for Lo, Lp and S together with the experimental data and KbT
    and calculates the sum of the magnitude of the differences between
    the experimental extension and calculated extension data squared.
    Input parameters;
    LoLpS: tuple containing values for Lo (nm), Lp (nm) and S (pN) respectively.
    extension_data: np array containing the experimental extension data (nm).
    force_data: np array containing the experimental force data (pN).
    KbT: Boltzmann's constant time temperature (pNnm).
    Returns;
    ss: the sum of squared difference between the extension data and the calculated extension data.
    """
    # unpack values for Lp, Lo and S from tuple
    Lo, Lp, S = LoLpS

    # calculate theoretical extension according to these variable values.
    calc_extension = force_to_extended_worm_like_chain(force_data, KbT, Lp, Lo, S)

    # calculate the sum of squares we want this to be minimized for Lo, Lp and S.
    ss = np.sum(np.power(extension_data - calc_extension, 2))
    return ss

# ------------- Set parameters in this section -------------------------------------------------- #

# Parameters which you should change according to your experiment are:
# temperature (T) in kelvin, the number of base pairs (num_bp),
# the directory containing the .csv files (directory).


# Specify directory by pasting the absolute path like this r'path' just copy paste the address that's all
# directory = r'W:\staff-groups\tnw\bn\nd\Shared\Daniel\A-Wet lab notebook\2021\Vincent Kruit\VK_003 C-Trap characterization of first batch of constructs made from pDRM2\Analysis\Processed H5 files without discarded data'
# directory = r'C:/Users/Fiona/Documents/MEP/QTRAP_data/210520/Chrom150'
directory = r'C:\Users\Fiona\Documents\MEP\MEP working folder\Elena_data\only_nucleosome_DNA_r'

# define min number of data points
min_num_data = 20

# Define limits of data used for modelling
extension_lower_bound = 3500  # nm
extension_upper_bound = 9000  # nm
force_lower_bound = 2  # pN
force_upper_bound = 50  # pN

# calculate KbT in pNnm
kb = 1.38064852 * 1E-23  # m^2 kg s^-2 K^-1
T = 303.15  # K
KbT = kb * T * 1E21  # pNnm

# define bounds for variables which are optimised
Lp_lower_bound = 10  # nm
Lp_upper_bound = 70  # nm
Lo_lower_bound = 400  # nm
Lo_upper_bound = 7500  # nm
S_lower_bound = 200  # pN
S_upper_bound = 5000  # pN

# defining expected values
#num_bp = 10052  # bp
# num_bp = 21169
num_bp = 10467  # Elena?
exp_Lo = num_bp * 0.343  # nm
exp_Lp = 50  # nm
exp_S = 1000  # pN

# ------------- End of parameters section -------------------------------------------------- #

# defining expected values array and boundary values array in nm for Lo and Lp and pN for S
x0 = np.array([exp_Lo, exp_Lp, exp_S])  # initial guess
bounds = ((Lo_lower_bound, Lo_upper_bound), (Lp_lower_bound, Lp_upper_bound), (S_lower_bound, S_upper_bound))

# Initialise Lo vector and Lp vector
Lo_vec = np.array([])
Lp_vec = np.array([])
S_vec = np.array([])

# initialise empty arrays to store all data points
all_ext_data = np.array([])
all_force_data = np.array([])

# create empty list for storage of files containing outliers (i.e. boundary values for Lp, Lo or S)
outliers = list()

# create empty list for storage of unused files due to too few data points
too_few_data = list()

# loop through all .csv files in the folder specified by directory
for root, dir, files in os.walk(directory):
    for file in files:
        if file.endswith(".csv"):  # only the .csv files will be opened and read
            try:
                # reading data from csv file
                extension_data, force_data = read(file, directory)

                if len(extension_data) >= min_num_data:
                    # define which subset of data is used for modelling according to bounds specified earlier
                    indices = np.where((extension_data > extension_lower_bound) &
                                       (extension_data < extension_upper_bound) &
                                       (force_data > force_lower_bound) &
                                       (force_data < force_upper_bound))

                    # minimizing the sum_squares function for Lo and Lp
                    res = scipy.optimize.minimize(sum_squares,
                                                  x0=x0,
                                                  args=(extension_data[indices], force_data[indices], KbT),
                                                  bounds=bounds)
                    Lo, Lp, S = res.x  # these are the actual found values

                    # adding found Lo and Lp values to vectors
                    Lo_vec = np.append(Lo_vec, Lo)
                    Lp_vec = np.append(Lp_vec, Lp)
                    S_vec = np.append(S_vec, S)

                    # add data to total array of data
                    all_ext_data = np.append(all_ext_data, extension_data[indices])
                    all_force_data = np.append(all_force_data, force_data[indices])

                    # If statement to distinguish outliers from data falling within the expected bounds
                    if not (Lp_lower_bound < Lp < Lp_upper_bound and
                            Lo_lower_bound < Lo < Lo_upper_bound and
                            S_lower_bound < S < S_upper_bound):
                        print(f'{file}: Persistence length: {round(Lp, 2)} nm,'
                              f' contour length is: {round(Lo, 2)} nm'
                              f' and Stretch modulus is: {round(S, 2)}. Resulted in boundary values!')
                        outliers.append(file)
                    else:
                        print(f'{file}: Persistence length: {round(Lp, 2)} nm,'
                              f' contour length is: {round(Lo, 2)} nm'
                              f' and Stretch modulus is: {round(S, 2)}.')
                else:
                    too_few_data.append(file)
                    print(f'{file}: contains less than {min_num_data}'
                          f' data points and was therefore not used, please check the file')
            except RuntimeError:
                # in case something goes wrong print error message
                print(f'{file}: failed to do calculations')

# print message to show which data sets were not used in calculation of mean values
if len(outliers) != 0:
    print(f'\n\n Data from the following {len(outliers)} '
          f'files resulted in boundary values for Lp, Lo or S: {outliers}')

if len(too_few_data) != 0:
    print(f'\n\n Data from the following {len(too_few_data)} '
          f'files were not used due to too few data < {min_num_data} points: {too_few_data}')

# calculate mean values without taking boundary values into account
mean_Lo, std_Lo = norm.fit(Lo_vec)
mean_Lp, std_Lp = norm.fit(Lp_vec)
mean_S, std_S = norm.fit(S_vec)

# print mean values
print(f'\n mean and std values are; Lo: mean={mean_Lo} std={std_Lo}, Lp: mean={mean_Lp} std={std_Lp} and S: mean={mean_S} std={std_S}')

# creating curves using found values for Lp, Lo and S
Force_range = np.linspace(np.min(all_force_data), np.max(all_force_data), 1000)  # create force range in pN
# calculate extension based on force range using optimized Lp and Lo values
extensionFit = force_to_extended_worm_like_chain(Force_range, KbT, mean_Lp, mean_Lo, mean_S)
# find indices which match the experimental data to get a nice plot
fit_indices = np.where((np.min(all_ext_data) < extensionFit) &
                       (extensionFit < np.max(all_ext_data)) &
                       (np.min(all_force_data) < Force_range) &
                       (Force_range < np.max(all_force_data)))

# sort pair wise, for residual plot only
zipped_lists = zip(all_force_data, all_ext_data)  # makes tuples
sorted_zipped_lists = sorted(zipped_lists, key=operator.itemgetter(0))  # sorts the list of tuples based on force
unzipped_list = list(zip(*sorted_zipped_lists))  # unzip the sorted list of tuples
sorted_force_data = np.array(unzipped_list[0])  # get the sorted force data back
sorted_ext_data = np.array(unzipped_list[1])  # and the corresponding extension data

# calculate residuals in order of increasing extension
residuals = sorted_ext_data - force_to_extended_worm_like_chain(sorted_force_data, KbT, mean_Lp, mean_Lo, mean_S)
mean_res_val, std_res_val = norm.fit(residuals)
print(f'mean res. val. = {mean_res_val}, std={std_res_val}')  # fit a normal distribution to the data to get the mean and std

# plotting using the found Lo, Lp and S values:
plt.figure(1)
plt.scatter(all_ext_data/1000, all_force_data, label="Experimental data", s=2, alpha=.3)  # experimental data
# fit obtained using optimised values for Lp, Lo and S
plt.plot(extensionFit[fit_indices]/1000, Force_range[fit_indices], color='orange', label="Simulated curve")
plt.xlabel('Extension ' + u"(\u03bcm)")
plt.ylabel('Force (pN)')
#plt.title(f'mean Lp = {round(mean_Lp, 2)} nm, mean Lo = {round(mean_Lo, 2)} nm, mean S = {round(mean_S, 2)} pN')
#plt.legend()

# plotting the residuals (the mean value should go to zero because for a good model the residuals are caused by noise)
plt.figure(2)
plt.scatter(sorted_force_data, residuals, label='Residuals', s=2, alpha=.3)
plt.plot(sorted_force_data, [mean_res_val]*len(sorted_force_data), color='orange', label='Mean residual value')
plt.ylabel('Residuals (nm)')
plt.xlabel('Force (pN)')
#plt.title(f'Mean residual value = {mean_res_val}')
#plt.legend()

# log y axis
# plotting using the found Lo, Lp and S values:
plt.figure(3)
plt.scatter(all_ext_data/1000, all_force_data, label="Experimental data", s=2, alpha=.3)  # experimental data
# fit obtained using optimised values for Lp, Lo and S
#plt.plot(extensionFit[fit_indices]/1000, Force_range[fit_indices], color='orange', label="Simulated curve")
plt.semilogy(extensionFit[fit_indices]/1000, Force_range[fit_indices], color='orange')
#plt.yscale('log')
plt.xlabel('Extension ' + u"(\u03bcm)")
plt.ylabel('Force (pN)')
#plt.title(f'mean Lp = {round(mean_Lp, 2)} nm, mean Lo = {round(mean_Lo, 2)} nm, mean S = {round(mean_S, 2)} pN')
#plt.legend()
plt.show()  # show all plots

# f = mticker.ScalarFormatter(useOffset=False, useMathText=True)
# g = lambda x,pos : "${}$".format(f._formatSciNotation('%1.10e' % x))
# plt.gca().yaxis.set_major_formatter(mticker.FuncFormatter(g))
# import matplotlib.ticker as mticker