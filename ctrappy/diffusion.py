"""
Calculate the diffusion coefficient from a track.
"""

import numpy as np
from scipy.optimize import curve_fit


def fit_func(x, a, b):
    """Linear fitting function."""
    return a + b * x


def get_p_min(diff_estimate, t_exposure, avg_photon_count, sigma_psf, frame_time):
    """Calculate p_min from diffusion estimate, exposure time, average photon count, sigma of psf and frame time."""

    # Estimate optimal number of points to take into account for fit.
    sigma0 = sigma_psf / np.sqrt(avg_photon_count)
    sigma = sigma0 * np.sqrt(1 + diff_estimate * t_exposure / sigma_psf ** 2)
    error = sigma ** 2 / (diff_estimate * frame_time)
    p_min = int(np.floor(2 + 2.7 * error**0.5))
    return p_min, sigma


def get_msd(x_vals):
    """Get mean square displacement from an array of values."""

    # Calculate displacements.
    x_arr = np.array(x_vals)
    msd = np.zeros(len(x_arr))

    # Iterate over lag times.
    for d in range(1, len(x_arr)):
        # Iterate over starting points.
        n_starting_points = min(d, len(x_arr) - d)
        for i in range(n_starting_points):
            lag_points = x_arr[i::d]
            differences = np.diff(lag_points)
            diff_squared = np.power(differences, 2)
            msd[d] = msd[d] + np.average(diff_squared) / n_starting_points
        # # Don't iterate over starting points...
        # lag_points = x_vals[0::d]
        # differences = np.diff(lag_points)
        # diff_squared = np.power(differences, 2)
        # msd[d] = msd[d] + np.average(diff_squared)
        # # Iterate over starting points by hand.
        # diffsq = []
        # for i in range(len(x_arr) - d):
        #     diff = x_arr[i + d] - x_arr[i]
        #     diffsq.append(diff**2)
        # msd[d] = msd[d] + np.average(diffsq)

    return msd[1:]


def fit_diffusion(msd, frame_time, p_min, diff_estimate, sigma, p_maxed=False):
    """Fit the msd with a linear function; take p_min points into account."""

    if p_min < len(msd):
        n = p_min
    else:
        n = len(msd)

    # Calculate diffusion coefficient.
    t = frame_time * np.arange(1, n+1)
    d = msd[:n]
    a_expected = 4 * sigma**2
    b_expected = 2 * diff_estimate
    params, cov = curve_fit(fit_func, t, d, p0=(a_expected, b_expected))
    diff = params[1] / 2
    # print(b_expected / 2, diff)

    # If diff is very small, but the trace is reasonably long,
    # look for a p_min for which diff converges. First increase p_min, then decrease.
    if diff < 1e-15 and len(msd) > 8:
        if p_min < len(msd) and p_maxed == False:
            # print(len(msd), diff, p_min, p_maxed)
            return fit_diffusion(msd, frame_time, p_min+1, diff_estimate, sigma)
        elif n == len(msd):
            # print(len(msd), diff, p_min, p_maxed)
            return fit_diffusion(msd, frame_time, n-1, diff_estimate, sigma, p_maxed=True)
        elif n > 3 and p_maxed == True:
            # print(len(msd), diff, p_min, p_maxed)
            return fit_diffusion(msd, frame_time, n-1, diff_estimate, sigma, p_maxed=True)

    return diff, params


def get_diffusion_coefficient(x_vals, diff_estimate, t_exposure, avg_photon_count, sigma_psf, frame_time, show=False):
    """Get diffusion coefficient using p_min values; find p_min using a convergence scheme.
    Based on https://doi.org/10.1103/PhysRevE.82.041914.

    Parameters
    ----------
    x_vals : array of floats
        Positions of particle over time.
    diff_estimate : float
        Diffusion coefficient estimate.
    t_exposure : float
        Exposure time of spot.
    avg_photon_count : float
        Average photon count for spot.
    sigma_psf : float
        Sigma of psf.
    frame_time : float
        Exposure time of frame.
    show : bool (optional)
        Plot MSD if calculation does not converge. Default: False.

    Returns
    -------
    diff : float
        Diffusion coefficient.
    """

    # Prepare.
    diff = 0.
    diff_est = diff_estimate
    n_max = 100
    p_vals = np.zeros(n_max, dtype=int)

    # Get MSD.
    msd = get_msd(x_vals)

    # Iterate, check for convergence.
    for i in range(n_max):

        # Number of points to take into account.
        p_min, sigma = get_p_min(diff_est, t_exposure, avg_photon_count, sigma_psf, frame_time)
        p_vals[i] = p_min

        # Fit diffusion constant
        diff, params = fit_diffusion(msd, frame_time, p_min, diff_est, sigma)
        # print(p_min, diff_est)

        if i > 10 and p_vals[i] == p_vals[i-2]:

            # We are in a loop... use largest p_min value.
            p_min = np.amax(p_vals[i-2:i])
            diff, params = fit_diffusion(msd, frame_time, p_min, diff_est, sigma)
            return diff

        elif i == 0 or (i > 0 and p_vals[i] != p_vals[i-1]):

            # Set estimate to new value with lower boundary 1e-16 so as not to divide by zero.
            diff_est = np.clip(diff, 1e-16, None)

        else:

            # Value found!
            # Plot MSD if calculation did not converge.
            if diff < 1.0001e-16:
                import matplotlib.pyplot as plt
                t = frame_time * np.arange(1, len(msd)+1)
                if show:
                    plt.plot(t, msd)
                    plt.plot(t, fit_func(t, *params))
                    plt.show()
                    plt.close()
                diff = np.nan
            break

    # print("Found! ", p_min, diff)
    return diff
