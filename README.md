# CTrapPy version 1.4.3

CTrap scan processing for Python, using a streamlit GUI. This interface 
is used to process and filter experiments consisting of multiple C-trap 
(or Q-trap) output files, and to generate and customize figures from the 
resulting data. It can also be used in the lab, to quickly see the 
contents of raw microscope output.

----

## Usage

Full documentation at: https://nynkedekkerlab.github.io/ctrappy/.

### Installation

Make sure you have 3.9.16<=python<=3.10.11 installed (if something goes
wrong with the `ruptures` installation, try Python version 3.10.11). Then open a 
terminal/command prompt in the ctrappy root directory, and type:

```
python -m venv venv
```

Then, on windows:

```
.\venv\Scripts\activate
```

Or, on linux/macOS:

```
source venv/bin/activate
```

Finally, install everything with

```
pip install -e .
```

### Starting the GUI
To start up the GUI, first activate the virtual environment `venv`.
Then use the (PyCharm) terminal to run the command

```
python -m ctrappy.run
```

The GUI should appear. If this is not the case, try to look
for the address of the GUI in the terminal window and copy
and paste that address into the address bar of a web browser.
It should look something like `http://localhost:8501`.

If you get a streamlit error, you can also try from the root directory:

```
streamlit run ctrappy/gui.py
```

Use the navigation button at the top left to select
the functionality you want to use.

### GUI tabs

An overview of the functionality of each GUI tab can be
found in `README_GUI_TABS.md`.

### Set GUI default values

To set default values for the GUI, copy the file 
`ctrappy/gui_tabs/defaults/gui_defaults.yml`, and rename it to
`ctrappy/gui_tabs/defaults/gui_custom.yml`. Now you can change the values
in the file; these values will be the default values when running the GUI.

----

## Notebooks

You can customize figures by using a notebook.
See for example *notebooks/example_custom_figure.ipynb*. 

Some tips:
- Run your notebooks with ``jupyter notebook`` in PyCharm terminal.
- You can copy any `plot_*` function from `ctrappy.gui_figures.py` into the notebook
  and make changes.

----

## Input format

To do: describe required fields in `config.yml`, format of image files,
format of metadata, etc.

----

## Output format

The `scans.csv` file contains the columns:
- **col_dist**: float, colocalization distance, if colocalized
- **colocalized** bool, True if colocalized
- **color_id**: integer; color of spot, 0, 1, 2 for 'r', 'g', 'b', respectively
- **color_str**: str; color of spot, 'r' or 'g' or 'b'
- **corrected_intensity**: float, intensity corrected for crosstalk
- **dna_end_pixel**: float, ending pixel coordinate of DNA (right offset)
- **dna_length_kbp**: float, dna length in kbp
- **dna_start_pixel**: float, starting pixel coordinate of DNA (left offset)
- **file_name**: str, name of file on file system
- **frame**: integer frame number
- **frame_subindex**: integer, index for each spot in a scan in a frame (used for making LAP matrices)
- **intensity**: integer, sum of pixel values in detected spot
- **laser_colors**: str; all laser colors that were used during the experiment
- **lifetime**: integer, lifetime of the track in frames
- **n_frames**: integer, number of frames of scan
- **n_scans**: integer, number of scans in experiment
- **scan_id**: integer, identifier for scan
- **spot_id**: integer, unique index for each spot (not used)
- **step_count**: integer, number of steps in track (from step fitting the bleaching trace)
- **time_s**: float, time in seconds
- **trace_id**: integer, identifier for colocalized tracks of different colors
- **track_id**: integer, a spot of a single color, tracked over time (unique after selecting a **scan_id** and **frame**)
- **x_kbp**: float, distance to left bead edge in kbp
- **x_micron**: float, distance to left bead edge in micron
- **x_pixel**: float, x-location of spot in pixels in absolute coordinates
- **y_pixel**: float, y-location of spot in pixels in absolute coordinates

### Table indices and class hierarchy

The `scans.csv` table has several index (`*_id`) columns. The most useful way 
to think about these is as follows.

A *track* is a single-color spot, tracked over multiple frames in a *scan*.
Hence, a track contains multiple spot locations; at most one spot location 
for each frame, although some frames might be skipped. 
By picking a `scan_id`, `track_id` in the table
you can find each separate track that is present in the dataset. The `trace_id`
index groups colocalized tracks together.

This data hierarchy is mirrored in the way the code is organized into classes. 
An *Experiment* can contain multiple *Scans* (one for each image taken); 
a *Scan* can contain multiple *Traces*; a *Trace* can contain multiple 
(colocalized) *Tracks*. A *Track* is a single-color spot, tracked over
multiple frames.

Different physical quantities belong to different levels in this class hierarchy.
Stoichiometry, for example, is a `Trace` attribute, but fluorophore lifetime
is a `Track` attribute.

----

## Full documentation

The latest published version can be found at https://nynkedekkerlab.github.io/ctrappy/.

In order to call the ctrappy functions from analysis notebooks, it is useful to
have an overview of the full functionality. The full (sphinx generated) documentation 
of every single class and function can be found in **docs/build/html/index.html**.

To regenerate the documentation, make sure you have sphinx installed, navigate
to the `docs/` folder and then run `make html`.

----

## Notes

**Desktop shortcut**

For adding a shortcut to the GUI to your desktop (provided you are using
Anaconda on Windows), make a file called `gui.bat`
in the repo root. This file should contain the lines:
```
call .\venv\Scripts\activate
python -m ctrappy.run
```
Then, make a shortcut to this .bat file on your desktop. Please note: bat files are
automatically ignored by git (see `.gitignore`).

On unix systems (linux/os x) you can use a similar bash script instead.

----

## Authors

- *Edo van Veen* original code development
- *Francisco J. Palmero Moya* current code development


## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
